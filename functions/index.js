const functions = require("firebase-functions");
const firebaseAdmin = require("firebase-admin");

let topic = "employees";
firebaseAdmin.initializeApp(functions.config().firebase);

exports.subcribes = functions.https.onRequest(async (req, res) => {
  try {
    const { token } = req.body;

    const response = await firebaseAdmin
      .messaging()
      .subscribeToTopic(token, `/topics/${topic}`);
    res.json({ ...response, topic });
  } catch (error) {
    console.error("Error: ", error);
  }
});

exports.notifyEmployees = functions.https.onRequest(async (req, res) => {
  try {
    const { notification, data } = req.body;
    const payload = {
      notification,
      data
    };
    const response = await firebaseAdmin
      .messaging()
      .sendToTopic(`/topics/${topic}`, payload);
    res.json({ ...response, topic });
  } catch (error) {
    console.error("Error: ", error);
  }
});

exports.notify = functions.https.onRequest(async (req, res) => {
  try {
    const { tokens, notification, data } = req.body;
    const payload = {
      notification,
      data
    };

    const response = await firebaseAdmin
      .messaging()
      .sendToDevice(tokens, payload);

    res.json({
      message: "It's worked!!",
      success: response.successCount,
      failed: response.failureCount,
      response: response.results
    });
  } catch (error) {
    console.error("Error: ", error);
  }
});
