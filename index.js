/**
 * @format
 */

import { AppRegistry } from "react-native";
import notificationService from "services/NotificationService";
import App from "./App";
import { name as appName } from "./app.json";

AppRegistry.registerComponent(appName, () => App);
AppRegistry.registerHeadlessTask(
  appName,
  () => notificationService.notificationListener
);
