import BaseService from "./BaseService";

class InformationService extends BaseService {
  async getAboutUs(token = null) {
    const endpoint = "/api/information_centres/about";
    const result = await this.get(endpoint, token);
    return result;
  }

  async getContactUs(token = null) {
    const endpoint = "/api/information_centres/contact";
    const result = await this.get(endpoint, token);
    return result;
  }

  async getHelp(token = null) {
    const endpoint = "/api/information_centres/help";
    const result = await this.get(endpoint, token);
    return result;
  }

  async getPhone(token = null) {
    const endpoint = "/api/information_centres/phone";
    const result = await this.get(endpoint, token);
    return result;
  }

  async getEmail(token = null) {
    const endpoint = "/api/information_centres/email";
    const result = await this.get(endpoint, token);
    return result;
  }
}

const informationService = new InformationService();
export default informationService;
