import BaseService from "./BaseService";

class RestaurantService extends BaseService {
  async fetchRestaurants(token) {
    const endpoint = `/api/restaurants?pagination=false`;
    const result = await this.get(endpoint, token);
    return result;
  }

  async getRestaurant(token, id) {
    const endpoint = `/api/restaurants/${id}`;
    const result = await this.get(endpoint, token);
    return result;
  }
}

const restaurantService = new RestaurantService();
export default restaurantService;
