import firebase from "react-native-firebase";
import userStore from "stores/UserStore";
import { android, isAndroid } from "utils/Constants";
import restaurantStore from "stores/RestaurantStore";
import homepageStore from "stores/HomepageStore";
import splashStore from "stores/SplashStore";
import profileStore from "stores/ProfileStore";
import navbarStore from "stores/NavbarStore";

import dataStore from "stores/DataStore";
import restaurantService from "services/RestaurantService";

class NotificationService {
  constructor() {
    // this.notificationDisplay = null;
    this.notificationListener = null;
    this.notificationOpenedListener = null;
    this.navigation = null;
  }

  init() {
    if (isAndroid) {
      this.createAndroidNotificationChannels();
    }
    this.checkPermission();
    this.createNotificationListeners();
  }

  release() {
    // this.notificationDisplay();
    try {
      this.notificationListener();
      this.notificationOpenedListener();
      console.log("Release notification");
    } catch (error) {
      console.log("Release notification error", JSON.stringify(error));
    }
  }

  checkPermission = async () => {
    const enabled = await firebase.messaging().hasPermission();

    if (enabled) {
      // user has permissions
      this.getToken();
    } else {
      // user doesn't have permission
      this.requestPermission();
    }
  };

  requestPermission = async () => {
    try {
      await firebase.messaging().requestPermission();

      // user has authorised

      this.getToken();
    } catch (error) {
      // user has rejected permission
      console.log("User reject notify permission");
    }
  };

  resetToken = async () => {
    const enabled = await firebase.messaging().hasPermission();
    userStore.user.deviceToken = null;
    try {
      if (!enabled) {
        // user doesn't have permission
        await firebase.messaging().requestPermission();
      }
      await firebase.messaging().deleteToken();
      this.getToken();
    } catch (error) {
      // user has rejected permission
      console.log("User reject notify permission");
    }
  };

  getToken = async () => {
    if (userStore.user.deviceToken) return;
    const deviceToken = await firebase.messaging().getToken();
    if (deviceToken) {
      console.log("getToken", deviceToken);
      userStore.user.deviceToken = deviceToken;
    }
  };

  createNotificationListeners = async () => {
    try {
      // Triggered when a particular notification has been received in foreground
      this.notificationListener = firebase
        .notifications()
        .onNotification(notification => {
          const notify = new firebase.notifications.Notification()
            .setNotificationId(notification.notificationId)
            .setTitle(notification.title)
            .setBody(notification.body)
            .setData(notification.data);

          if (isAndroid) {
            notify.android
              .setChannelId(android.notification.channelId)
              .android.setColor(dataStore.currentTheme)
              .android.setSmallIcon("ic_notification")
              .android.setBigText(notification.body);
          }

          firebase.notifications().displayNotification(notify);
        });

      // If your app is in background, you can listen for when
      // a notification is clicked / tapped / opened as follows:
      this.notificationOpenedListener = firebase
        .notifications()
        .onNotificationOpened(notificationOpen => {
          console.log(
            "Notification when notification is clicked",
            notificationOpen
          );
          notificationOpen.notification.android.setBigText(
            notificationOpen.notification.body
          );
          this.handleOnOpenNotification(notificationOpen.notification.data);

          firebase
            .notifications()
            .removeDeliveredNotification(
              notificationOpen.notification.notificationId
            );
        });

      // If your app is closed, you can check if it was opened by
      // a notification being clicked / tapped / opened as follows:
      firebase
        .notifications()
        .getInitialNotification()
        .then(async notificationOpen => {
          if (notificationOpen) {
            console.log(
              "Notification when app close",
              JSON.stringify(notificationOpen.notification.notificationId)
            );
            notificationOpen.notification.android.setBigText(
              notificationOpen.notification.body
            );
            splashStore.setNotificationPayload(
              notificationOpen.notification.data
            );
            await this.handleOnOpenNotification(
              notificationOpen.notification.data
            );
          }
        });
    } catch (error) {
      console.log("Error notification listener: ", error);
    }
  };

  createAndroidNotificationChannels = () => {
    try {
      // Build a channel
      const channel = new firebase.notifications.Android.Channel(
        android.notification.channelId,
        android.notification.channelName,
        firebase.notifications.Android.Importance.Default
      );

      // Create the channel
      firebase.notifications().android.createChannel(channel);
    } catch (error) {
      console.log("Error creating channel: ", error);
    }
  };

  setNavigation = navigation => {
    this.navigation = navigation;
  };

  handleOnOpenNotification = async data => {
    const options = {
      NOTIFY_NEW_RESTAURANT: this.handleOnNewRestaurant,
      NOTIFY_EXPIRATION: this.handleOnExpiration,
      NOTIFY_CREDIT_AFFECTION: this.handleOnEffection,
      default: () => {
        this.navigation.navigate("Profile");
        navbarStore.setSelectedTab("ProfileTab");
      }
    };

    if (options[data.type]) {
      options[data.type](data);
    } else {
      options.default();
    }
  };

  handleOnExpiration = async () => {
    try {
      await profileStore.updateData();
      this.navigation.navigate("Profile");
      navbarStore.setSelectedTab("ProfileTab");
    } catch (error) {
      console.log("Error opening expiration: ", error);
    }
  };

  handleOnEffection = async () => {
    try {
      await profileStore.updateData();
      navbarStore.setSelectedTab("ProfileTab");
      this.navigation.navigate("Profile");
    } catch (error) {
      console.log("Error opening effection: ", error);
    }
  };

  handleOnNewRestaurant = async data => {
    try {
      const { token } = userStore.user;
      if (!token) return;

      let newRestaurant = restaurantStore.listRestaurants[data.restaurantId];
      if (!newRestaurant) {
        newRestaurant = await restaurantService.getRestaurant(
          token,
          data.restaurantId
        );
        restaurantStore.listRestaurants.push(newRestaurant);
      }
      homepageStore.initListAnnotations();

      const restaurant = {
        id: newRestaurant.id,
        avatar: newRestaurant.avatar,
        name: newRestaurant.restaurantName,
        images: newRestaurant.photo,
        facebook: newRestaurant.facebook,
        instagram: newRestaurant.instagram,
        website: newRestaurant.website,
        email: newRestaurant.email,
        phoneNumber: [newRestaurant.phoneNumber1, newRestaurant.phoneNumber2],
        openingHours: newRestaurant.openingHours,
        description: newRestaurant.description,
        isOpening: newRestaurant.isOpening,
        time: newRestaurant.time,
        cardAccepted: newRestaurant.cardAccepted
      };
      this.navigation.navigate("RestaurantDetail", { restaurant });
    } catch (error) {
      console.log("Error opening notification: ", error);
    }
  };
}

const notificationService = new NotificationService();
export default notificationService;
