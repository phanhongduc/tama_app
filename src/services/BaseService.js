import axios from "axios";
import { SERVER_URL } from "utils/API";
// const SERVER_URL = "http://t-api.kyanon.digital";

class BaseService {
  constructor() {
    this.baseUrl = SERVER_URL;
  }

  post(endpoint, params, token = null, contentType = "application/json") {
    return this.requestHttp(
      "POST",
      this.baseUrl + endpoint,
      params,
      token,
      contentType
    );
  }

  get(endpoint, token = null) {
    return this.requestHttp("GET", this.baseUrl + endpoint, null, token);
  }

  put(endpoint, params, token = null) {
    return this.requestHttp("PUT", this.baseUrl + endpoint, params, token);
  }

  patch(endpoint, params, token = null) {
    return this.requestHttp("PATCH", this.baseUrl + endpoint, params, token);
  }

  delete(endpoint, params, token = null) {
    return this.requestHttp("DELETE", this.baseUrl + endpoint, params, token);
  }

  // eslint-disable-next-line class-methods-use-this
  requestHttp(
    method,
    url,
    params,
    token = null,
    contentType = "application/json"
  ) {
    return new Promise((resolve, reject) => {
      const config = {
        method,
        url,
        headers: {
          Accept: "application/json",
          "Content-Type": contentType
        }
      };

      if (params) {
        config.data = params;
      }

      if (token) {
        config.headers.Authorization = `Bearer ${token}`;
      }
      // console.log("request", JSON.stringify(config));
      axios(config)
        .then(response => {
          resolve(response.data);
        })
        .catch(error => {
          if (error.response) {
            // The request was made and the server responded with a status code
            // that falls out of the range of 2xx
            // console.log('Error 1 data', error.response.data);
            // console.log('Error 1 status', error.response.status);
            // console.log('Error 1 headers', error.response.headers);
            console.log("error response", error.response);
            reject(error.response);
          } else if (error.request) {
            // The request was made but no response was received
            // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
            // http.ClientRequest in node.js
            reject(error.request);
            console.log("Error 2", error.request);
          } else {
            // Something happened in setting up the request that triggered an Error
            console.log("Error 3", error.message);
            reject(error);
          }
          // console.log('Error 4', error.config);
        });
    });
  }

  uploadFileWithProgress(
    endpoint,
    filePath,
    fileName,
    key,
    params,
    token = null
  ) {
    return new Promise((resolve, reject) => {
      const url = this.baseUrl + endpoint;
      const formData = new FormData();

      // formData.append(key, {
      //   type: "image/jpeg",
      //   name: fileName,
      //   uri: filePath
      // });

      axios
        .post(url, formData, {
          headers: {
            Accept: "application/json",
            "Content-Type": "multipart/form-data",
            Authorization: `Bearer ${token}`
          },
          auth: `Bearer ${token}`
        })
        .then(response => resolve(response))
        .catch(error => reject(error));
    });
  }
}

export default BaseService;
