import { userRole } from "utils/Constants";
import BaseService from "./BaseService";

class UserService extends BaseService {
  async login(token, username, password, deviceInfo) {
    const endpoint = `/api/login`;
    const result = this.post(
      endpoint,
      { username, password, deviceInfo },
      token
    );
    return result;
  }

  async logout(token) {
    const endpoint = `/logout`;
    const result = await this.post(endpoint, null, token);
    return result;
  }

  async getEmployeeTermCondition(token) {
    const endpoint = `/api/information_centres/general_conditions`;
    const result = this.get(endpoint, token);
    return result;
  }

  async getRestaurantTermCondition(token) {
    const endpoint = `/api/information_centres/general_conditions`;
    const result = this.get(endpoint, token);
    return result;
  }

  async acceptTermCondition(token) {
    const endpoint = `/api/accepted_conditions`;
    const result = this.post(endpoint, null, token);
    return result;
  }

  async createPincode(token, pincode) {
    const endpoint = `/api/pincode/create`;
    const result = this.post(endpoint, { pincode }, token);
    return result;
  }

  async verifyPincode(token, pincode, customID) {
    const endpoint = `/api/pincode/verify`;
    const result = this.post(endpoint, { pincode, customID }, token);
    return result;
  }

  async changePincode(token, pincode) {
    const endpoint = `/api/pincode/create`;
    const result = this.post(endpoint, { pincode }, token);
    return result;
  }

  async forgotPincode(email, token) {
    const endpoint = `/api/forgot-pincode`;
    const result = this.post(endpoint, { email }, token);
    return result;
  }

  async readAboutApp(token) {
    const endpoint = `/api/read_about_app`;
    const result = this.post(endpoint, null, token);
    return result;
  }

  async readInstruction(token) {
    const endpoint = `/api/read_instruction`;
    const result = this.post(endpoint, null, token);
    return result;
  }

  async forgotPassword(email) {
    const endpoint = `/api/forgotpassword`;
    const result = this.post(endpoint, { email }, null);
    return result;
  }

  async scanQRCode(token, customId) {
    const endpoint = `/api/qrcode`;
    const result = this.post(endpoint, { customId }, token);
    return result;
  }

  async getEmployeeInfo(token, id) {
    const endpoint = `/api/employees/${id}`;
    const result = this.get(endpoint, token);
    return result;
  }

  async getRestaurantInfo(token, id) {
    const endpoint = `/api/restaurants/${id}`;
    const result = this.get(endpoint, token);
    return result;
  }

  async makePayment(token, customId, amount) {
    const endpoint = `/api/payment`;
    const result = this.post(endpoint, { amount, customId }, token);
    return result;
  }

  async getHistory(id, role = userRole.EMPLOYEE, token) {
    let endpoint = `/api/user/${id}/employee/transaction`;
    if (role === userRole.RESTAURANT) {
      endpoint = `/api/user/${id}/transaction/transaction`;
    }
    const result = this.get(endpoint, token);
    return result;
  }

  async uploadPhotos(id, photos, token) {
    const endpoint = `/api/user/${id}/upload/photo`;
    const result = this.post(endpoint, photos, token, "multipart/form-data");
    return result;
  }

  async dropSession(token) {
    const endpoint = `/api/drop-app-session`;
    const result = this.get(endpoint, token);
    return result;
  }
}

const userService = new UserService();
export default userService;
