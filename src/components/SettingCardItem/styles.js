import { StyleSheet } from "react-native";
import { fontSizes, fonts, colors } from "utils/Constants";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  screenWidth
} from "utils/Styling";

const styles = StyleSheet.create({
  cardItemView: {
    width: wp(screenWidth * 0.8),
    height: hp(50),
    backgroundColor: colors.background.white,
    borderRadius: 10,
    margin: wp(5),
    padding: wp(10),
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  cardItemTitle: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    flex: 3
  },
  cardItemText: {
    flex: 4,
    fontFamily: fonts.primary.medium,
    color: colors.text.gray.icon,
    fontSize: fontSizes.detail.small
  },
  cardItemIconView: { flex: 1, alignItems: "flex-end" },
  cardItemTitleView: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    flex: 5
  },
  cardIconWrapper: {
    flex: 1
  }
});

export default styles;
