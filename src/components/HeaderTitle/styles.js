import { StyleSheet } from "react-native";
import { colors, fontSizes, fonts } from "utils/Constants";

const styles = StyleSheet.create({
  titleStyle: {
    fontFamily: fonts.primary.semibold,
    fontSize: fontSizes.header,
    color: colors.text.white
  }
});

export default styles;
