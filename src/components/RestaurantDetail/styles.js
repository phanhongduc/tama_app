import { StyleSheet } from "react-native";
import { Header } from "react-navigation";
import { fontSizes, fonts, colors } from "utils/Constants";
import {
  heightPercentageToDP as hp,
  screenWidth,
  screenHeight
} from "utils/Styling";

const mainScreenHeight = screenHeight - Header.HEIGHT;
const headerHeight = mainScreenHeight * 0.2;

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    backgroundColor: colors.background.white,
    width: screenWidth
  },
  headerWrapper: {
    alignItems: "center",
    marginBottom: 30
  },
  cardContainer: {
    height: headerHeight * 0.5,
    marginTop: headerHeight * 0.28
  },
  cardInfoContainer: {
    height: headerHeight * 0.5 * 0.8
  },
  nameStyle: {
    fontSize: fontSizes.header,
    fontFamily: fonts.primary.bold
  },
  socialAction: {
    flexDirection: "row",
    justifyContent: "space-evenly",
    alignItems: "center",
    backgroundColor: colors.background.gray.bar,
    height: hp(100)
  },
  iconSocialWrapper: {
    height: "50%",
    aspectRatio: 1 / 1,
    justifyContent: "center",
    alignItems: "center"
  },
  infoDetailContainer: {
    paddingTop: hp(30),
    paddingBottom: hp(30),
    alignItems: "center"
  },
  infoDetailDescription: {
    fontFamily: fonts.primary.regular,
    fontSize: fontSizes.detail.small,
    color: colors.text.gray.title,
    textAlign: "center",
    width: screenWidth * 0.8,
    paddingTop: 5,
    paddingBottom: 2.5
  },
  infoDetailOpenTime: {
    fontFamily: fonts.primary.semibold,
    fontSize: fontSizes.detail.small,
    color: colors.text.gray.title,
    textAlign: "center",
    paddingTop: 2.5,
    paddingBottom: 2.5
  },
  infoDetailPhoneNumberWrapper: {
    flexDirection: "row",
    paddingTop: 2.5,
    paddingBottom: 2.5
  },
  infoDetailPhoneNumber: {
    fontFamily: fonts.primary.regular,
    fontSize: fontSizes.detail.small,
    // color: colors.text.gray.title
    color: colors.text.gray.title
  },
  infoDetailAddress: {
    fontFamily: fonts.primary.regular,
    fontSize: fontSizes.detail.small,
    color: colors.text.green,
    textAlign: "center",
    paddingTop: 2.5,
    paddingBottom: 2.5
  },
  columnStyle: {
    justifyContent: "space-between"
  },
  restaurantColumnStyle: { justifyContent: "flex-start" },
  separator: {
    height: screenWidth * 0.0155
  },
  imageStyle: {
    width: screenWidth * 0.323,
    aspectRatio: 121 / 128
  },
  blankArea: {
    height: 100
  },
  modal: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: colors.background.overlayDark
  },
  modalGalleryContainer: {
    justifyContent: "center",
    alignItems: "center",
    height: screenHeight * 0.9,
    width: screenWidth
  },
  modalWrapper: {
    marginTop: screenHeight * 0.1,
    height: screenHeight * 0.7
  },
  modalSlider: {
    overflow: "visible",
    marginTop: 20
  },
  modalImage: {
    height: screenWidth
  },
  modalLeftCloseButton: {
    position: "absolute",
    left: 20,
    top: 0,
    height: 40
  },
  modalRightCloseButton: {
    position: "absolute",
    right: 20,
    top: 0,
    height: 40
  },
  emptyViewWrapper: {
    height: screenHeight * 0.5,
    backgroundColor: colors.background.gray.bar,
    justifyContent: "flex-start",
    alignItems: "center",
    padding: 10
  },
  emptyTextStyle: {
    fontFamily: fonts.primary.regular,
    fontSize: fontSizes.detail.small,
    color: colors.text.gray.detail
  },
  uploadButtonStyle: {
    backgroundColor: "rgba(0,0,0,.5)",
    justifyContent: "center",
    alignItems: "center",
    marginRight: 3
  },
  uploadButton: { width: "70%", height: "70%" },
  uploadModalOuterStyle: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: colors.background.overlay
  },
  uploadModalStyle: {
    width: "80%",
    justifyContent: "center",
    alignSelf: "center",
    backgroundColor: colors.background.white
  },
  uploadOptionView: {
    height: 80,
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    padding: 10,
    borderBottomWidth: 0.5,
    borderBottomColor: colors.text.gray.line
  },
  uploadOptionImage: { flex: 1, width: 40, height: 40 },
  uploadOptionTitle: {
    flex: 2,
    fontFamily: fonts.primary.regular,
    fontSize: fontSizes.detail.big2,
    color: colors.text.gray.tab
  },
  uploadOptionBelowView: {
    height: 80,
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    padding: 10
  }
});

export default styles;
