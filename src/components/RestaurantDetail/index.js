import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  FlatList,
  Modal,
  Linking,
  Alert
} from "react-native";
import FastImage from "react-native-fast-image";
import Carousel from "react-native-snap-carousel";
import IconM from "react-native-vector-icons/MaterialIcons";
import { observer, inject } from "mobx-react";
import {
  convertTimeObjectToStringArray,
  convertExceptionTimeToStringArray
} from "utils/Time";
import RestaurantCardItem from "components/RestaurantCardItem";
import SocialPanel from "components/SocialPanel";
import ToastMessage from "components/ToastMessage";
import {
  colors,
  isIOS,
  userRole,
  errorMessage,
  infoMessage,
  Images
} from "utils/Constants";
import Validate from "utils/Validate";
import { screenWidth } from "utils/Styling";
import { SERVER_URL } from "utils/API";
import styles from "./styles";

const imgPlaceholder = require("assets/images/img_placeholder.png");

@inject(
  "restaurantDetailStore",
  "restaurantHomepageStore",
  "userStore",
  "dataStore"
)
@observer
export default class RestaurantDetailScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      uploadModal: false
    };
  }

  componentDidMount() {
    const { restaurantDetailStore } = this.props;
    restaurantDetailStore.checkInternetConnection();
  }

  setModal = state => {
    this.setState({ uploadModal: state });
  };

  renderPickPhoto = () => {
    return (
      <TouchableOpacity
        style={[styles.imageStyle, styles.uploadButtonStyle]}
        onPress={() => this.setModal(true)}
      >
        <Image
          style={styles.uploadButton}
          source={Images.IC_UPLOAD}
          resizeMode="contain"
        />
      </TouchableOpacity>
    );
  };

  renderItem = ({ item, index }) => {
    const { restaurantDetailStore } = this.props;
    const imagePath = SERVER_URL + item.path;
    const imageSource = restaurantDetailStore.isConnected
      ? { uri: imagePath }
      : imgPlaceholder;

    const marginLeft = index % 3 === 2 ? { marginLeft: 3 } : {};
    const margin = index % 3 === 1 ? { marginLeft: 3, marginRight: 3 } : {};
    const marginRight = index % 3 === 0 ? { marginRight: 3 } : {};

    return (
      <TouchableOpacity
        style={[marginLeft, margin, marginRight]}
        onPress={() => restaurantDetailStore.onPressImage(index)}
      >
        <FastImage
          style={styles.imageStyle}
          source={imageSource}
          resizeMode={FastImage.resizeMode.cover}
        />
      </TouchableOpacity>
    );
  };

  renderRestaurantItem = ({ item, index }) => {
    const { restaurantDetailStore } = this.props;
    const imagePath = SERVER_URL + item.path;
    const imageSource = restaurantDetailStore.isConnected
      ? { uri: imagePath }
      : imgPlaceholder;
    if (item.id === -1) {
      return this.renderPickPhoto();
    }
    const marginLeft = index % 3 === 2 ? { marginLeft: 3 } : {};
    const margin = index % 3 === 1 ? { marginLeft: 3, marginRight: 3 } : {};
    const marginRight = index % 3 === 0 ? { marginRight: 3 } : {};
    return (
      <TouchableOpacity
        style={[marginLeft, marginRight, margin]}
        onPress={() => restaurantDetailStore.onPressImage(index - 1)}
      >
        <FastImage
          style={styles.imageStyle}
          source={imageSource}
          resizeMode={FastImage.resizeMode.cover}
        />
      </TouchableOpacity>
    );
  };

  renderSeparator = () => <View style={styles.separator} />;

  renderGalleryModal = () => {
    const { restaurantDetailStore, restaurant } = this.props;

    return (
      <Modal
        visible={restaurantDetailStore.openGalleryModal}
        animationType="fade"
        transparent
        hardwareAccelerated
      >
        <View style={styles.modal}>
          <View style={styles.modalGalleryContainer}>
            <View style={styles.modalWrapper}>
              <Carousel
                sliderWidth={screenWidth}
                sliderHeight={screenWidth * 2}
                itemWidth={screenWidth * 0.9}
                data={restaurant.images.slice()}
                renderItem={this.renderItemGalleryModal}
                inactiveSlideScale={0.94}
                inactiveSlideOpacity={0}
                containerCustomStyle={styles.modalSlider}
                firstItem={restaurantDetailStore.selectedImgIdx}
              />
            </View>
            <TouchableOpacity
              style={
                isIOS
                  ? styles.modalLeftCloseButton
                  : styles.modalRightCloseButton
              }
              onPress={restaurantDetailStore.toggleGalleryModal}
            >
              <IconM
                name="close"
                size={30}
                color={colors.background.gray.dot}
              />
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  renderItemGalleryModal = ({ item }) => {
    const imagePath = SERVER_URL + item.path;

    return (
      <View style={styles.item}>
        <Image
          source={{ uri: imagePath }}
          style={styles.modalImage}
          resizeMode="contain"
        />
      </View>
    );
  };

  handleOnPickFromGallery = async () => {
    const { restaurantHomepageStore, userStore } = this.props;
    if (userStore.user.role === userRole.RESTAURANT) {
      const response = await restaurantHomepageStore.pickFromGallery(
        userStore.user.userID
      );

      if (response.status !== "failed") {
        userStore.getInfo();
      }
    }
  };

  handleOnPickFromCamera = async () => {
    const { restaurantHomepageStore, userStore } = this.props;
    if (userStore.user.role === userRole.RESTAURANT) {
      const response = await restaurantHomepageStore.pickFromCamera(
        userStore.user.userID
      );

      userStore.getInfo();
    }
  };

  handleOnPressMail = mail => {
    if (!mail) return;
    Linking.canOpenURL(`mailto:${mail}`)
      .then(supported => {
        if (supported) {
          Linking.openURL(`mailto:${mail}`);
        } else {
          Linking.openURL(
            "https://accounts.google.com/ServiceLogin?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1"
          );
        }
      })
      .catch(error => console.log(error));
  };

  handleOnPressFacebook = facebookURL => {
    if (!facebookURL) return;
    Linking.canOpenURL(facebookURL)
      .then(supported => {
        if (supported) {
          Linking.openURL(facebookURL);
        }
      })
      .catch(error => console.log(error));
  };

  handleOnPressInstagram = instagramURL => {
    if (!instagramURL) return;
    Linking.canOpenURL(instagramURL)
      .then(supported => {
        if (supported) {
          Linking.openURL(instagramURL);
        }
      })
      .catch(error => console.log(error));
  };

  handleOnOpenWebsite = website => {
    if (!website) return;
    Linking.canOpenURL(website)
      .then(supported => {
        if (supported) {
          Linking.openURL(website);
        }
      })
      .catch(error => console.log(website, error));
  };

  handleOnMakeCall = phoneNumber => {
    const actionURL = `tel:${phoneNumber}`;
    Linking.canOpenURL(actionURL)
      .then(supported => {
        if (!supported) {
          Alert.alert(errorMessage.PHONE_NOT_AVAILABLE);
        } else {
          return Linking.openURL(actionURL);
        }
        return false;
      })
      .catch(err => console.log(err));
  };

  render() {
    const { restaurant, isRestaurant, theme, mode } = this.props;
    const { phoneNumber, phoneNumber1, phoneNumber2 } = restaurant;
    const { uploadModal } = this.state;

    if (!restaurant) return <View />;
    console.log(restaurant);
    const openingHourStr = convertTimeObjectToStringArray(
      restaurant.openingHours
    );
    const photos = restaurant.images.slice();
    photos.unshift({ id: -1 });

    const validatedFacebook = Validate.isValidField(restaurant.facebook);
    const validatedWebsite = Validate.isValidField(restaurant.website);
    const validatedOpeningHour = Validate.isValidField(restaurant.openingHours);
    const validatedExceptions = Validate.isValidField(
      restaurant.openingHours.exceptions
    );
    const validatedImages = Validate.isValidField(restaurant.images);
    const validatedEmail = Validate.isValidField(restaurant.email);

    return (
      <View style={{ flex: 1, alignItems: "center" }}>
        <ScrollView
          style={styles.container}
          showsVerticalScrollIndicator={false}
        >
          <View style={styles.headerWrapper}>
            <RestaurantCardItem
              containerStyle={styles.cardContainer}
              infoContainerStyle={styles.cardInfoContainer}
              nameStyle={styles.nameStyle}
              restaurant={restaurant}
            />
          </View>
          <SocialPanel
            restaurant={restaurant}
            mode={mode}
            isRestaurant={isRestaurant}
          />
          <View style={styles.infoDetailContainer}>
            <Text style={styles.infoDetailDescription}>
              {restaurant.description}
            </Text>
            {validatedOpeningHour && (
              <Text style={styles.infoDetailOpenTime}>
                {openingHourStr && openingHourStr.join("\n")}
              </Text>
            )}
            {validatedExceptions ? (
              <Text style={styles.infoDetailOpenTime}>
                {`Exception: \n ${convertExceptionTimeToStringArray(
                  restaurant.openingHours.exceptions
                ).join("\n")}`}
              </Text>
            ) : (
              <View />
            )}
            {!isRestaurant && (phoneNumber[0] || phoneNumber[1]) && (
              <View style={styles.infoDetailPhoneNumberWrapper}>
                <Text style={styles.infoDetailPhoneNumber}>
                  {phoneNumber[0] || phoneNumber[1]}
                  {phoneNumber[0] && phoneNumber[1]
                    ? `  -  ${phoneNumber[1]}`
                    : ""}
                </Text>
              </View>
            )}
            {isRestaurant && (phoneNumber || phoneNumber1) && (
              <View style={styles.infoDetailPhoneNumberWrapper}>
                <Text style={styles.infoDetailPhoneNumber}>
                  {phoneNumber1 || phoneNumber2}
                  {phoneNumber1 && phoneNumber2 ? `  -  ${phoneNumber2}` : ""}
                </Text>
              </View>
            )}
            {validatedEmail && (
              <TouchableOpacity
                onPress={() => this.handleOnPressMail(restaurant.email)}
              >
                <Text
                  style={[
                    styles.infoDetailAddress,
                    { color: theme.currentTheme || theme }
                  ]}
                >
                  {restaurant.email}
                </Text>
              </TouchableOpacity>
            )}
            {validatedWebsite && (
              <TouchableOpacity
                onPress={() => this.handleOnOpenWebsite(restaurant.website)}
              >
                <Text
                  style={[
                    styles.infoDetailAddress,
                    { color: theme.currentTheme || theme }
                  ]}
                >
                  {restaurant.website}
                </Text>
              </TouchableOpacity>
            )}
            {!validatedWebsite &&
              (validatedFacebook && (
                <TouchableOpacity
                  onPress={() => {
                    this.handleOnPressFacebook(restaurant.facebook);
                  }}
                >
                  <Text
                    style={[
                      styles.infoDetailAddress,
                      { color: theme.currentTheme || theme }
                    ]}
                  >
                    {restaurant.facebook}
                  </Text>
                </TouchableOpacity>
              ))}
          </View>
          {isRestaurant && validatedImages && (
            <FlatList
              columnWrapperStyle={styles.restaurantColumnStyle}
              keyExtractor={item => item.id.toString()}
              data={photos}
              renderItem={this.renderRestaurantItem}
              numColumns={3}
              ItemSeparatorComponent={this.renderSeparator}
              scrollEnabled={false}
            />
          )}
          {!isRestaurant && validatedImages && (
            <FlatList
              columnWrapperStyle={styles.restaurantColumnStyle}
              keyExtractor={item => item.id.toString()}
              data={restaurant.images.slice()}
              renderItem={this.renderItem}
              numColumns={3}
              ItemSeparatorComponent={this.renderSeparator}
              scrollEnabled={false}
            />
          )}
          {!isRestaurant && !validatedImages && (
            <View style={styles.emptyViewWrapper}>
              <Text style={styles.emptyTextStyle}>{errorMessage.NO_PHOTO}</Text>
            </View>
          )}
          {isRestaurant && !validatedImages && (
            <View
              style={[styles.emptyViewWrapper, { alignItems: "flex-start" }]}
            >
              {this.renderPickPhoto()}
            </View>
          )}
          <View
            style={[
              styles.blankArea,
              !validatedImages && {
                backgroundColor: colors.background.gray.bar
              }
            ]}
          />
          {validatedImages && this.renderGalleryModal()}
        </ScrollView>
        <ToastMessage />
        {isRestaurant && (
          <Modal
            animationType="fade"
            visible={uploadModal}
            onRequestClose={() => {}}
            transparent
          >
            <TouchableOpacity
              style={styles.uploadModalOuterStyle}
              activeOpacity={1}
              onPress={() => this.setModal(false)}
            >
              <View style={styles.uploadModalStyle}>
                <TouchableOpacity
                  style={styles.uploadOptionView}
                  onPress={this.handleOnPickFromGallery}
                >
                  <Image
                    source={Images.IC_GALLERY}
                    resizeMode="contain"
                    style={styles.uploadOptionImage}
                  />
                  <Text style={styles.uploadOptionTitle}>
                    {infoMessage.CHOOSE_PHOTO_FROM_LIBRARY}
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.uploadOptionBelowView}
                  onPress={this.handleOnPickFromCamera}
                >
                  <Image
                    source={Images.IC_CAMERA}
                    resizeMode="contain"
                    style={styles.uploadOptionImage}
                  />
                  <Text style={styles.uploadOptionTitle}>
                    {infoMessage.TAKE_A_PHTO}
                  </Text>
                </TouchableOpacity>
              </View>
            </TouchableOpacity>
          </Modal>
        )}
      </View>
    );
  }
}
