import React from "react";
import { RefreshHeader } from "react-native-spring-scrollview/RefreshHeader";
import {
  ActivityIndicator,
  Animated,
  View,
  StyleSheet,
  Text
} from "react-native";
import { infoMessage, Images } from "utils/Constants";

class CustomHeader extends RefreshHeader {
  static height = 80;

  static style = "stickyContent";

  render() {
    return (
      <View style={styles.container}>
        {this.renderIcon()}
        <View style={styles.rContainer}>
          <Text style={styles.text}>{this.getTitle()}</Text>
          {this.renderContent()}
        </View>
      </View>
    );
  }

  renderIcon() {
    const s = this.state.status;
    if (s === "refreshing" || s === "rebound") {
      return <ActivityIndicator color="gray" />;
    }
    const { maxHeight, offset } = this.props;
    return (
      <Animated.Image
        source={Images.ARROW_REFRESH}
        style={{
          width: 15,
          height: 40,
          transform: [
            {
              rotate: offset.interpolate({
                inputRange: [-maxHeight - 1 - 10, -maxHeight - 10, -50, -49],
                outputRange: ["180deg", "180deg", "0deg", "0deg"]
              })
            }
          ]
        }}
      />
    );
  }

  renderContent() {
    this.content = null;
    return this.content;
  }

  getTitle() {
    const s = this.state.status;
    if (s === "pulling" || s === "waiting") {
      return infoMessage.PULL_TO_REFRESH;
    }
    if (s === "pullingEnough") {
      return infoMessage.RELEASE_TO_REFRESH;
    }
    if (s === "refreshing") {
      return infoMessage.REFRESHING;
    }
    if (s === "pullingCancel") {
      return infoMessage.GIVE_UP_REFRESHING;
    }
    if (s === "rebound") {
      return infoMessage.REFRESH_COMPLETED;
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row"
  },
  rContainer: {
    marginLeft: 20
  },
  text: {
    marginVertical: 5,
    fontSize: 15,
    color: "#666",
    textAlign: "center",
    width: 200
  }
});

export default CustomHeader;
