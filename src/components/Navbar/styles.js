import { StyleSheet } from "react-native";
import { fontSizes, fonts } from "utils/Constants";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "utils/Styling";

const styles = StyleSheet.create({
  container: {
    position: "absolute",
    bottom: 0,
    width: wp("100%"),
    height: hp(80),
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center"
  },
  leftRightIconWrapper: {
    flex: 2,
    alignItems: "center",
    justifyContent: "center"
  },
  leftRightIcon: {
    width: wp(25),
    height: hp(25)
  },
  tabLabel: {
    fontFamily: fonts.primary.regular,
    fontSize: fontSizes.detail.small
  },
  qrIconContainer: {
    flex: 1,
    alignItems: "center"
  },
  qrIconWrapper: {
    width: wp(50),
    height: hp(50),
    borderRadius: 25,
    justifyContent: "center",
    alignItems: "center",
    marginBottom: hp(50),
    marginRight: hp(5)
  },
  qrIcon: {
    width: wp(25),
    height: hp(25)
  }
});

export default styles;
