import React, { Component } from "react";
import {
  View,
  Text,
  ImageBackground,
  Image,
  TouchableOpacity
} from "react-native";
import { observer, inject } from "mobx-react";
import { withNavigation } from "react-navigation";
import { userRole, Images } from "utils/Constants";
import styles from "./styles";

@inject("navbarStore", "dataStore", "userStore")
@observer
class Navbar extends Component {
  goToTab = tab => {
    const { navbarStore, navigation } = this.props;
    navbarStore.setSelectedTab(tab);
    navigation.navigate(navbarStore.selected);
  };

  render() {
    const { navbarStore, dataStore, userStore } = this.props;
    return userStore.user.role === userRole.EMPLOYEE ? (
      <ImageBackground
        source={Images.TAB_BACKGROUND}
        style={styles.container}
        resizeMode="stretch"
      >
        <TouchableOpacity
          style={styles.leftRightIconWrapper}
          onPress={() => this.goToTab("HomeTab")}
        >
          {navbarStore.selected === "HomeTab" ? (
            <Image
              source={Images.RESTAURANT_ACTIVE}
              style={styles.leftRightIcon}
              resizeMode="contain"
            />
          ) : (
            <Image
              source={Images.RESTAURANT_DEACTIVE}
              style={styles.leftRightIcon}
              resizeMode="contain"
            />
          )}
          <Text
            style={[
              styles.tabLabel,
              navbarStore.selected === "HomeTab" && {
                color: dataStore.currentTheme
              }
            ]}
          >
            Restaurants
          </Text>
        </TouchableOpacity>
        <View style={styles.qrIconContainer}>
          {navbarStore.selected !== "QRScanTab" && (
            <TouchableOpacity
              style={[
                styles.qrIconWrapper,
                { backgroundColor: dataStore.currentTheme }
              ]}
              onPress={() => this.goToTab("QRScanTab")}
            >
              <Image
                source={Images.TAB_QRCODE}
                style={styles.qrIcon}
                resizeMode="contain"
              />
            </TouchableOpacity>
          )}
        </View>
        <TouchableOpacity
          style={styles.leftRightIconWrapper}
          onPress={() => this.goToTab("ProfileTab")}
        >
          {navbarStore.selected === "ProfileTab" ? (
            <Image
              source={Images.TAB_PROFILE_DEACTIVE}
              style={styles.leftRightIcon}
              resizeMode="contain"
            />
          ) : (
            <Image
              source={Images.TAB_PROFILE_ACTIVE}
              style={styles.leftRightIcon}
              resizeMode="contain"
            />
          )}
          <Text
            style={[
              styles.tabLabel,
              navbarStore.selected === "ProfileTab" && {
                color: dataStore.currentTheme
              }
            ]}
          >
            Profil
          </Text>
        </TouchableOpacity>
      </ImageBackground>
    ) : (
      <ImageBackground
        source={Images.TAB_BACKGROUND}
        style={styles.container}
        resizeMode="stretch"
      >
        <TouchableOpacity
          style={styles.leftRightIconWrapper}
          onPress={() => this.goToTab("APRHomeTab")}
        >
          {navbarStore.selected === "APRHomeTab" ? (
            <Image
              source={Images.RESTAURANT_APR_ACTIVE}
              style={styles.leftRightIcon}
              resizeMode="contain"
            />
          ) : (
            <Image
              source={Images.RESTAURANT_DEACTIVE}
              style={styles.leftRightIcon}
              resizeMode="contain"
            />
          )}
          <Text
            style={[
              styles.tabLabel,
              navbarStore.selected === "APRHomeTab" && {
                color: dataStore.currentTheme
              }
            ]}
          >
            Restaurant
          </Text>
        </TouchableOpacity>
        <View style={styles.qrIconContainer}>
          {navbarStore.selected !== "APRQRScanTab" && (
            <TouchableOpacity
              style={[
                styles.qrIconWrapper,
                { backgroundColor: dataStore.currentTheme }
              ]}
              onPress={() => this.goToTab("APRQRScanTab")}
            >
              <Image
                source={Images.TAB_QRCODE}
                style={styles.qrIcon}
                resizeMode="contain"
              />
            </TouchableOpacity>
          )}
        </View>
        <TouchableOpacity
          style={styles.leftRightIconWrapper}
          onPress={() => this.goToTab("APRProfileTab")}
        >
          {navbarStore.selected === "APRProfileTab" ? (
            <Image
              source={Images.TAB_PROFILE_APR_ACTIVE}
              style={styles.leftRightIcon}
              resizeMode="contain"
            />
          ) : (
            <Image
              source={Images.TAB_PROFILE_ACTIVE}
              style={styles.leftRightIcon}
              resizeMode="contain"
            />
          )}
          <Text
            style={[
              styles.tabLabel,
              navbarStore.selected === "APRProfileTab" && {
                color: dataStore.currentTheme
              }
            ]}
          >
            Compte
          </Text>
        </TouchableOpacity>
      </ImageBackground>
    );
  }
}
export default withNavigation(Navbar);
