import React from "react";
import { Text } from "react-native";
import { findAll } from "highlight-words-core";
import latinise from "utils/latinise.min";

export default function Highlighter({
  autoEscape,
  highlightStyle,
  searchWords,
  textToHighlight,
  sanitize,
  style,
  ...props
}) {
  const chunks = findAll({
    textToHighlight: latinise(textToHighlight),
    searchWords: [latinise(searchWords[0])],
    sanitize,
    autoEscape
  });

  return (
    <Text style={style} {...props}>
      {chunks.map(chunk => {
        const text = textToHighlight.substr(
          chunk.start,
          chunk.end - chunk.start
        );

        return !chunk.highlight ? (
          text
        ) : (
          <Text
            key={`Highlighter${chunk}`}
            style={chunk.highlight && highlightStyle}
          >
            {text}
          </Text>
        );
      })}
    </Text>
  );
}
