import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  Linking,
  Modal
} from "react-native";
import { observer, inject } from "mobx-react";
import { toMiliseconds } from "utils/Time";
import { SERVER_URL } from "utils/API";
import toastMessageStore from "stores/ToastMessageStore";
import { userRole, errorMessage } from "utils/Constants";
import validate from "utils/Validate";
import styles from "./styles";

const callIcon = require("assets/images/ic_call.png");
const cardIcon = require("assets/images/ic_card_green.png");
const aprCardIcon = require("assets/images/ic_card_orange.png");
const imagePlaceholder = require("assets/images/img_placeholder.png");

@inject("restaurantCardItemStore", "dataStore", "userStore")
@observer
export default class RestaurantCardItem extends Component {
  constructor(props) {
    super(props);

    this.state = {
      phoneModalVisible: false
    };
  }

  // eslint-disable-next-line class-methods-use-this
  get daysInFrench() {
    return [
      "dimanche",
      "lundi",
      "mardi",
      "mercredi",
      "juedi",
      "vendredi",
      "samedi"
    ];
  }

  get remain() {
    const { restaurant } = this.props;
    // const isOpening = true;

    const { isOpening } = restaurant;
    // const isOpening = restaurant.isOpening;
    // const remain = `3:00`;
    const now = new Date();
    const target = toMiliseconds(restaurant.time, isOpening);
    const delta = Math.abs(target - now.getTime());
    const inMinutes = Math.floor(delta / 60000);
    const toDate = new Date(target);
    const isSameDay =
      now.getDay() === toDate.getDay() &&
      now.getMonth() === toDate.getMonth() &&
      now.getFullYear() === toDate.getFullYear();

    let hour = toDate.getHours();
    let minutes = toDate.getMinutes();
    hour = (hour < 10 ? "0" : "") + hour;
    minutes = (minutes < 10 ? "0" : "") + minutes;

    const specificTime = `${hour}:${minutes}`;
    let result = !target ? "Ouvre bientôt" : null;
    if (!result) {
      result = !isOpening ? "Ouvert" : "Ferme";
      if (inMinutes < 60) {
        result = `${result} en ${inMinutes} minutes`;
      } else if (isSameDay) {
        result = `${result} à ${specificTime}`;
      } else {
        result = `${result} le ${
          this.daysInFrench[toDate.getDay()]
        } ${specificTime}`;
      }
    }
    return result;
  }

  handleOnMakeCall = phoneNumber => {
    const phoneNumberList = phoneNumber.filter(phone =>
      validate.isValidPhone(phone)
    );

    switch (phoneNumberList.length) {
      case 0:
        toastMessageStore.toast(errorMessage.PHONE_NOT_AVAILABLE);
        break;
      case 1: {
        const actionURL = `tel:${phoneNumberList[0] || phoneNumberList[1]}`;
        Linking.canOpenURL(actionURL)
          .then(supported => {
            if (!supported) {
              toastMessageStore.toast(errorMessage.PHONE_NOT_AVAILABLE);
            } else {
              return Linking.openURL(actionURL);
            }
            return false;
          })
          .catch(err => console.log(err));
        break;
      }
      case 2:
        this.setState({ phoneModalVisible: true });
        break;
      default:
        break;
    }
  };

  renderModalChoosePhone = phoneNumber => {
    const { phoneModalVisible } = this.state;
    return (
      <Modal
        visible={phoneModalVisible}
        onRequestClose={() => this.setState({ phoneModalVisible: false })}
        animationType="fade"
        transparent
        hardwareAccelerated
      >
        <View style={styles.modalContainer}>
          <View style={styles.modalWrapper}>
            <View style={styles.modalRow}>
              <Text style={styles.modalButtonTitle}>Choose phone number</Text>
            </View>
            <TouchableOpacity
              style={styles.modalButton}
              onPress={() => this.handleOnMakeCall([phoneNumber[0]])}
            >
              <Text style={styles.modalButtonSubTitle}>{phoneNumber[0]}</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.modalButton}
              onPress={() => this.handleOnMakeCall([phoneNumber[0]])}
            >
              <Text style={styles.modalButtonSubTitle}>{phoneNumber[1]}</Text>
            </TouchableOpacity>
            <View style={styles.modalCloseButtonWrapper}>
              <TouchableOpacity
                onPress={() => this.setState({ phoneModalVisible: false })}
                style={styles.modalCloseButton}
              >
                <Text style={styles.modalCloseButtonTitle}>Fermer</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    );
  };

  render() {
    const {
      containerStyle,
      infoContainerStyle,
      nameStyle,
      restaurant,
      userStore
    } = this.props;

    let avatar = null;
    if (restaurant.avatar) {
      avatar = SERVER_URL + restaurant.avatar.path;
    }

    return (
      <View style={[styles.container, containerStyle]}>
        <Image
          source={avatar ? { uri: avatar } : imagePlaceholder}
          style={styles.imageStyle}
          // resizeMode={"contain"}
        />
        <View style={[styles.infoContainer, infoContainerStyle]}>
          <View style={styles.contentContainer}>
            <Text style={[styles.name, nameStyle]}>
              {userStore.user.role === userRole.EMPLOYEE
                ? restaurant.name
                : restaurant.restaurantName}
            </Text>
            <Text style={styles.time}>{this.remain}</Text>
            {restaurant.cardAccepted && (
              <View style={styles.cardRowContainer}>
                <Image
                  source={
                    userStore.user.role === userRole.EMPLOYEE
                      ? cardIcon
                      : aprCardIcon
                  }
                  style={styles.cardIcon}
                  resizeMode="contain"
                />
                <Text style={styles.cardText}>Carte individuelle acceptée</Text>
              </View>
            )}
          </View>
          {userStore.user.role === userRole.EMPLOYEE && (
            <View style={styles.callIconContainer}>
              <TouchableOpacity
                style={styles.callIconWrapper}
                onPress={() => this.handleOnMakeCall(restaurant.phoneNumber)}
              >
                <Image source={callIcon} style={styles.callIcon} />
              </TouchableOpacity>
            </View>
          )}
        </View>
        {userStore.user.role === userRole.EMPLOYEE &&
          this.renderModalChoosePhone(restaurant.phoneNumber)}
      </View>
    );
  }
}
