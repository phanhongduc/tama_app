import { StyleSheet, Dimensions } from "react-native";
import { colors, fontSizes, fonts } from "utils/Constants";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "utils/Styling";

const { width } = Dimensions.get("window");

const styles = StyleSheet.create({
  container: {
    height: hp(65),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    padding: 10,
    paddingLeft: 20,
    paddingRight: 20
  },
  imageStyle: {
    width: wp(65),
    height: hp(65),
    justifyContent: "center",
    alignItems: "center",
    marginRight: wp(10)
  },
  infoContainer: {
    width: "80%",
    flexDirection: "row"
  },
  contentContainer: {
    justifyContent: "center",
    alignItems: "flex-start",
    width: "80%"
  },
  name: {
    fontSize: fontSizes.detail.big,
    fontFamily: fonts.primary.semibold,
    color: colors.text.gray.title
  },
  time: {
    fontSize: fontSizes.detail.small,
    color: colors.text.gray.transaction,
    fontFamily: fonts.primary.medium
  },
  cardRowContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start"
  },
  cardIcon: {
    width: wp(18)
  },
  cardText: {
    fontSize: fontSizes.detail.tiny,
    color: colors.text.gray.tab,
    fontFamily: fonts.primary.regular,
    // fontWeight: "500",
    paddingLeft: 10
  },
  aboutIcon: {
    width: 9,
    height: 9
  },
  callIconContainer: {
    width: "23%",
    height: "100%",
    justifyContent: "center",
    borderLeftWidth: 1,
    borderLeftColor: "#E8E8E8"
  },
  callIconWrapper: {
    flex: 1,
    justifyContent: "center"
  },
  callIcon: {
    marginLeft: "35.65%",
    width: wp(25),
    height: hp(25)
  },
  modalContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: colors.background.overlay
  },
  modalWrapper: {
    width: width * 0.72,
    maxWidth: 300,
    borderRadius: 12,
    height: 180,
    backgroundColor: colors.background.white
  },
  modalRow: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  modalButton: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  modalCloseButton: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  modalCloseButtonWrapper: {
    flex: 1,
    borderTopWidth: 0.5,
    borderTopColor: colors.text.gray.border
  },
  modalButtonTitle: {
    color: colors.text.black,
    fontSize: fontSizes.title,
    fontFamily: fonts.secondary.medium,
    textAlign: "center"
  },
  modalButtonSubTitle: {
    color: colors.text.black,
    fontSize: fontSizes.detail.big2,
    fontFamily: fonts.secondary.medium,
    textAlign: "center"
  },
  modalCloseButtonTitle: {
    color: colors.text.blue,
    fontSize: fontSizes.detail.bigger,
    fontFamily: fonts.secondary.medium,
    textAlign: "center"
  }
});

export default styles;
