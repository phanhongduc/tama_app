import { StyleSheet, Dimensions } from "react-native";
import { fontSizes, fonts, colors } from "utils/Constants";

const { height, width } = Dimensions.get("window");

const styles = StyleSheet.create({
  modalContainer: {
    height,
    width,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: colors.background.overlay
  },
  modalWrapper: {
    width: width * 0.72,
    maxWidth: 300,
    borderRadius: 12,
    backgroundColor: colors.background.white
  },
  contentWrapper: {
    justifyContent: "center",
    alignItems: "center",
    paddingVertical: 30
  },
  title: {
    color: colors.text.gray.detail,
    fontSize: fontSizes.detail.bigger,
    fontFamily: fonts.primary.medium,
    textAlign: "center",
    width: "90%"
  },
  subTitle: {
    color: colors.text.black,
    fontSize: fontSizes.detail.bigger,
    fontFamily: fonts.primary.regular
  },
  modalButtonWrapper: {
    width: "100%",
    flexDirection: "row",
    borderTopWidth: 1,
    borderTopColor: colors.text.gray.buttonRefulser,
    height: 50
  },
  modalButton: {
    width: "100%",
    justifyContent: "center",
    alignItems: "center"
  },
  rightVerticalRule: {
    borderRightWidth: 1,
    borderRightColor: colors.text.gray.border
  },
  modalButtonTitle: {
    color: colors.text.blue,
    fontSize: fontSizes.detail.bigger,
    fontFamily: fonts.secondary.medium
  }
});

export default styles;
