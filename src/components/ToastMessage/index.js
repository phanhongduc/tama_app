import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  Modal,
  TouchableWithoutFeedback
} from "react-native";
import { observer, inject } from "mobx-react";
import Validate from "utils/Validate";
import styles from "./styles";

@inject("toastMessageStore")
@observer
export default class ToastMessage extends Component {
  render() {
    const { toastMessageStore } = this.props;

    return (
      <Modal
        visible={toastMessageStore.showModal}
        animationType="fade"
        transparent
        hardwareAccelerated
        onRequestClose={toastMessageStore.closeModal}
      >
        <TouchableWithoutFeedback onPress={toastMessageStore.closeModal}>
          <View style={styles.modalContainer}>
            <View style={styles.modalWrapper}>
              <View style={styles.contentWrapper}>
                <Text style={styles.title}>{toastMessageStore.title}</Text>
                {!Validate.isEmpty(toastMessageStore.subTitle) && (
                  <Text style={styles.subTitle}>
                    {toastMessageStore.subTitle}
                  </Text>
                )}
              </View>
              <View style={styles.modalButtonWrapper}>
                <TouchableOpacity
                  style={styles.modalButton}
                  onPress={toastMessageStore.closeModal}
                >
                  <Text style={styles.modalButtonTitle}>OK</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    );
  }
}
