import { StyleSheet } from "react-native";
import { fonts } from "utils/Constants";
import dataStore from "stores/DataStore";

const styles = StyleSheet.create({
  container: {
    // height: height * 0.355
    width: "80%",
    aspectRatio: 4 / 3
  },
  row: {
    flexDirection: "row",
    alignItems: "center",
    height: "25%",
    justifyContent: "space-between"
  },
  numWrapper: {
    height: "100%",
    width: "20%",
    justifyContent: "center",
    alignItems: "center"
  },
  numText: {
    fontSize: 45,
    color: dataStore.currentTheme,
    fontFamily: fonts.primary.regular
  }
});

export default styles;
