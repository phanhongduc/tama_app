import React, { Component } from "react";
import { View, TouchableOpacity } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { observer, inject } from "mobx-react";
import styles from "./styles";

@inject("headerLeftStore")
@observer
export default class HeaderLeft extends Component {
  render() {
    const {
      goBack,
      headerLeftStore: { visible }
    } = this.props;

    return (
      <View style={styles.container}>
        {visible && (
          <TouchableOpacity style={styles.iconContainer} onPress={goBack}>
            <Icon name="chevron-left" size={35} style={styles.icon} />
          </TouchableOpacity>
        )}
      </View>
    );
  }
}
