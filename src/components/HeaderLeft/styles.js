import { StyleSheet } from "react-native";
import { colors } from "utils/Constants";
import { widthPercentageToDP as wp } from "utils/Styling";

const styles = StyleSheet.create({
  container: {
    marginLeft: wp(20)
  },
  iconContainer: {
    width: wp(50),
    height: "100%",
    justifyContent: "center",
    alignItems: "center"
  },
  icon: {
    color: colors.text.white
  }
});

export default styles;
