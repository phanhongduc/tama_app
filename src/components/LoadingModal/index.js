import React, { Component } from "react";
import { View, Modal, ActivityIndicator } from "react-native";
import { observer } from "mobx-react";
import { colors } from "utils/Constants";

@observer
export default class LoadingModal extends Component {
  render() {
    const { isLoading } = this.props;
    return (
      <Modal
        visible={isLoading}
        style={{ flex: 1 }}
        animationType="fade"
        onRequestClose={() => {}}
        transparent
      >
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: colors.background.overlay
          }}
        >
          <ActivityIndicator size="large" />
        </View>
      </Modal>
    );
  }
}
