import React, { Component } from "react";
import { Text, TouchableOpacity } from "react-native";
import { observer } from "mobx-react";
import styles from "./styles";

@observer
export default class Button extends Component {
  render() {
    const { title, style, labelStyle, onPress, disabled } = this.props;
    const buttonStyle = [styles.container, style];
    const textStyle = [styles.label, labelStyle];

    return (
      <TouchableOpacity
        style={buttonStyle}
        onPress={onPress}
        disabled={disabled}
      >
        <Text style={textStyle}>{title}</Text>
      </TouchableOpacity>
    );
  }
}
