import { StyleSheet, Dimensions } from "react-native";
import { fontSizes, fonts, colors } from "utils/Constants";

const { width } = Dimensions.get("window");

const styles = StyleSheet.create({
  container: {
    width: width * 0.67,
    aspectRatio: 175 / 52,
    maxHeight: 52,
    borderRadius: 26,
    justifyContent: "center",
    alignItems: "center"
  },
  label: {
    color: colors.text.white,
    fontFamily: fonts.primary.medium,
    fontSize: fontSizes.button.huge,
    textAlign: "center"
  }
});

export default styles;
