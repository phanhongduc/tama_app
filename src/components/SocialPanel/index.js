import React, { Component } from "react";
import {
  View,
  TouchableOpacity,
  Image,
  Linking,
  Platform,
  Text
} from "react-native";
import { inject } from "mobx-react";
import { withNavigation } from "react-navigation";
import MapboxGL from "@mapbox/react-native-mapbox-gl";
import _ from "lodash";
import { userRole, colors, mapToken, mailUrl } from "utils/Constants";
import Validate from "utils/Validate";
import styles from "./styles";
import { Images } from "../../utils/Constants";

@inject("homepageStore", "userStore")
class SocialPanel extends Component {
  constructor(props) {
    super(props);
    MapboxGL.setAccessToken(mapToken);
  }

  getCoordinates = restaurant => {
    const { homepageStore } = this.props;
    return _.find(homepageStore.listAnnotations, function(item) {
      return item.id === restaurant.id;
    });
  };

  handleOnPressInstagram = instagramURL => {
    if (!instagramURL) return;
    Linking.canOpenURL(instagramURL)
      .then(supported => {
        if (supported) {
          Linking.openURL(instagramURL);
        }
      })
      .catch(error => console.log(error));
  };

  handleOnPressFacebook = facebookURL => {
    if (!facebookURL) return;
    Linking.canOpenURL(facebookURL)
      .then(supported => {
        if (supported) {
          Linking.openURL(facebookURL);
        }
      })
      .catch(error => console.log(error));
  };

  handleOnPressMail = mail => {
    if (!mail) return;
    Linking.canOpenURL(`mailto:${mail}`)
      .then(supported => {
        if (supported) {
          Linking.openURL(`mailto:${mail}`);
        } else {
          Linking.openURL(mailUrl);
        }
      })
      .catch(error => console.log(error));
  };

  handleOnPressLocation = restaurant => {
    const { userStore, navigation, homepageStore } = this.props;
    if (userStore.user.role === userRole.RESTAURANT) {
      const scheme = Platform.select({
        ios: "maps:0,0?q=",
        android: "geo:0,0?q="
      });
      const latLng = `${restaurant.latitude},${restaurant.longitude}`;
      const label = restaurant.restaurantName;
      const url = Platform.select({
        ios: `${scheme}${label}@${latLng}`,
        android: `${scheme}${latLng}(${label})`
      });
      Linking.canOpenURL(url)
        .then(supported => {
          if (supported) {
            Linking.openURL(url);
          }
        })
        .catch(error => console.log(error));
    } else if (userStore.user.role === userRole.EMPLOYEE) {
      navigation.navigate("Map");
      const coordinates = this.getCoordinates(restaurant);
      homepageStore.moveToMarker(
        coordinates.coordinate[0],
        coordinates.coordinate[1]
      );
    }
  };

  renderMarkerStaticMap = restaurant => {
    const { isRestaurant } = this.props;
    const coordinates = this.getCoordinates(restaurant);
    return (
      <MapboxGL.PointAnnotation
        key={restaurant.id}
        id={restaurant.id.toString()}
        title={restaurant.restaurantName}
        coordinate={
          isRestaurant
            ? [restaurant.longitude, restaurant.latitude]
            : [coordinates.coordinate[0], coordinates.coordinate[1]]
        }
      >
        <Image
          source={Images.IC_MARKER_OPEN}
          style={styles.pinIconStyle}
          resizeMode="contain"
        />
      </MapboxGL.PointAnnotation>
    );
  };

  renderStaticPhoto = restaurant => {
    const { mode, isRestaurant } = this.props;
    const coordinates = this.getCoordinates(restaurant);
    const validatedEmail = Validate.isValidField(restaurant.email);
    const validatedFacebook = Validate.isValidField(restaurant.facebook);
    const validatedInstagram = Validate.isValidField(restaurant.instagram);

    if (
      (validatedEmail && validatedFacebook && validatedInstagram) ||
      (validatedEmail && validatedFacebook) ||
      (validatedEmail && validatedInstagram) ||
      (validatedFacebook && validatedInstagram)
    ) {
      return (
        <View
          style={[
            styles.socialAction,
            { backgroundColor: colors.background.gray.bar }
          ]}
        >
          <TouchableOpacity
            style={styles.iconSocialWrapper}
            onPress={() => this.handleOnPressLocation(restaurant)}
          >
            <Image
              source={Images.IC_PROFILE_LOCATION}
              resizeMode="contain"
              style={styles.locationIconStyle}
            />
          </TouchableOpacity>
          {validatedEmail && (
            <TouchableOpacity
              style={styles.iconSocialWrapper}
              onPress={() => this.handleOnPressMail(restaurant.email)}
            >
              <Image
                source={Images.IC_PROFILE_MAIL}
                resizeMode="contain"
                style={styles.mailIconStyle}
              />
            </TouchableOpacity>
          )}
          {validatedFacebook && (
            <TouchableOpacity
              style={styles.iconSocialWrapper}
              onPress={() => this.handleOnPressFacebook(restaurant.facebook)}
            >
              <Image
                source={Images.IC_PROFILE_FACEBOOK}
                resizeMode="contain"
                style={styles.facebookIconStyle}
              />
            </TouchableOpacity>
          )}
          {validatedInstagram && (
            <TouchableOpacity
              style={styles.iconSocialWrapper}
              onPress={() => this.handleOnPressInstagram(restaurant.instagram)}
            >
              <Image
                source={Images.IC_PROFILE_INSTAGRAM}
                resizeMode="contain"
                style={styles.instagramIconStyle}
              />
            </TouchableOpacity>
          )}
        </View>
      );
    }
    if (validatedInstagram && !validatedEmail && !validatedFacebook) {
      return (
        <View style={styles.socialViewWrapper}>
          {mode === "detail" ? (
            <MapboxGL.MapView
              style={styles.socialViewWrapper}
              ref={c => {
                this.map = c;
              }}
              centerCoordinate={
                isRestaurant
                  ? [restaurant.longitude, restaurant.latitude]
                  : [coordinates.coordinate[0], coordinates.coordinate[1]]
              }
              attributionEnabled={false}
              logoEnabled={false}
              rotateEnabled={false}
              scrollEnabled={false}
              zoomEnabled={false}
              zoomLevel={14}
            >
              {this.renderMarkerStaticMap(restaurant)}
            </MapboxGL.MapView>
          ) : (
            <View style={styles.emptyViewWrapper} />
          )}
          <View
            style={[
              styles.socialAction,
              styles.twoIconsWrapper,
              styles.absoluteWrapper
            ]}
          >
            <TouchableOpacity
              style={styles.iconSocialWrapper}
              onPress={() => this.handleOnPressInstagram(restaurant.instagram)}
            >
              <Image
                source={Images.IC_PROFILE_INSTAGRAM}
                resizeMode="contain"
                style={styles.instagramIconStyle}
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.iconSocialWrapper}
              onPress={() => this.handleOnPressLocation(restaurant)}
            >
              <Image
                source={Images.IC_CHEVRON_RIGHT}
                resizeMode="contain"
                style={styles.chevronIconStyle}
              />
            </TouchableOpacity>
          </View>
        </View>
      );
    }
    if (validatedFacebook && !validatedEmail && !validatedInstagram) {
      return (
        <View style={styles.socialViewWrapper}>
          {mode === "detail" ? (
            <MapboxGL.MapView
              style={styles.socialViewWrapper}
              ref={c => {
                this.map = c;
              }}
              centerCoordinate={
                isRestaurant
                  ? [restaurant.longitude, restaurant.latitude]
                  : [coordinates.coordinate[0], coordinates.coordinate[1]]
              }
              attributionEnabled={false}
              logoEnabled={false}
              rotateEnabled={false}
              scrollEnabled={false}
              zoomEnabled={false}
              zoomLevel={15}
            >
              {this.renderMarkerStaticMap(restaurant)}
            </MapboxGL.MapView>
          ) : (
            <View style={styles.emptyViewWrapper} />
          )}
          <View
            style={[
              styles.socialAction,
              styles.twoIconsWrapper,
              styles.absoluteWrapper
            ]}
          >
            <TouchableOpacity
              style={styles.iconSocialWrapper}
              onPress={() => this.handleOnPressFacebook(restaurant.facebook)}
            >
              <Image
                source={Images.IC_PROFILE_FACEBOOK}
                resizeMode="contain"
                style={styles.facebookIconStyle}
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.iconSocialWrapper}
              onPress={() => this.handleOnPressLocation(restaurant)}
            >
              <Image
                source={Images.IC_CHEVRON_RIGHT}
                resizeMode="contain"
                style={styles.chevronIconStyle}
              />
            </TouchableOpacity>
          </View>
        </View>
      );
    }
    if (validatedEmail && !validatedFacebook && !validatedInstagram) {
      return (
        <View style={styles.socialViewWrapper}>
          {mode === "detail" ? (
            <MapboxGL.MapView
              style={styles.socialViewWrapper}
              ref={c => {
                this.map = c;
              }}
              centerCoordinate={
                isRestaurant
                  ? [restaurant.longitude, restaurant.latitude]
                  : [coordinates.coordinate[0], coordinates.coordinate[1]]
              }
              attributionEnabled={false}
              logoEnabled={false}
              rotateEnabled={false}
              scrollEnabled={false}
              zoomEnabled={false}
              zoomLevel={15}
            >
              {this.renderMarkerStaticMap(restaurant)}
            </MapboxGL.MapView>
          ) : (
            <View style={styles.emptyViewWrapper} />
          )}
          <View
            style={[
              styles.socialAction,
              styles.twoIconsWrapper,
              styles.absoluteWrapper
            ]}
          >
            <TouchableOpacity
              style={styles.iconSocialWrapper}
              onPress={() => this.handleOnPressMail(restaurant.email)}
            >
              <Image
                source={Images.IC_PROFILE_MAIL}
                resizeMode="contain"
                style={styles.mailIconStyle}
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.iconSocialWrapper}
              onPress={() => this.handleOnPressLocation(restaurant)}
            >
              <Image
                source={Images.IC_CHEVRON_RIGHT}
                resizeMode="contain"
                style={styles.chevronIconStyle}
              />
            </TouchableOpacity>
          </View>
        </View>
      );
    }
    return (
      <View style={styles.socialViewWrapper}>
        <Text>No Information</Text>
      </View>
    );
  };

  render() {
    const { restaurant } = this.props;
    return this.renderStaticPhoto(restaurant);
  }
}

export default withNavigation(SocialPanel);
