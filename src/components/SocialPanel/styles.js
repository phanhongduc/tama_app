import { StyleSheet } from "react-native";
import { colors } from "utils/Constants";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "utils/Styling";

const styles = StyleSheet.create({
  socialAction: {
    flexDirection: "row",
    justifyContent: "space-evenly",
    alignItems: "center",
    height: hp(100)
  },
  iconSocialWrapper: {
    height: "50%",
    aspectRatio: 1 / 1,
    justifyContent: "center",
    alignItems: "center"
  },
  twoIconsWrapper: {
    justifyContent: "space-between",
    paddingLeft: wp("10%"),
    paddingRight: wp("10%")
  },
  absoluteWrapper: { position: "absolute", width: "100%" },
  pinIconStyle: { width: 30, height: 30 },
  locationIconStyle: { width: wp(20) },
  mailIconStyle: { width: wp(28) },
  facebookIconStyle: { width: wp(21) },
  instagramIconStyle: { width: wp(22) },
  socialViewWrapper: { height: 100 },
  emptyViewWrapper: {
    height: 100,
    backgroundColor: colors.background.gray.bar
  },
  chevronIconStyle: { width: wp(15) }
});

export default styles;
