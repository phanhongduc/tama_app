import { StyleSheet, Dimensions } from "react-native";
import { Header } from "react-navigation";
import { colors, fontSizes, fonts } from "utils/Constants";
import { heightPercentageToDP as hp } from "utils/Styling";

const { height, width } = Dimensions.get("window");
const mainScreenHeight = height - Header.HEIGHT;

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "yellow",
    zIndex: 25
  },
  container: {
    flex: 1,
    alignItems: "center"
  },
  dragHandler: {
    height: 40,
    width,
    alignItems: "center",
    justifyContent: "center",
    position: "absolute",
    top: -10,
    left: 0,
    zIndex: 1,
    backgroundColor: colors.background.white,
    borderTopLeftRadius: 12,
    borderTopRightRadius: 12
  },
  icon: {
    height: 40,
    width: 40,
    justifyContent: "center",
    alignItems: "center"
  },
  previewContainer: {
    paddingTop: mainScreenHeight * 0.056,
    backgroundColor: colors.background.white,
    width,
    flex: 1,
    alignItems: "center",
    paddingBottom: 50
  },
  cardContainer: {
    height: mainScreenHeight * 0.1
  },
  cardInfoContainer: {
    height: mainScreenHeight * 0.08
  },
  nameStyle: {
    fontSize: fontSizes.header,
    fontFamily: fonts.primary.bold
  },
  blankArea: {
    paddingBottom: hp(150),
    backgroundColor: colors.background.gray.bar
  }
});

export default styles;
