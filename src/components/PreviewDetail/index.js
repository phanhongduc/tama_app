/* eslint-disable react/jsx-props-no-spreading */
import React, { Component } from "react";
import { View, Dimensions, Animated } from "react-native";
import { observer } from "mobx-react";
import SlidingUpPanel from "rn-sliding-up-panel";
import { Header } from "react-navigation";
import { getStatusBarHeight } from "react-native-status-bar-height";
import Icon from "react-native-vector-icons/Feather";
import RestaurantDetail from "components/RestaurantDetail";
import RestaurantCardItem from "components/RestaurantCardItem";
import { colors, isIOS } from "utils/Constants";
import styles from "./styles";

const { height } = Dimensions.get("window");
const statusBar = getStatusBarHeight();
const top = isIOS
  ? height - Header.HEIGHT - statusBar - 50
  : height - Header.HEIGHT - statusBar - 60;
const mainScreenHeight = height - Header.HEIGHT;
const bottom = mainScreenHeight * 0.15 + 120; // Height of bottom tab is 120

@observer
export default class PreviewDetailRestaurant extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isShow: true,
      isShowDetail: false
    };
    this.animatedValue = new Animated.Value(0);
  }

  componentDidMount() {
    this.animatedValue.addListener(this.onAnimatedValueChange);
  }

  componentWillUnmount() {
    this.animatedValue.removeListener(this.onAnimatedValueChange);
  }

  show = () => {
    this.panel.show();
  };

  hide = () => {
    this.panel.hide();
  };

  onAnimatedValueChange = ({ value }) => {
    const { isShow, isShowDetail } = this.state;
    switch (true) {
      case value === top && isShow:
        this.setState({ isShow: false });
        break;
      case value === bottom && (!isShow || isShowDetail):
        this.setState({ isShow: true, isShowDetail: false });
        break;
      case value > bottom && !isShowDetail:
        this.setState({ isShowDetail: true });
        break;
      default:
        break;
    }
  };

  render() {
    const { isShow, isShowDetail } = this.state;
    const { restaurant, theme } = this.props;

    return (
      <SlidingUpPanel
        ref={c => {
          this.panel = c;
        }}
        draggableRange={{ top, bottom }}
        animatedValue={this.animatedValue}
      >
        {dragHandlers => (
          <View style={styles.container}>
            <View {...dragHandlers} style={styles.dragHandler}>
              {!isShow ? (
                <View style={styles.icon} onPress={this.hide}>
                  <Icon
                    name="chevron-down"
                    size={22}
                    color={colors.text.gray.buttonRefulser}
                  />
                </View>
              ) : (
                <View style={styles.icon} onPress={this.show}>
                  <Icon
                    name="chevron-up"
                    size={22}
                    color={colors.text.gray.buttonRefulser}
                  />
                </View>
              )}
            </View>
            {isShowDetail ? (
              <RestaurantDetail
                restaurant={restaurant}
                theme={theme}
                mode="preview"
              />
            ) : (
              <View style={styles.previewContainer}>
                <RestaurantCardItem
                  containerStyle={styles.cardContainer}
                  infoContainerStyle={styles.cardInfoContainer}
                  nameStyle={styles.nameStyle}
                  restaurant={restaurant}
                />
              </View>
            )}
            <View style={styles.blankArea} />
          </View>
        )}
      </SlidingUpPanel>
    );
  }
}
