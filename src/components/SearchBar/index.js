import React, { Component } from "react";
import {
  TouchableOpacity,
  Keyboard,
  TextInput,
  Image,
  View,
  FlatList
} from "react-native";
import { observer, inject } from "mobx-react";
import IconIonic from "react-native-vector-icons/Ionicons";
import Highlighter from "components/Highlighter";
import { colors, infoMessage, isAndroid, Images } from "utils/Constants";
import styles from "./styles";

@inject("searchbarStore", "restaurantStore", "homepageStore")
@observer
export default class SearchBar extends Component {
  onMoveToMarkerHandle = (lng, lat, id, restaurantName) => {
    const { moveToMarker, searchbarStore } = this.props;
    searchbarStore.hideSearchResult();
    this.hideKeyboad();
    moveToMarker(lng, lat, id, restaurantName);
  };

  onMoveToIslandHandle = (lng, lat) => {
    const { moveToMarker, searchbarStore } = this.props;
    searchbarStore.hideSearchResult();
    this.hideKeyboad();
    moveToMarker(lng, lat);
  };

  renderResultItem = ({ item }) => {
    const { searchbarStore, restaurantStore, homepageStore } = this.props;
    const textToHighlight = `${item.restaurantName} ${item.address}`;
    const islandHighlight = `${item.islandName}`;
    return (
      <TouchableOpacity
        style={styles.suggestItem}
        onPress={() => {
          if (item.island === true) {
            searchbarStore.setSearchText(islandHighlight);
            searchbarStore.saveHistory(item);
            this.onMoveToIslandHandle(item.longitude, item.latitude);
            homepageStore.onAnnotationDeselected();
          } else {
            searchbarStore.setSearchText(textToHighlight);
            searchbarStore.saveHistory(item);
            this.onMoveToMarkerHandle(
              item.longitude,
              item.latitude,
              item.id,
              item.restaurantName
            );
            const index = restaurantStore.listRestaurants.findIndex(
              res => res.id === item.id
            );
            homepageStore.onAnnotationSelected(index);
          }
        }}
      >
        <View style={styles.suggestItemWrapper}>
          <IconIonic name="ios-pin" size={22} style={styles.suggestItemIcon} />
          <Highlighter
            style={styles.itemText}
            highlightStyle={styles.highlightText}
            searchWords={[searchbarStore.searchTextMap]}
            textToHighlight={
              item.island === true ? islandHighlight : textToHighlight
            }
            ellipsizeMode="tail"
            numberOfLines={1}
          />
        </View>
      </TouchableOpacity>
    );
  };

  keyExtractor = (item, index) => index.toString();

  onSubmitEditing = () => {
    const { searchbarStore } = this.props;
    this.hideKeyboad();
    if (!searchbarStore.searchTextMap) {
      searchbarStore.hideSearchResult();
    }
  };

  // eslint-disable-next-line class-methods-use-this
  hideKeyboad() {
    Keyboard.dismiss();
  }

  renderListResult = () => {
    const {
      searchbarStore: { hideResult, searchResultMap, searchHistory }
    } = this.props;

    let result = null;
    if (!hideResult) {
      const data = searchResultMap.length ? searchResultMap : searchHistory;
      result = (
        <FlatList
          renderItem={this.renderResultItem}
          keyExtractor={this.keyExtractor}
          data={data}
          removeClippedSubviews
          keyboardShouldPersistTaps="handled"
        />
      );
    }

    return result;
  };

  render() {
    const maxListSuggestHeight = 4 * 45;
    const { searchbarStore } = this.props;

    return (
      <View style={styles.searchBarContainer}>
        <View style={styles.searchInputContainer}>
          <IconIonic
            name="ios-search"
            color={colors.text.gray.icon}
            size={25}
            style={styles.searchIcon}
          />
          <TextInput
            value={searchbarStore.searchTextMap}
            autoCorrect={false}
            autoCapitalize="none"
            keyboardType={isAndroid ? "visible-password" : "default"}
            placeholder={infoMessage.SEARCH_PLACEHOLDER}
            placeholderTextColor={colors.text.gray.icon}
            style={styles.inputStyle}
            onChange={({ nativeEvent }) => {
              searchbarStore.searchMap(nativeEvent.text);
            }}
            onFocus={searchbarStore.showSearchResult}
            blurOnSubmit={false}
            returnKeyType="search"
            onSubmitEditing={this.onSubmitEditing}
            clearButtonMode="always"
          />
          {isAndroid && searchbarStore.searchTextMap !== "" && (
            <TouchableOpacity onPress={() => searchbarStore.clearInputMap()}>
              <Image
                source={Images.IC_INPUT_CLOSE}
                resizeMode="contain"
                style={styles.clearTextIcon}
              />
            </TouchableOpacity>
          )}
        </View>
        <View
          style={[
            styles.suggestItemContainer,
            { maxHeight: maxListSuggestHeight }
          ]}
        >
          {this.renderListResult()}
        </View>
      </View>
    );
  }
}
