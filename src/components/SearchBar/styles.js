import { StyleSheet } from "react-native";
import { colors, fontSizes, fonts, isIOS } from "utils/Constants";
import { widthPercentageToDP as wp } from "utils/Styling";

const styles = StyleSheet.create({
  searchBarContainer: {
    width: "100%",
    zIndex: 1
  },
  searchInputContainer: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    height: 50,
    backgroundColor: colors.background.white,
    width: "100%"
  },
  inputStyle: {
    fontFamily: fonts.primary.regular,
    fontSize: fontSizes.detail.big2,
    width: isIOS ? wp("80%") : wp("75%"),
    paddingTop: 15,
    paddingBottom: 15
  },
  searchIcon: {
    paddingLeft: 20,
    paddingRight: 20
  },
  itemText: {
    paddingTop: 10,
    paddingBottom: 10,
    paddingRight: 20,
    margin: 2,
    fontFamily: fonts.primary.regular,
    fontSize: fontSizes.detail.big2,
    color: colors.text.gray.icon
  },
  highlightText: {
    color: colors.text.gray.border,
    fontFamily: fonts.primary.semibold
  },
  resultContainer: {
    backgroundColor: colors.background.white,
    maxHeight: 200
  },
  suggestItemContainer: {
    backgroundColor: colors.background.white
  },
  suggestItemWrapper: {
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 10,
    marginLeft: 15
  },
  suggestItem: {
    height: 45,
    borderTopWidth: 1,
    borderTopColor: colors.text.grayWhite
  },
  suggestItemIcon: {
    color: colors.text.gray.icon,
    marginRight: 10
  },
  clearTextIcon: { width: 20, height: 20, marginRight: 20 },
  backIcon: {
    width: 20,
    height: 20,
    marginLeft: 10
  }
});

export default styles;
