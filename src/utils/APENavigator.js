import React from "react";
import {
  createStackNavigator,
  createMaterialTopTabNavigator,
  createBottomTabNavigator,
  NavigationActions,
  StackActions
} from "react-navigation";
// -------APE Main Stack-------
import MapScreen from "screens/Main/HomeTab/Map";
import RestaurantsScreen from "screens/Main/HomeTab/Restaurants";
import RestaurantDetailScreen from "screens/Main/HomeTab/RestaurantDetail";
import PreviewDetailRestaurant from "components/PreviewDetail";
import QRScanScreen from "screens/Main/QRScanTab/QRScan";
import PaymentAmountScreen from "screens/Main/QRScanTab/PaymentAmount";
import PaymentPincodeScreen from "screens/Main/QRScanTab/PaymentPincode";
import ProgressScreen from "screens/Main/QRScanTab/Progress";
import PaymentSuccessScreen from "screens/Main/QRScanTab/PaymentSuccess";
import PaymentFailureScreen from "screens/Main/QRScanTab/PaymentFailure";
import ProfileScreen from "screens/Main/ProfileTab/Profile";
import SettingScreen from "screens/Main/ProfileTab/Setting";
import ChangePincodeScreen from "screens/Main/ProfileTab/ChangePincode";
import MyQRCodeScreen from "screens/Main/ProfileTab/MyQRCode";
import HistoryScreen from "screens/Main/ProfileTab/History";
import MesInfosScreen from "screens/Main/ProfileTab/MesInfos";
import ContactUsScreen from "screens/Main/ProfileTab/ContactUs";
import HelpScreen from "screens/Main/ProfileTab/Help";
import AboutUsScreen from "screens/Main/ProfileTab/AboutUs";

// import components
import Navbar from "components/Navbar";
import HeaderLeft from "components/HeaderLeft";
import HeaderTitle from "components/HeaderTitle";
// import constants
import { fonts, colors, fontSizes } from "utils/Constants";
// import stores
import paymentPincodeStore from "stores/PaymentPincodeStore";
import headerTitleStore from "stores/HeaderTitleStore";
import navbarStore from "stores/NavbarStore";
import qrScanStore from "stores/QRScanStore";
import dataStore from "stores/DataStore";

const MapStack = createStackNavigator(
  {
    Map: {
      screen: MapScreen
    },
    RestaurantDetail: {
      screen: RestaurantDetailScreen
    },
    Preview: {
      screen: PreviewDetailRestaurant
    }
  },
  {
    initialRouteName: "Map",
    headerMode: "none"
  }
);

const RestaurantsStack = createStackNavigator(
  {
    Liste: {
      screen: RestaurantsScreen
    },
    RestaurantDetail: {
      screen: RestaurantDetailScreen
    }
  },
  {
    initialRouteName: "Liste",
    headerMode: "none"
  }
);

const MainHomeStack = createMaterialTopTabNavigator(
  {
    Map: {
      screen: MapStack
    },
    Liste: {
      screen: RestaurantsStack
    }
  },
  {
    initialRouteName: "Map",
    headerBackTitle: null,
    tabBarOptions: {
      upperCaseLabel: false,
      style: {
        backgroundColor: dataStore.currentTheme,
        shadowOffset: { width: 0, height: 0 },
        elevation: 0,
        borderBottomColor: colors.border
      },
      labelStyle: {
        fontFamily: fonts.primary.medium,
        fontSize: fontSizes.tab,
        color: colors.text.white
      },
      tabStyle: {
        borderColor: colors.border
      },
      indicatorStyle: {
        backgroundColor: colors.border,
        height: 7
      }
    },
    swipeEnabled: false
    // optimizationsEnabled: true,
  }
);

RestaurantsStack.navigationOptions = ({ navigation }) => {
  const { routeName } = navigation.state.routes[navigation.state.index];
  const tabBarVisible = routeName === "Liste";

  return {
    tabBarVisible
  };
};

MapStack.navigationOptions = ({ navigation }) => {
  const { routeName } = navigation.state.routes[navigation.state.index];
  const tabBarVisible = routeName === "Map";

  return {
    tabBarVisible
  };
};

const HomeStack = createStackNavigator(
  {
    MainHome: {
      screen: MainHomeStack
    }
  },
  {
    initialRouteName: "MainHome",
    defaultNavigationOptions: ({ navigation }) => {
      let headerLeft = null;
      const parentRoute = navigation.state.routes[navigation.state.index];
      if (
        parentRoute.routes &&
        parentRoute.routes[parentRoute.index].routeName === "RestaurantDetail"
      ) {
        headerLeft = (
          <HeaderLeft
            goBack={() => {
              headerTitleStore.setTitle(null);
              navigation.goBack(null);
            }}
          />
        );
      }
      return {
        headerTitle: <HeaderTitle />,
        headerBackTitle: null,
        headerTintColor: colors.text.white,
        headerTitleStyle: {
          fontFamily: fonts.primary.semibold,
          fontSize: fontSizes.header,
          color: colors.text.white
        },
        headerLeft,
        headerStyle: {
          backgroundColor: dataStore.currentTheme,
          shadowOffset: { width: 0, height: 0 },
          elevation: 0,
          borderBottomWidth: 0
        }
      };
    },
    headerLayoutPreset: "center"
  }
);

const QRStack = createStackNavigator(
  {
    QRScan: {
      screen: QRScanScreen
    },
    PaymentAmount: {
      screen: PaymentAmountScreen
    },
    PaymentPincode: {
      screen: PaymentPincodeScreen,
      navigationOptions: {
        gesturesEnabled: false
      }
    },
    Progress: {
      screen: ProgressScreen,
      navigationOptions: {
        gesturesEnabled: false
      }
    },
    PaymentSuccess: {
      screen: PaymentSuccessScreen,
      navigationOptions: {
        gesturesEnabled: false
      }
    },
    PaymentFailure: {
      screen: PaymentFailureScreen,
      navigationOptions: {
        gesturesEnabled: false
      }
    }
  },
  {
    initialRouteName: "QRScan",
    defaultNavigationOptions: {
      headerBackTitle: null,
      headerStyle: {
        backgroundColor: dataStore.currentTheme,
        shadowOffset: { width: 0, height: 0 },
        elevation: 0
      },
      headerTintColor: colors.text.white,
      headerTitleStyle: {
        fontFamily: fonts.primary.semibold,
        fontSize: fontSizes.header,
        color: colors.text.white
      },
      tabBarVisible: false
    },
    headerLayoutPreset: "center"
  }
);

QRStack.navigationOptions = ({ navigation }) => {
  const tabBarVisible =
    navigation.state.routes[navigation.state.index].routeName === "QRScan";

  return {
    tabBarVisible
  };
};

const defaultGetStateForActionQRStack = QRStack.router.getStateForAction;
QRStack.router.getStateForAction = (action, state) => {
  if (!state || action.type !== NavigationActions.BACK) {
    return defaultGetStateForActionQRStack(action, state);
  }

  const { routeName } = state.routes[state.index];
  switch (routeName) {
    case "QRScan":
      navbarStore.setSelectedTab("HomeTab");
      return defaultGetStateForActionQRStack(action, state);
    case "PaymentAmount":
      qrScanStore.setIsProcessing(false);
      return defaultGetStateForActionQRStack(action, state);
    case "PaymentPincode":
      paymentPincodeStore.toggleCancelModal();
      return null;
    case "Progress":
      return null;
    case "PaymentSuccess":
    case "PaymentFailure": {
      const { navigation } = state.routes[state.index].params;
      navigation.popToTop();
      navbarStore.setSelectedTab("HomeTab");
      qrScanStore.setIsProcessing(false);
      const resetAction = StackActions.reset({
        key: "HomeTab",
        index: 0,
        actions: [NavigationActions.navigate({ routeName: "MainHome" })]
      });
      navigation.dispatch(resetAction);
      return null;
    }
    default:
      return defaultGetStateForActionQRStack(action, state);
  }
};

const ProfileStack = createStackNavigator(
  {
    Profile: {
      screen: ProfileScreen
    },
    Setting: {
      screen: SettingScreen
    },
    MyQRCode: {
      screen: MyQRCodeScreen
    },
    History: {
      screen: HistoryScreen
    },
    MesInfos: {
      screen: MesInfosScreen
    },
    ChangePincode: {
      screen: ChangePincodeScreen,
      navigationOptions: {
        gesturesEnabled: false
      }
    },
    ContactUs: {
      screen: ContactUsScreen
    },
    Help: {
      screen: HelpScreen
    },
    AboutUs: {
      screen: AboutUsScreen
    }
  },
  {
    initialRouteName: "Profile",
    defaultNavigationOptions: () => {
      return {
        headerBackTitle: null,
        headerStyle: {
          backgroundColor: dataStore.currentTheme,
          shadowOffset: { width: 0, height: 0 },
          elevation: 0
        },
        headerTintColor: colors.text.white,
        headerTitleStyle: {
          fontFamily: fonts.primary.semibold,
          fontWeight: "200",
          fontSize: fontSizes.header,
          color: colors.text.white
        }
      };
    },
    headerLayoutPreset: "center"
  }
);

ProfileStack.navigationOptions = ({ navigation }) => {
  const { routeName } = navigation.state.routes[navigation.state.index];
  const tabBarVisible = routeName !== "ChangePincode";
  const gesturesEnabled = routeName !== "ChangePincode";

  return {
    tabBarVisible,
    gesturesEnabled
  };
};

const APEMainStack = createBottomTabNavigator(
  {
    HomeTab: {
      screen: HomeStack
    },
    QRScanTab: {
      screen: QRStack
    },
    ProfileTab: {
      screen: ProfileStack
    }
  },
  {
    initialRouteName: "HomeTab",
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: dataStore.currentTheme,
        shadowOffset: { width: 0, height: 0 },
        elevation: 0
      },
      headerTintColor: colors.text.white,
      headerTitleStyle: {
        fontFamily: fonts.primary.semibold,
        fontSize: fontSizes.header,
        color: colors.text.white
      }
    },
    headerMode: "screen",
    headerLayoutPreset: "center",
    tabBarComponent: Navbar
  }
);
export default APEMainStack;
