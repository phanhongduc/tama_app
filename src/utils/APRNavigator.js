import {
  createStackNavigator,
  createBottomTabNavigator,
  NavigationActions,
  StackActions
} from "react-navigation";
// import screens
import QRScanScreen from "screens/Main/QRScanTab/QRScan";
import PaymentAmountScreen from "screens/Main/QRScanTab/PaymentAmount";
import PaymentPincodeScreen from "screens/Main/QRScanTab/PaymentPincode";
import ProgressScreen from "screens/Main/QRScanTab/Progress";
import PaymentSuccessScreen from "screens/Main/QRScanTab/PaymentSuccess";
import PaymentFailureScreen from "screens/Main/QRScanTab/PaymentFailure";
import VerifyPincode from "screens/Main/ProfileTab/VerifyPincode";
import SettingScreen from "screens/Main/ProfileTab/Setting";
import ChangePincodeScreen from "screens/Main/ProfileTab/ChangePincode";
import MyQRCodeScreen from "screens/Main/ProfileTab/MyQRCode";
import HistoryScreen from "screens/Main/ProfileTab/History";
import MesInfosScreen from "screens/Main/ProfileTab/MesInfos";
import ContactUsScreen from "screens/Main/ProfileTab/ContactUs";
import HelpScreen from "screens/Main/ProfileTab/Help";
import AboutUsScreen from "screens/Main/ProfileTab/AboutUs";
import RestaurantHomepage from "screens/Main/HomeTab/RestaurantHomepage";
// import components
import Navbar from "components/Navbar";
// import constants
import { fonts, colors, fontSizes } from "utils/Constants";
// import stores
import paymentPincodeStore from "stores/PaymentPincodeStore";
import dataStore from "stores/DataStore";
import navbarStore from "stores/NavbarStore";
import qrScanStore from "stores/QRScanStore";

const MainRestaurantStack = createStackNavigator(
  {
    RestaurantHomePage: {
      screen: RestaurantHomepage
    }
  },
  {
    initialRouteName: "RestaurantHomePage",
    defaultNavigationOptions: ({ screenProps }) => {
      const themeApp = screenProps.theme;
      return {
        headerBackTitle: null,
        headerStyle: {
          backgroundColor: themeApp.currentTheme,
          shadowOffset: { width: 0, height: 0 },
          elevation: 0,
          borderBottomWidth: 0
        },
        headerTintColor: colors.text.white,
        headerTitleStyle: {
          fontFamily: fonts.primary.semibold,
          fontWeight: "200",
          fontSize: fontSizes.header,
          color: colors.text.white
        }
      };
    },
    headerLayoutPreset: "center"
  }
);

const QRStack = createStackNavigator(
  {
    QRScan: {
      screen: QRScanScreen
    },
    PaymentAmount: {
      screen: PaymentAmountScreen
    },
    PaymentPincode: {
      screen: PaymentPincodeScreen,
      navigationOptions: {
        gesturesEnabled: false
      }
    },
    Progress: {
      screen: ProgressScreen,
      navigationOptions: {
        gesturesEnabled: false
      }
    },
    PaymentSuccess: {
      screen: PaymentSuccessScreen,
      navigationOptions: {
        gesturesEnabled: false
      }
    },
    PaymentFailure: {
      screen: PaymentFailureScreen,
      navigationOptions: {
        gesturesEnabled: false
      }
    }
  },
  {
    initialRouteName: "QRScan",
    defaultNavigationOptions: {
      headerBackTitle: null,
      headerStyle: {
        backgroundColor: dataStore.currentTheme,
        shadowOffset: { width: 0, height: 0 },
        elevation: 0
      },
      headerTintColor: colors.text.white,
      headerTitleStyle: {
        fontFamily: fonts.primary.semibold,
        fontSize: fontSizes.header,
        color: colors.text.white
      },
      tabBarVisible: false
    },
    headerLayoutPreset: "center"
  }
);

QRStack.navigationOptions = ({ navigation }) => {
  const tabBarVisible =
    navigation.state.routes[navigation.state.index].routeName === "QRScan";

  return {
    tabBarVisible
  };
};

const defaultGetStateForActionQRStack = QRStack.router.getStateForAction;
QRStack.router.getStateForAction = (action, state) => {
  if (!state || action.type !== NavigationActions.BACK) {
    return defaultGetStateForActionQRStack(action, state);
  }

  const { routeName } = state.routes[state.index];
  switch (routeName) {
    case "QRScan":
      navbarStore.setSelectedTab("APRHomeTab");
      return defaultGetStateForActionQRStack(action, state);
    case "PaymentAmount":
      qrScanStore.setIsProcessing(false);
      return defaultGetStateForActionQRStack(action, state);
    case "PaymentPincode":
      paymentPincodeStore.toggleCancelModal();
      return null;
    case "Progress":
      return null;
    case "PaymentSuccess":
    case "PaymentFailure": {
      const { navigation } = state.routes[state.index].params;
      navigation.popToTop();
      navbarStore.setSelectedTab("APRHomeTab");
      qrScanStore.setIsProcessing(false);
      const resetAction = StackActions.reset({
        key: "APRHomeTab",
        index: 0,
        actions: [
          NavigationActions.navigate({ routeName: "RestaurantHomePage" })
        ]
      });
      navigation.dispatch(resetAction);
      return null;
    }
    default:
      return defaultGetStateForActionQRStack(action, state);
  }
};

const ProfileStack = createStackNavigator(
  {
    Verify: {
      screen: VerifyPincode
    },
    Setting: {
      screen: SettingScreen
    },
    MyQRCode: {
      screen: MyQRCodeScreen
    },
    History: {
      screen: HistoryScreen
    },
    MesInfos: {
      screen: MesInfosScreen
    },
    ChangePincode: {
      screen: ChangePincodeScreen,
      navigationOptions: {
        gesturesEnabled: false
      }
    },
    ContactUs: {
      screen: ContactUsScreen
    },
    Help: {
      screen: HelpScreen
    },
    AboutUs: {
      screen: AboutUsScreen
    }
  },
  {
    initialRouteName: "Verify",
    defaultNavigationOptions: () => {
      return {
        headerBackTitle: null,
        headerStyle: {
          backgroundColor: dataStore.currentTheme,
          shadowOffset: { width: 0, height: 0 },
          elevation: 0
        },
        headerTintColor: colors.text.white,
        headerTitleStyle: {
          fontFamily: fonts.primary.semibold,
          fontWeight: "200",
          fontSize: fontSizes.header,
          color: colors.text.white
        }
      };
    },
    headerLayoutPreset: "center"
  }
);

ProfileStack.navigationOptions = ({ navigation }) => {
  const { routeName } = navigation.state.routes[navigation.state.index];
  const tabBarVisible = routeName !== "ChangePincode";
  const gesturesEnabled = routeName !== "ChangePincode";

  return {
    tabBarVisible,
    gesturesEnabled
  };
};

const APRMainStack = createBottomTabNavigator(
  {
    APRHomeTab: {
      screen: MainRestaurantStack
    },
    APRQRScanTab: {
      screen: QRStack
    },
    APRProfileTab: {
      screen: ProfileStack
    }
  },
  {
    initialRouteName: "APRHomeTab",
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: dataStore.currentTheme,
        shadowOffset: { width: 0, height: 0 },
        elevation: 0
      },
      headerTintColor: colors.text.white,
      headerTitleStyle: {
        fontFamily: fonts.primary.semibold,
        fontSize: fontSizes.header,
        color: colors.text.white
      }
    },
    headerMode: "screen",
    headerLayoutPreset: "center",
    tabBarComponent: Navbar
  }
);

export default APRMainStack;
