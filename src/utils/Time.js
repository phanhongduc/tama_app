export const daysOfWeek = [
  "sunday",
  "monday",
  "tuesday",
  "wednesday",
  "thursday",
  "friday",
  "saturday"
];

const now = new Date();

const isBefore = (a = 0, b = 0) => {
  const a1 = new Date(a);
  const b1 = new Date(b);

  return b1.getTime() - a1.getTime() > 0;
};

const normalizeHour = (hourInString = "") => {
  if (!hourInString) return null;
  const fromTo = hourInString.split("-");
  if (fromTo.length < 2) return null;
  const from = fromTo[0].split(":");
  if (from.length < 2) return null;
  const to = fromTo[1].split(":");
  if (to.length < 2) return null;
  return {
    from: parseInt(from[0], 10) * 60 + parseInt(from[1], 10),
    to: parseInt(to[0], 10) * 60 + parseInt(to[1], 10)
  };
};

const normalizeException = (exception = {}) => {
  const keys = Object.keys(exception);
  const result = {};
  keys.forEach(key => {
    if (!!exception[key].length && typeof exception[key] === "object") {
      result[key] = [...[], ...exception[key].map(h => normalizeHour(h))];
    }
  });
  return result;
};
export const toTime = (scheduler = {}) => {
  const days = Object.keys(scheduler);
  const result = {};
  days.forEach(val => {
    if (!!scheduler[val].length && typeof scheduler[val] === "object") {
      result[val] = [...[], ...scheduler[val].map(h => normalizeHour(h))];
    }
  });
  return result;
};

export const toString = () => {
  const date = now.getDate();
  const year = now.getFullYear();
  const month = now.getMonth();
  return `${year}-${month + 1}-${date}`;
};

const getDeltaDate = (d = daysOfWeek[0], week = 0) => {
  const index = daysOfWeek.indexOf(d);
  const day = now.getDay();

  let delta = index - day;
  if (delta >= 0) {
    delta += week * 7;
  } else {
    delta = delta + 7 + week * 7;
  }

  const result = new Date(now.getTime());
  result.setDate(now.getDate() + delta + week * 7);
  return result;
};

class OpeningHour {
  constructor(scheduler = {}) {
    this.exceptions = { ...scheduler.exceptions };
    this.scheduler = { ...scheduler };
    delete this.scheduler.exceptions;
  }

  get timeTable() {
    return toTime(this.scheduler);
  }

  isOpening(moment = new Date()) {
    const name = toString(moment);
    let result = false;
    if (!this.exceptions[name]) {
      const day = daysOfWeek[moment.getDate()];
      if (this.timeTable[day]) {
        if (this.timeTable[day].length !== 0) {
          const minutes = moment.getHours() * 60 + moment.getMinutes();
          this.timeTable[day].forEach(val => {
            if (val.from <= minutes && val.to >= minutes) {
              result = true;
            }
          });
        }
      }
      return result;
    }
    // if not day-off
    if (this.exceptions[toString].length !== 0) {
      const minutes = moment.getHours() * 60 + moment.getMinutes();
      this.exceptions[toString].forEach(val => {
        if (val.from <= minutes && val.to >= minutes) {
          result = true;
        }
      });
    }
    return result;
  }

  // eslint-disable-next-line class-methods-use-this
  sort(normalize = {}) {
    const dates = Object.keys(normalize);
    const sortedDates = dates.sort(
      (a, b) => new Date(a).getTime() - new Date(b).getTime()
    );
    const result = {};
    sortedDates.forEach(d => {
      result[d] = [...normalize[d], ...[]];
    });

    return result;
  }

  // let delta = daysOfWeek.indexOf(d) - moment.getDay();
  // let from = this.timeTable[d].from;
  // let to = this.timeTable[d].to;
  // if (delta > 0) {
  //   from = from + delta * 24 * 60;
  //   to = to + delta * 24 * 60;
  // } else {
  //   from = from + (delta + 7) * 24 * 60;
  //   to = to + (delta + 7) * 24 * 60;
  // }
  // open.push(from);
  // close.push(to);

  normalizeAWeek(week = 0) {
    try {
      let days = Object.keys(this.scheduler);
      const result = {};
      days.forEach(d => {
        const target = getDeltaDate(d, week);

        // turn into { '2019-7-18': [], '2019-7-19': [] }
        const string = `${target.getFullYear()}-${target.getMonth() +
          1}-${target.getDate()}`;
        result[string] = [...this.scheduler[d], ...[]];
      });

      days = Object.keys(result);

      days.forEach(d => {
        if (this.exceptions[d]) {
          result[d] = [...this.exceptions[d], ...[]];
        }
      });

      return result;
    } catch (e) {
      console.log("error", JSON.stringify(e));
      return false;
    }
  }
}

export const getNextOpening = (scheduler = {}) => {
  const openHour = new OpeningHour(scheduler);
  let result = null;
  let i = 0;

  // break the loop when scheduler is empty
  const normalScheduler = Object.keys(openHour.scheduler);
  const exceptions = Object.keys(openHour.scheduler);
  if (!normalScheduler.length && !exceptions.length) return {};

  while (!result) {
    const thisWeek = openHour.normalizeAWeek(i);

    const sort = openHour.sort(toTime(thisWeek));

    const keys = Object.keys(sort);

    // eslint-disable-next-line no-loop-func
    keys.forEach(key => {
      sort[key].forEach(e => {
        const temp = new Date(key);
        const tempHours = Math.floor(e.from / 60);
        const tempMinutes = e.from % 60;
        temp.setHours(tempHours);
        temp.setMinutes(tempMinutes);
        if (result) return;
        if (isBefore(now, temp.getTime())) {
          result = {
            [key]: { from: e.from, to: e.to }
          };
        }
      });
    });

    i += 1;
  }

  return result;
};

function translateDayLanguage(data) {
  // Change name days of week to France
  return {
    Lundi: data.monday,
    Mardi: data.tuesday,
    Mercredi: data.wednesday,
    Jeudi: data.thursday,
    Vendredi: data.friday,
    Samedi: data.saturday,
    Dimanche: data.sunday
  };
}

// eslint-disable-next-line func-names
String.prototype.replaceAll = function(search, replacement) {
  const target = this;
  return target.replace(new RegExp(search, "g"), replacement);
};

function convertDateTimeToString(dateTimeArray) {
  try {
    return dateTimeArray
      .map(time => time.replaceAll(":", "h").replace("-", " - "))
      .join(", ");
  } catch (exception) {
    console.info(
      "Error when convert DateTime to string ",
      JSON.stringify(exception)
    );
    return "";
  }
}

export const convertTimeObjectToStringArray = data => {
  try {
    const tempData = translateDayLanguage(data);
    const temp = {};
    const str = [];

    Object.keys(tempData).forEach(key => {
      if (tempData[key]) {
        tempData[key] = convertDateTimeToString(tempData[key]);
      }
    });
    Object.keys(tempData).forEach(key => {
      if (tempData[key]) {
        temp[tempData[key]] = [];
      }
    });
    Object.keys(tempData).forEach(key => {
      if (tempData[key]) {
        temp[tempData[key]].push(key);
      }
    });
    Object.keys(temp).forEach(key => {
      if (temp[key].length > 2) {
        temp[key] = [
          ...temp[key].slice(0, 1),
          ...temp[key].slice(temp[key].length - 1)
        ];
      }
    });
    Object.keys(temp).forEach(key => {
      if (key.length !== 0) {
        const item = `${temp[key].join(" à ")}: ${key}`;
        str.push(item.charAt(0).toUpperCase() + item.slice(1));
      }
    });

    return str;
  } catch (error) {
    console.log(error);
    return false;
  }
};

export const convertExceptionTimeToStringArray = data => {
  try {
    const tempData = {};

    Object.keys(data).forEach(key => {
      if (data[key] && data[key].length) {
        tempData[key] = convertDateTimeToString(data[key]);
      }
    });

    return Object.keys(tempData).map(key => `${key}: ${tempData[key]}`);
  } catch (error) {
    console.log(error);
    return false;
  }
};

// eslint-disable-next-line consistent-return
export const validateDaysAllowed = (conditions = {}) => {
  try {
    const normalDays = Object.keys(conditions).filter(o => o !== "exceptions");
    const exceptions = Object.keys(conditions.exceptions);
    if (conditions.length === 0) {
      return false;
    }
    // if today is not in conditions => false
    // check today in exception => false
    if (!normalDays.includes(daysOfWeek[now.getDay()])) {
      return false;
    }
    if (
      exceptions.filter(o => {
        const date = new Date(o);
        return (
          now.getDate() === date.getDate() &&
          now.getMonth() === date.getMonth() &&
          now.getFullYear() === date.getFullYear()
        );
      }).length === 0
    ) {
      return false;
    }
  } catch (e) {
    return false;
  }
};

export const getStatus = (scheduler = {}) => {
  try {
    let keys = Object.keys(scheduler);
    if (keys.length === 0) {
      return {
        isOpening: false,
        time: {}
      };
    }
    const standard = daysOfWeek.concat("exceptions");

    const normalizeScheduler = {};
    standard.forEach(day => {
      if (typeof scheduler[day] === "object") {
        normalizeScheduler[day] = scheduler[day];
      }
    });

    keys = Object.keys(normalizeScheduler);
    if (keys.length === 0) {
      return {
        isOpening: false,
        time: {}
      };
    }

    const openingHour = new OpeningHour(normalizeScheduler);
    const string = toString();
    let isOpening = false;
    let time = null;

    if (!openingHour.exceptions[string]) {
      const day = daysOfWeek[now.getDay()];

      if (openingHour.timeTable[day]) {
        if (openingHour.timeTable[day].length !== 0) {
          const minutes = now.getHours() * 60 + now.getMinutes();
          openingHour.timeTable[day].forEach(val => {
            if (time) return;
            if (val.from <= minutes && val.to >= minutes) {
              isOpening = true;
              time = { [toString()]: { ...val } };
            }
          });
        }
      }
      if (!time) {
        time = getNextOpening(normalizeScheduler);
      }
      return {
        isOpening,
        time
      };
    }
    // if not day-off
    const exceptions = normalizeException(openingHour.exceptions);
    if (exceptions[string].length !== 0) {
      const minutes = now.getHours() * 60 + now.getMinutes();
      exceptions[string].forEach(val => {
        if (time) return;
        if (val.from <= minutes && val.to >= minutes) {
          isOpening = true;
          time = { [toString()]: { ...val } };
        }
      });
    }
    if (!time) {
      time = getNextOpening(normalizeScheduler);
    }
    return {
      isOpening,
      time
    };
  } catch (e) {
    return {
      isOpening: false,
      time: {}
    };
  }
};

export const toMiliseconds = (time = {}, isOpening = false) => {
  // console.log("time", JSON.stringify(time));
  const keys = Object.keys(time)[0];
  // console.log("keys", JSON.stringify(keys));
  if (!keys) return null;
  const result = new Date(keys);
  // console.log("result", JSON.stringify(result.toDateString()));
  const target = isOpening ? time[keys].to : time[keys].from;
  const hour = Math.floor(target / 60);
  const minute = target % 60;
  result.setHours(hour);
  result.setMinutes(minute);
  const miliseconds = result.getTime();
  return miliseconds;
};

// const fakeData = {
//   // monday: ["1:00-19:00", "13:00-18:00"],
//   // // tuesday: ["1:00-19:00"],
//   // // wednesday: ["1:00-19:00"],
//   // // friday: ["1:00-19:00"],
//   // tuesday: [],
//   // wednesday: [],
//   // thursday: ["string"],
//   // // thursday: [],
//   // friday: ["11:00-14:00"],
//   // saturday: ["1:00-14:00"],
//   // sunday: ["1:00-19:00"],
//   // exceptions: {
//   //   // "2019-7-19": []
//   //   "2019-7-19": ["09:00-24:00"]
//   //   // "2019-7-20": ["09:00-12:00", "13:00-15:00"]
//   // }
//   exceptions: []
// };

// console.log("status of fakedata", getStatus(fakeData));

export const convertDateFormat = timestamp => {
  if (!timestamp) return false;

  const time = new Date(timestamp);
  let dd = time.getDate();
  let mm = time.getMonth() + 1; // January is 0!
  const yyyy = time.getFullYear().toString();

  if (dd < 10) {
    dd = `0${dd}`;
  }
  if (mm < 10) {
    mm = `0${mm}`;
  }
  return `${dd}/${mm}/${yyyy}`;
};

export const convertTimeFormat = timestamp => {
  if (!timestamp) return false;

  const time = new Date(timestamp);

  return `${time.getHours()}:${time.getMinutes()}`;
};
