import { Dimensions, PixelRatio } from "react-native";

export const screenWidth = Dimensions.get("window").width;
export const screenHeight = Dimensions.get("window").height;

export const widthPercentageToDP = widthPercent => {
  return typeof widthPercent === "string"
    ? PixelRatio.roundToNearestPixel(
        (screenWidth * parseFloat(widthPercent)) / 100
      )
    : PixelRatio.roundToNearestPixel(
        (screenWidth *
          parseFloat(
            `${(((widthPercent * 1.0) / screenWidth) * 100).toString()}%`
          )) /
          100
      );
};

export const heightPercentageToDP = heightPercent => {
  return typeof heightPercent === "string"
    ? PixelRatio.roundToNearestPixel(
        (screenHeight * parseFloat(heightPercent)) / 100
      )
    : PixelRatio.roundToNearestPixel(
        (screenHeight *
          parseFloat(
            `${(((heightPercent * 1.0) / screenHeight) * 100).toString()}%`
          )) /
          100
      );
};
