import { heightPercentageToDP as hp } from "utils/Styling";
import { Platform } from "react-native";

export const fonts = {
  primary: {
    regular: "Montserrat-Regular",
    bold: "Montserrat-Bold",
    semibold: "Montserrat-SemiBold",
    medium: "Montserrat-Medium",
    light: "Montserrat-Light",
    boldItalic: "Montserrat-BoldItalic",
    italic: "Montserrat-Italic"
  },
  secondary: {
    medium: "SFProDisplay-Medium",
    regular: "SFProDisplay-Regular"
  }
};

export const colors = {
  primary: {
    green: "#00C68E",
    orange: "#FFB75A"
  },
  secondary: "#FFF",
  background: {
    gray: {
      dot: "#D8D8D8",
      item: "#EEEFEE",
      bar: "#F6F6F6"
    },
    white: "#FFF",
    green: "#00C68E",
    overlay: "rgba(45, 45, 45, 0.5)",
    nearwhite: "#FBFCFC",
    overlayDark: "rgba(0,0,0,0.95)",
    overlayWhite: "rgba(255,255,255,.8)",
    orange: "#FFB57A"
  },
  text: {
    black: "#000",
    white: "#FFF",
    grayWhite: "#E3E3E3",
    green: "#00C68E",
    darkGreen: "#187B5E",
    blackGreen: "#04241A",
    blue: "#007AFF",
    gray: {
      tab: "#747373",
      transaction: "#929191",
      instruction: "#322F2F",
      title: "#393939",
      detail: "#686868",
      border: "#4D4D4D",
      notice: "#00C68E",
      line: "#D8D8D8",
      buttonRefulser: "rgba(183,181,181,100)",
      icon: "#979797",
      chevronRight: "#A4A7A6",
      menuIcon: "#B9B9B9"
    },
    red: "#F00"
  },
  border: "#169580",
  disabled: "#B7B5B5",
  termDisabled: "#D8D8D8"
};

export const backgroundColors = {
  balance: "#EEEFEE"
};

export const fontSizes = {
  header: hp(16),
  button: {
    huge: hp(18),
    big: hp(17)
  },
  title: hp(18),
  bigtitle: hp(28),
  tab: hp(14),
  balance: hp(40),
  detail: {
    amount: hp(25),
    extremeLarge: hp(34),
    largerTitle: hp(22),
    largeTitle: hp(20),
    huge: hp(17),
    bigger: hp(17),
    big2: hp(16),
    big: hp(14),
    normal: hp(15),
    lightBig: hp(13),
    small: hp(12),
    tiny: hp(10)
  }
};

export const isIOS = Platform.OS === "ios";
export const isAndroid = Platform.OS === "android";
export const mapToken =
  "pk.eyJ1IjoibWluaG5ndXllbjEyIiwiYSI6ImNqeXRtemo5NDA1dnczbm8xOTJuYWlseTgifQ.hCzpIsuRgB_AZi6ybQmOIg";
export const mailUrl =
  "https://accounts.google.com/ServiceLogin?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1";
export const userRole = {
  EMPLOYEE: "ROLE_EMPLOYEE",
  RESTAURANT: "ROLE_RESTAURANT"
};

export const errorMessage = {
  SOMETHING_WENT_WRONG:
    "Désolé, une erreur s'est produite. Veuillez réessayer.",
  USERNAME_PASSWORD_NOT_FILL_OUT: `L'identifiant ou le mot de passe n'a pas été renseigné`,
  USERNAME_PASSWORD_INCORRECT: `L'identifiant ou le mot passe est incorrect`,
  FILL_OUT_EMAIL: `Veuillez saisir votre adresse email`,
  EMAIL_INVALID: `Your email address is invalid`,
  PINCODE_CANNOT_SAME_THE_OLD_ONE: `Votre ancien et nouveau code Tama'a ne peuvent être identitiques`,
  YOU_HAVE_BEEN_BLOCKED_TRY_AGAIN_15SECOND: `Votre compte est bloqué.\nVeuillez réessayer dans 15 secondes.`,
  YOU_HAVE_BEEN_BLOCKED_TRY_AGAIN_30SECOND: `Votre compte est bloqué.\nVeuillez réessayer dans 30 secondes.`,
  NO_INTERNET_CONNECTION: `Aucune connexion. Veuillez réessayer.`,
  YOUR_BALANCE_EXPIRED: `Votre crédit a expiré. `,
  YOUR_BALANCE_NOT_ENOUGH: `Désolé, votre crédit est insuffisant`,
  YOU_REACH_MAXIMUM_DAYLY_AMOUNT: `Vous avez atteint le maximum du solde autorisé aujourd'hui`,
  SORRY_TRY_AGAIN: `Désolé, veuillez réessayer.`,
  PINCODE_NOT_MATCH: `Le code Tama'a saisit ne correspond pas. Veuillez resaisir votre code Tama'a`,
  VALIDATION_FAIL: `L'authentification a écouchée`,
  CANNOT_FETCH_LOCATION: "Géolocalisation indisponible",
  CURRENT_LOCATION: "Impossible de connaitre votre position actuelle",
  ERROR: "Erreur",
  ACCESS_DENIED: `L'accès a été refusé`,
  EMAIL_NOT_FOUND: `Aucun résultat de recherche. Veuillez saisir une autre adresse`,
  OVER_TIME_SLOT: `Vous avez dépassé le créneau horaire autorisé`,
  OVER_DAY_SLOT: `Vous ne pouvez effectuer de transaction aujourd'hui. Veuillez réessayer utlérieurement`,
  PHONE_NOT_AVAILABLE: `Le numéro de téléphone n'est pas disponible`,
  ACCOUNT_DEACTIVATE: `Votre compte a été désactivé`,
  EMAIL_NOT_EXIST: `Cette adresse email n'existe pas`,
  NO_PHOTO: `Aucune photo disponible`,
  EXCEED_10_PHOTOS: `Your number of images exceeds 10. Try again.`,
  SOME_FAILED_PHOTOS: `The maximum photo size is 5MB. Veuillez réessayer.`,
  UPLOAD_FAILED: `Uploading failed. Veuillez réessayer.`,
  PLEASE_TRY_AGAIN: `Veuillez réessayer.`,
  NO_CONTENT: `Aucun contenu disponible`
};

export const infoMessage = {
  CHOOSE_PHOTO_FROM_LIBRARY: `Choisir dans la bibliothèque d'images`,
  ARE_YOU_WANT_TO_LOGOUT: `Êtes-vous sûr de vouloir vous déconnecter ?`,
  TAKE_A_PHTO: `Prendre une photo`,
  CALL_US: `Veuillez nous contacter`,
  EXCEPTION: `Exception`,
  CHANGE_PINCODE_SUCCESS: `Votre nouveau code Tama'a a bien été pris en compte`,
  PULL_TO_REFRESH: `Glisser vers le bas pour rafraîchir`,
  RELEASE_TO_REFRESH: `Lacher pour rafraîchir`,
  REFRESHING: `Rechargement en cours`,
  GIVE_UP_REFRESHING: `Abandonner le rechargement`,
  REFRESH_COMPLETED: `La page a été rechargée`,
  SEARCH_PLACEHOLDER: `Trouvez un endroit où manger`
};

export const android = {
  notification: {
    channelId: "tamaa_channel_notify",
    channelName: "Tamaa Channel Notify"
  }
};

export const Images = {
  ARROW_REFRESH: require("assets/images/ic_arrow_refresh.png"),
  TAB_BACKGROUND: require("assets/images/it_tab_background.png"),
  RESTAURANT_ACTIVE: require("assets/images/ic_tab_restaurant_active.png"),
  RESTAURANT_DEACTIVE: require("assets/images/ic_tab_restaurant.png"),
  TAB_QRCODE: require("assets/images/ic_tab_qrcode.png"),
  TAB_PROFILE_DEACTIVE: require("assets/images/ic_tab_profile_active.png"),
  TAB_PROFILE_ACTIVE: require("assets/images/ic_tab_profile.png"),
  RESTAURANT_APR_ACTIVE: require("assets/images/ic_tab_restaurant_active_apr.png"),
  TAB_PROFILE_APR_ACTIVE: require("assets/images/ic_tab_profile_active_apr.png"),
  IC_UPLOAD: require("assets/images/ic_upload.png"),
  IC_GALLERY: require("assets/images/ic_gallery.png"),
  IC_CAMERA: require("assets/images/ic_camera.png"),
  IC_INPUT_CLOSE: require("assets/images/ic_input_close.png"),
  IC_MARKER_OPEN: require("assets/images/ic_marker_open.png"),
  IC_PROFILE_LOCATION: require("assets/images/ic_profile_location.png"),
  IC_PROFILE_MAIL: require("assets/images/ic_profile_mail.png"),
  IC_PROFILE_FACEBOOK: require("assets/images/ic_profile_facebook.png"),
  IC_PROFILE_INSTAGRAM: require("assets/images/ic_profile_instagram.png"),
  IC_CHEVRON_RIGHT: require("assets/images/ic_chevron_right.png"),
  IC_LOGO: require("assets/images/ic_logo.png"),
  IC_EMAIL: require("assets/images/ic_email.png"),
  IC_USERNAME: require("assets/images/ic_username.png"),
  IC_PASS: require("assets/images/ic_pass.png"),
  IC_EYE: require("assets/images/ic_eye.png"),
  IC_EYE_OFF: require("assets/images/ic_eye_off.png"),
  IC_CROSSHAIR: require("assets/images/ic_crosshair.png"),
  IC_LOGO_VERTICAL_APE: require("assets/images/ic_logo_vertical_ape.png"),
  IC_LOGO_VERTICAL_APR: require("assets/images/ic_logo_vertical_apr.png"),
  IC_SETTING_HISTORY: require("assets/images/ic_setting_history.png"),
  IC_SETTING_INFO: require("assets/images/ic_setting_info.png"),
  IC_SETTING_CHANGEPIN: require("assets/images/ic_setting_changepin.png"),
  IC_SETTING_CONTACT: require("assets/images/ic_setting_contact.png"),
  IC_SETTING_HELP: require("assets/images/ic_setting_help.png"),
  IC_SETTING_ABOUT: require("assets/images/ic_setting_about.png"),
  IC_SETTING_LOGOUT: require("assets/images/ic_setting_logout.png"),
  IC_QR_CORNER: require("assets/images/ic_qr_corner.png"),
  IC_CLOSE: require("assets/images/ic_close.png"),
  IG_ABOUTAPP_1: require("assets/images/ig_aboutapp_1.png"),
  IG_ABOUTAPP_2: require("assets/images/ig_aboutapp_2.png"),
  IG_ABOUTAPP_3: require("assets/images/ig_aboutapp_3.png"),
  IG_ABOUTAPP_4: require("assets/images/ig_aboutapp_4.png"),
  IG_APR_ABOUTAPP_1: require("assets/images/ig_apr_aboutapp_1.png"),
  IG_APR_ABOUTAPP_2: require("assets/images/ig_apr_aboutapp_2.png"),
  IG_APR_ABOUTAPP_3: require("assets/images/ig_apr_aboutapp_3.png"),
  IG_APR_ABOUTAPP_4: require("assets/images/ig_apr_aboutapp_4.png"),
  IG_INSTRUCTION_1: require("assets/images/ig_instruction_1.png"),
  IG_INSTRUCTION_2: require("assets/images/ig_instruction_2.png"),
  IG_INSTRUCTION_3: require("assets/images/ig_instruction_3.png"),
  IG_INSTRUCTION_4: require("assets/images/ig_instruction_4.png"),
  IG_INSTRUCTION_1_APR: require("assets/images/ig_instruction_1_apr.png"),
  IG_INSTRUCTION_2_APR: require("assets/images/ig_instruction_2_apr.png"),
  IG_INSTRUCTION_3_APR: require("assets/images/ig_instruction_3_apr.png"),
  IG_INSTRUCTION_4_APR: require("assets/images/ig_instruction_4_apr.png")
};

export const Gifs = {
  TRANSACTION_LOADING: require("assets/images/ic_transaction_loading.gif"),
  TRANSACTION_LOADING_APR: require("assets/images/ic_transaction_loading_apr.gif"),
  IG_LOADER: require("assets/images/ig_loader.gif")
};
