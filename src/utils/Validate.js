/* eslint-disable no-else-return */
import lang from "lodash/lang";

const emailRegex = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
// eslint-disable-next-line no-useless-escape
const phoneRegex = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;

class Validate {
  constructor() {
    this.emailRegex = emailRegex;
    this.phoneRegex = phoneRegex;
  }

  // eslint-disable-next-line class-methods-use-this
  isEmpty(data) {
    return lang.isEmpty(data);
  }

  isEmail(email) {
    return this.emailRegex.test(email);
  }

  // eslint-disable-next-line class-methods-use-this
  isValidCoordinates(latitude, longitude) {
    if (typeof latitude !== "number" || typeof longitude !== "number")
      return false;

    return (
      latitude <= 90 && latitude >= -90 && longitude <= 180 && longitude >= -180
    );
  }

  isValidPhone(phone) {
    if (phone) {
      return this.phoneRegex.test(phone.replace(/\s/g, ""));
    }
    return "";
  }

  // eslint-disable-next-line class-methods-use-this
  isValidField(field) {
    if (!field) return false;
    if (
      Object.prototype.toString.call(field) === "[object Array]" &&
      field.length === 0
    ) {
      return false;
    }
    return true;
  }
}

export default new Validate();
