import React, { Component } from "react";
import { View, StyleSheet, ImageBackground } from "react-native";
import { observer, inject } from "mobx-react";
import { colors } from "utils/Constants";
import notificationService from "services/NotificationService";

@inject(
  "userStore",
  "networkStore",
  "headerTitleStore",
  "dataStore",
  "splashStore"
)
@observer
class Splash extends Component {
  async componentDidMount() {
    const { navigation, splashStore } = this.props;
    notificationService.setNavigation(navigation);

    setTimeout(() => {
      splashStore.init(navigation);
      // this.handleValidateAccount();
      splashStore.handleValidateAccount();
    }, 2000);
    // await AsyncStorage.clear();
  }

  render() {
    return (
      <View style={styles.container}>
        <ImageBackground
          source={require("assets/images/ig_loader.gif")}
          style={{ width: "100%", height: "100%" }}
          resizeMode="contain"
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: colors.background.green
  }
});

export default Splash;
