import React, { Component } from "react";
import { inject, observer } from "mobx-react";
import RestaurantDetail from "components/RestaurantDetail";
import HeaderTitle from "components/HeaderTitle";
import { userRole } from "utils/Constants";

@inject("headerTitleStore", "restaurantDetailStore", "userStore", "dataStore")
@observer
export default class RestaurantHomepage extends Component {
  static navigationOptions = ({ screenProps }) => {
    const themeApp = screenProps.theme;
    return {
      headerTitle: <HeaderTitle />,
      headerStyle: {
        backgroundColor: themeApp.currentTheme
      }
    };
  };

  componentDidMount() {
    const { headerTitleStore, userStore } = this.props;
    headerTitleStore.setTitle(userStore.user.restaurantName);
  }

  render() {
    const {
      userStore,
      screenProps: { theme }
    } = this.props;
    const { user } = userStore;
    return (
      <RestaurantDetail
        restaurant={user}
        isRestaurant={user.role === userRole.RESTAURANT}
        theme={theme}
        mode="detail"
      />
    );
  }
}
