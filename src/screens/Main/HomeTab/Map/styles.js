import { StyleSheet, Dimensions, Platform } from "react-native";
import { fonts, fontSizes, colors } from "utils/Constants";

const { width, height } = Dimensions.get("window");
const ANNOTATION_SIZE = 45;

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  searchBarContainer: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    backgroundColor: colors.background.white,
    height: 50,
    position: "absolute",
    left: 0,
    right: 0,
    top: 0
  },
  matchParent: {
    flex: 1
  },
  modalBoxWrap: {
    position: "absolute",
    borderRadius: 6,
    top: (height * 50) / 100,
    width,
    height: (height * 50) / 100,
    flex: 1,
    backgroundColor: "transparent",
    zIndex: 10
  },
  wrap: {
    flex: 1,
    zIndex: 9999,
    position: "relative",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: colors.background.white,
    borderRadius: 6,
    paddingTop: 0,
    paddingRight: 0,
    paddingBottom: 0,
    paddingLeft: 0
  },
  layoutBox: {
    flex: 1,
    flexDirection: "row",
    flexWrap: "wrap",
    justifyContent: "center",
    alignItems: "center",
    paddingTop: 20,
    paddingRight: 20,
    paddingBottom: 20,
    paddingLeft: 20,
    marginTop: 10
  },
  hiddenModal: {
    height: 50
  },
  RestaurantCardItemModal: {
    height: "70%"
  },
  map: {
    ...StyleSheet.absoluteFillObject,
    width: "100%",
    height: "100%"
    // top: 40
  },
  annotationContainer: {
    width: ANNOTATION_SIZE,
    height: ANNOTATION_SIZE,
    alignItems: "center",
    justifyContent: "center",
    opacity: 1
  },
  annotationFill: {
    width: ANNOTATION_SIZE - 3,
    height: ANNOTATION_SIZE - 3,
    borderRadius: (ANNOTATION_SIZE - 3) / 2,
    backgroundColor: "orange",
    transform: [{ scale: 0.6 }]
  },
  imageMarker: {
    flex: 1,
    resizeMode: "contain",
    width: 40,
    height: 40,
    transform: [{ scale: 0.8 }],
    opacity: 1
  },
  containerCallout: {
    alignItems: "center",
    justifyContent: "flex-start",
    width: 120,
    zIndex: 9999999,
    marginVertical: 5,
    ...Platform.select({
      ios: {
        position: "absolute",
        top: 42,
        left: -60,
        right: 0
      }
    })
  },
  titleCallout: {
    fontSize: fontSizes.detail.big,
    fontWeight: "bold",
    fontFamily: fonts.primary.bold,
    textAlign: "center",
    color: colors.text.gray.title
  },
  calloutContentStyle: { backgroundColor: "transparent", borderWidth: 0 },
  calloutTextStyle: {
    fontSize: fontSizes.detail.big2,
    fontWeight: "bold",
    fontFamily: fonts.primary.semibold
  },
  calloutTipStyle: { borderTopColor: "transparent" },
  previewContainerIOS: {
    zIndex: 2
  },
  mapContainer: {
    flex: 1,
    width: "100%",
    zIndex: -1
  },
  crosshairWrapper: {
    width: 40,
    height: 40,
    backgroundColor: colors.background.overlayWhite,
    position: "absolute",
    bottom: 90,
    right: 10,
    borderRadius: 5,
    elevation: 2,
    justifyContent: "center",
    alignItems: "center"
  },
  searchBarWrapper: {
    width: "100%",
    position: "absolute",
    top: 0,
    left: 0,
    zIndex: 0
  }
});

export default styles;
