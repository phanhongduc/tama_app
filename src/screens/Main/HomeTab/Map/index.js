/* eslint-disable react/no-unused-state */
import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  Platform,
  PermissionsAndroid,
  Keyboard,
  Alert,
  Animated,
  Image
} from "react-native";
import geoViewport from "@mapbox/geo-viewport";
import MapboxGL from "@mapbox/react-native-mapbox-gl";
import { observer, inject } from "mobx-react";
import PreviewDetailRestaurant from "components/PreviewDetail";
import SearchBar from "components/SearchBar";
import ToastMessage from "components/ToastMessage";
import { isIOS, errorMessage, mapToken, Images } from "utils/Constants";
import styles from "./styles";

const screen = Dimensions.get("window");
const CENTER_COORD = [-149.4499982, -17.6333308];
const MAPBOX_VECTOR_TILE_SIZE = 512;
const iconMarkerOpen = require("assets/images/ic_marker_open.png");
const iconMarkerClose = require("assets/images/ic_marker_close.png");

@inject("restaurantStore", "homepageStore", "searchbarStore", "dataStore")
@observer
export default class Map extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "offlinePack",
      offlineRegion: null,
      offlineRegionStatus: null
    };
    MapboxGL.setAccessToken(mapToken);
    this.onDownloadProgress = this.onDownloadProgress.bind(this);
    this.onDidFinishLoadingStyle = this.onDidFinishLoadingStyle.bind(this);
    this.onStatusRequest = this.onStatusRequest.bind(this);
  }

  componentDidMount() {
    const { restaurantStore, homepageStore } = this.props;

    restaurantStore.fetchListRestaurants(() => {
      homepageStore.initListAnnotations();
    });
    restaurantStore.setReloadInterval();
    homepageStore.setMapRef(this.map);
    this.getLocationHandler();
  }

  componentWillUnmount() {
    const { restaurantStore } = this.props;

    Keyboard.dismiss();
    restaurantStore.removeReloadInterval();
  }

  async onDidFinishLoadingStyle() {
    const { name, offlineRegion } = this.state;
    const bounds = geoViewport.bounds(
      CENTER_COORD,
      12,
      [screen.width, screen.height],
      MAPBOX_VECTOR_TILE_SIZE
    );
    const options = {
      name,
      styleURL: MapboxGL.StyleURL.Street,
      bounds: [[bounds[0], bounds[1]], [bounds[2], bounds[3]]],
      minZoom: 10,
      maxZoom: 20
    };
    MapboxGL.offlineManager.setTileCountLimit(1000);
    const offlinePack = await MapboxGL.offlineManager.getPack("offlinePack");
    if (!offlinePack) {
      // start download
      MapboxGL.offlineManager.createPack(options, this.onDownloadProgress);
    } else {
      if (offlineRegion) {
        offlineRegion.resume();
      }
      await offlinePack.resume();
    }
  }

  onDownloadProgress(offlineRegion, offlineRegionStatus) {
    this.setState({
      name: offlineRegion.name,
      offlineRegion,
      offlineRegionStatus
    });
  }

  async onStatusRequest() {
    const { offlineRegion } = this.state;
    if (offlineRegion) {
      const offlineRegionStatus = await offlineRegion.status();
      Alert.alert("Get Status: ", JSON.stringify(offlineRegionStatus, null, 2));
    }
  }

  getCurrentLocation = () => {
    const { homepageStore } = this.props;
    navigator.geolocation.getCurrentPosition(
      pos => {
        homepageStore.moveToMarker(pos.coords.longitude, pos.coords.latitude);
      },
      () => {
        Alert.alert(errorMessage.ERROR, errorMessage.CURRENT_LOCATION);
      }
    );
  };

  getLocationHandler = async () => {
    const { restaurantStore, homepageStore } = this.props;

    if (Platform.OS === "android") {
      const permission = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
      );

      if (permission === PermissionsAndroid.RESULTS.GRANTED) {
        if (restaurantStore.listRestaurants.length !== 0) {
          homepageStore.moveToMarker(
            restaurantStore.listRestaurants[0].longitude,
            restaurantStore.listRestaurants[0].latitude
          );
        }
      } else {
        Alert.alert(errorMessage.ERROR, errorMessage.CANNOT_FETCH_LOCATION);
      }
    } else if (restaurantStore.listRestaurants.length !== 0) {
      homepageStore.moveToMarker(
        restaurantStore.listRestaurants[0].longitude,
        restaurantStore.listRestaurants[0].latitude
      );
    }
  };

  renderAnnotations = () => {
    const { homepageStore } = this.props;
    let annotationsJSX = null;
    if (!homepageStore.isInitializing) {
      annotationsJSX = homepageStore.listAnnotations.map((element, index) => {
        const opacityStyle = {};
        const animationStyle = {};
        if (index === homepageStore.activeAnnotationIndex) {
          const scaleIn = new Animated.Value(0.8);
          Animated.timing(scaleIn, { toValue: 1.0, duration: 200 }).start();
          animationStyle.transform = [{ scale: scaleIn }];
          opacityStyle.opacity = new Animated.Value(1);
        } else if (index === homepageStore.previousActiveAnnotationIndex) {
          const scaleOut = new Animated.Value(1);
          Animated.timing(scaleOut, { toValue: 0.8, duration: 200 }).start();
          animationStyle.transform = [{ scale: scaleOut }];
        }
        return (
          <MapboxGL.PointAnnotation
            key={element.id}
            id={element.id.toString()}
            title={element.title}
            coordinate={element.coordinate}
          >
            <Animated.View>
              <TouchableOpacity
                style={styles.annotationContainer}
                onPress={() => homepageStore.onAnnotationSelected(index)}
              >
                <Animated.Image
                  source={element.isOpening ? iconMarkerOpen : iconMarkerClose}
                  style={[
                    styles.imageMarker,
                    { transform: [{ scale: homepageStore.animationValue }] },
                    { opacity: homepageStore.opacityValue },
                    opacityStyle,
                    animationStyle
                  ]}
                />
              </TouchableOpacity>
            </Animated.View>
            <MapboxGL.Callout
              tipStyle={{ borderTopColor: "transparent" }}
              contentStyle={styles.calloutContentStyle}
              textStyle={styles.calloutTextStyle}
            >
              <View style={styles.containerCallout}>
                <Text style={styles.titleCallout}>{element.title}</Text>
              </View>
            </MapboxGL.Callout>
          </MapboxGL.PointAnnotation>
        );
      });
    }
    return annotationsJSX;
  };

  renderCurrentLocation = () => {
    return (
      <TouchableOpacity
        style={styles.crosshairWrapper}
        onPress={this.getCurrentLocation}
      >
        <Image
          source={Images.IC_CROSSHAIR}
          style={{ width: 25, height: 25 }}
          resizeMode="contain"
        />
      </TouchableOpacity>
    );
  };

  hideKeyboard = () => {
    Keyboard.dismiss();
  };

  onPressMap = async () => {
    const { searchbarStore, homepageStore } = this.props;
    this.hideKeyboard();
    searchbarStore.hideSearchResult();
    await homepageStore.onAnnotationDeselected(
      homepageStore.activeAnnotationIndex
    );
  };

  render() {
    const { homepageStore, dataStore } = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.searchBarWrapper}>
          <SearchBar moveToMarker={homepageStore.moveToMarkerProps} />
        </View>
        <View style={styles.mapContainer}>
          <MapboxGL.MapView
            zoomLevel={10}
            ref={c => {
              this.map = c;
            }}
            onDidFinishLoadingMap={this.onDidFinishLoadingStyle}
            centerCoordinate={CENTER_COORD}
            attributionEnabled={false}
            logoEnabled={false}
            rotateEnabled={false}
            animated
            showUserLocation
            style={styles.map}
            onPress={this.onPressMap}
          >
            {this.renderAnnotations()}
          </MapboxGL.MapView>
          {this.renderCurrentLocation()}
        </View>
        <ToastMessage />
        {homepageStore.restaurant &&
          homepageStore.showPreview &&
          (isIOS ? (
            <View style={styles.previewContainerIOS}>
              <PreviewDetailRestaurant
                restaurant={homepageStore.restaurant}
                theme={dataStore.currentTheme}
              />
            </View>
          ) : (
            <PreviewDetailRestaurant
              restaurant={homepageStore.restaurant}
              theme={dataStore.currentTheme}
            />
          ))}
      </View>
    );
  }
}
