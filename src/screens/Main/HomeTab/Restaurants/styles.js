import { StyleSheet } from "react-native";
import { colors, fontSizes, fonts } from "utils/Constants";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    backgroundColor: colors.background.gray.bar,
    paddingBottom: 100
  },
  cardWrapper: {
    alignItems: "center"
  },
  listContainer: {
    marginTop: 10
  },
  sectionContainer: {
    width: "100%",
    minHeight: 8,
    paddingHorizontal: "9%",
    paddingTop: 10,
    backgroundColor: colors.background.gray.bar
  },
  sectionText: {
    marginBottom: 10,
    fontFamily: fonts.primary.medium,
    fontSize: fontSizes.header,
    color: colors.text.gray.title
  },
  searchContainer: {
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    backgroundColor: colors.background.white
  },
  inputStyle: {
    fontFamily: fonts.primary.regular,
    fontSize: fontSizes.detail.big2,
    width: "80%"
  },
  searchIcon: {
    paddingLeft: 20,
    paddingRight: 20
  },
  suggestItem: {
    height: 45,
    borderTopWidth: 1,
    borderTopColor: colors.text.grayWhite,
    justifyContent: "center"
  },
  suggestItemWrapper: {
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 10,
    marginLeft: 15
  },
  suggestItemIcon: {
    color: colors.text.gray.icon,
    marginRight: 10
  },
  searchBarWrapper: {
    flexDirection: "column",
    width: "100%",
    backgroundColor: colors.background.white,
    position: "absolute",
    top: 0,
    left: 0,
    zIndex: 1
  },
  searchBarViewStyle: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    height: 50,
    width: "100%"
  },
  backIcon: {
    width: 20,
    height: 20,
    marginLeft: 10
  }
});

export default styles;
