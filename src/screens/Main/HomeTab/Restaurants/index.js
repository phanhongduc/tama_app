import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  Platform,
  FlatList,
  Keyboard,
  Image
} from "react-native";
import { LargeList } from "react-native-largelist-v3";
import IconIonic from "react-native-vector-icons/Ionicons";
import { inject, observer } from "mobx-react";
import RestaurantCardItem from "components/RestaurantCardItem";
import CustomHeader from "components/CustomHeader";
import {
  fonts,
  fontSizes,
  colors,
  infoMessage,
  isAndroid,
  Images
} from "utils/Constants";
import styles from "./styles";

@inject("restaurantStore", "searchbarStore")
@observer
export default class Restaurants extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showSuggest: false
    };
  }

  componentDidMount() {
    // this.props.restaurantStore.setReloadInterval();
    // this.props.restaurantStore.fetchListRestaurants();
  }

  componentWillUnmount() {
    const { restaurantStore } = this.props;

    restaurantStore.removeReloadInterval();
  }

  hideKeyboard = () => {
    Keyboard.dismiss();
  };

  navigateToDetail = restaurant => {
    const { navigation, searchbarStore } = this.props;
    navigation.navigate("RestaurantDetail", { restaurant });
    searchbarStore.saveHistoryList(restaurant);
  };

  onRefresh = () => {
    const { restaurantStore } = this.props;
    restaurantStore.fetchListRestaurants(() => {
      this.listRef.endRefresh();
    });
  };

  onFocus = () => {
    const { searchbarStore } = this.props;
    if (!searchbarStore.searchTextList) {
      this.setState({ showSuggest: true });
    } else {
      this.setState({ showSuggest: false });
    }
  };

  onSubmit = () => {
    this.hideKeyboard();
    this.setState({ showSuggest: false });
  };

  onChangeText = text => {
    if (!text) {
      this.setState({ showSuggest: true });
    } else {
      this.setState({ showSuggest: false });
    }
  };

  renderItem = ({ section, row }) => {
    const { restaurantStore, searchbarStore } = this.props;

    let item = null;
    if (searchbarStore.searchResultList.length) {
      item = searchbarStore.searchResultList[section].items[row];
    } else {
      item = restaurantStore.data[section].items[row];
    }

    const restaurant = {
      id: item.id,
      avatar: item.avatar,
      name: item.restaurantName,
      images: item.photo,
      facebook: item.facebook,
      instagram: item.instagram,
      website: item.website,
      email: item.email,
      phoneNumber: [item.phoneNumber1, item.phoneNumber2],
      openingHours: item.openingHours,
      description: item.description,
      isOpening: item.isOpening,
      time: item.time,
      cardAccepted: item.cardAccepted,
      longtitude: item.longitude,
      latitude: item.latitude
    };

    return (
      <TouchableOpacity
        style={styles.cardWrapper}
        onPress={() => this.navigateToDetail(restaurant)}
      >
        <RestaurantCardItem
          restaurant={restaurant}
          containerStyle={{
            borderBottomWidth: 1,
            borderBottomColor: "#E8E8E8"
          }}
        />
      </TouchableOpacity>
    );
  };

  renderListRestaurants = () => {
    const { restaurantStore, searchbarStore } = this.props;
    const result = !restaurantStore.isLoading ? (
      <LargeList
        ref={ref => {
          this.listRef = ref;
        }}
        data={
          searchbarStore.searchResultList.length
            ? searchbarStore.searchResultList
            : restaurantStore.data
        }
        heightForSection={() => 50}
        renderSection={this.renderSection}
        heightForIndexPath={() => 76}
        renderIndexPath={this.renderItem}
        refreshHeader={CustomHeader}
        onRefresh={this.onRefresh}
      />
    ) : null;
    return result;
  };

  renderSection = section => {
    const { restaurantStore, searchbarStore } = this.props;
    return (
      <View style={styles.sectionContainer}>
        <Text style={styles.sectionText} numberOfLines={1} ellipsizeMode="tail">
          {searchbarStore.searchResultList.length
            ? searchbarStore.searchResultList[0].section
            : restaurantStore.data[section].section}
        </Text>
      </View>
    );
  };

  renderSearchBar = () => {
    const { searchbarStore } = this.props;

    return (
      <View style={styles.searchBarWrapper}>
        <View style={styles.searchBarViewStyle}>
          <IconIonic
            name="ios-search"
            color={colors.text.gray.icon}
            size={25}
            style={styles.searchIcon}
          />
          <TextInput
            value={searchbarStore.searchTextList}
            autoCorrect={false}
            autoCapitalize="none"
            keyboardType={
              Platform.OS === "android" ? "visible-password" : "default"
            }
            placeholder={infoMessage.SEARCH_PLACEHOLDER}
            placeholderTextColor={colors.text.gray.icon}
            style={styles.inputStyle}
            onChange={({ nativeEvent }) => {
              searchbarStore.searchList(nativeEvent.text);
            }}
            onChangeText={text => this.onChangeText(text)}
            onBlur={() => this.setState({ showSuggest: false })}
            onFocus={this.onFocus}
            onSubmitEditing={this.onSubmit}
            clearButtonMode="always"
          />
          {isAndroid && searchbarStore.searchTextList !== "" && (
            <TouchableOpacity onPress={() => searchbarStore.clearInputSearch()}>
              <Image
                source={Images.IC_INPUT_CLOSE}
                resizeMode="contain"
                style={styles.backIcon}
              />
            </TouchableOpacity>
          )}
        </View>
        {this.renderListSuggest()}
      </View>
    );
  };

  renderListSuggest = () => {
    const { searchbarStore } = this.props;
    const { showSuggest } = this.state;
    let result = null;
    if (showSuggest) {
      result = (
        <FlatList
          renderItem={this.renderSuggestItem}
          keyExtractor={item => item.id.toString()}
          data={searchbarStore.searchHistoryList}
          removeClippedSubviews
          keyboardShouldPersistTaps="handled"
        />
      );
    }

    return result;
  };

  renderSuggestItem = ({ item }) => {
    return (
      <TouchableOpacity
        style={styles.suggestItem}
        onPress={() => this.navigateToDetail(item)}
      >
        <View style={styles.suggestItemWrapper}>
          <IconIonic name="ios-pin" size={22} style={styles.suggestItemIcon} />
          <Text
            style={{
              fontFamily: fonts.primary.regular,
              fontSize: fontSizes.detail.big,
              color: colors.text.gray.icon
            }}
          >
            {item.name}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        {this.renderSearchBar()}
        <View style={{ height: 50 }} />
        {this.renderListRestaurants()}
      </View>
    );
  }
}
