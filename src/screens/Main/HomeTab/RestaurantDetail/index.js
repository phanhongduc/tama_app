import React, { Component } from "react";
import { observer, inject } from "mobx-react";
import RestaurantDetail from "components/RestaurantDetail";
import HeaderLeft from "components/HeaderLeft";

@inject("headerTitleStore", "dataStore")
@observer
export default class RestaurantDetailScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerLeft: (
        <HeaderLeft
          goBack={() => {
            headerTitleStore.setTitle(null);
            navigation.goBack(null);
          }}
        />
      )
    };
  };

  componentDidMount() {
    const { headerTitleStore } = this.props;
    headerTitleStore.setTitle("Restaurant");
  }

  render() {
    const { navigation, dataStore } = this.props;
    const restaurant = navigation.getParam("restaurant");
    return (
      <RestaurantDetail
        restaurant={restaurant}
        theme={dataStore.currentTheme}
        mode="detail"
      />
    );
  }
}
