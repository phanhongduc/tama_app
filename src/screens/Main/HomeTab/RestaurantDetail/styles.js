import { StyleSheet, Dimensions } from "react-native";
import { Header } from "react-navigation";
import { fontSizes, fonts, colors } from "utils/Constants";

const { height, width } = Dimensions.get("window");
const mainScreenHeight = height - Header.HEIGHT;
const headerHeight = mainScreenHeight * 0.1986;

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    backgroundColor: colors.background.white
  },
  headerWrapper: {
    height: headerHeight,
    width: "100%",
    alignItems: "center"
  },
  cardContainer: {
    height: headerHeight * 0.5,
    marginTop: headerHeight * 0.28
  },
  cardInfoContainer: {
    height: headerHeight * 0.5 * 0.8
  },
  nameStyle: {
    fontSize: fontSizes.header,
    fontFamily: fonts.primary.bold
  },
  socialAction: {
    flexDirection: "row",
    justifyContent: "space-evenly",
    alignItems: "center",
    backgroundColor: colors.background.gray.bar,
    height: mainScreenHeight * 0.125
  },
  iconSocialWrapper: {
    height: "50%",
    aspectRatio: 1 / 1,
    justifyContent: "center",
    alignItems: "center"
  },
  infoDetailContainer: {
    height: mainScreenHeight * 0.26,
    justifyContent: "center"
  },
  infoDetailWrapper: {
    justifyContent: "space-evenly",
    alignItems: "center"
  },
  infoDetailDescription: {
    fontFamily: fonts.primary.regular,
    fontSize: fontSizes.detail.small,
    color: colors.text.gray.title,
    textAlign: "center",
    width: width * 0.8
  },
  infoDetailOpenTime: {
    fontFamily: fonts.primary.semibold,
    fontSize: fontSizes.detail.small,
    color: colors.text.gray.title,
    textAlign: "center"
  },
  infoDetailPhoneNumberWrapper: {
    flexDirection: "row"
  },
  infoDetailPhoneNumber: {
    fontFamily: fonts.primary.regular,
    fontSize: fontSizes.detail.small,
    // color: colors.text.gray.title
    color: colors.text.green
  },
  infoDetailAddress: {
    fontFamily: fonts.primary.regular,
    fontSize: fontSizes.detail.small,
    color: colors.text.green,
    textAlign: "center"
  },
  columnStyle: {
    justifyContent: "space-between"
  },
  separator: {
    height: width * 0.0155
  },
  imageStyle: {
    width: width * 0.323,
    aspectRatio: 121 / 128
  },
  blankArea: {
    height: 100
  },
  modal: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: colors.background.overlayDark
  },
  modalGalleryContainer: {
    justifyContent: "center",
    alignItems: "center",
    height: height * 0.9,
    width
  },
  modalWrapper: {
    marginTop: height * 0.1,
    height: height * 0.7
  },
  modalSlider: {
    overflow: "visible",
    marginTop: 20
  },
  modalImage: {
    height: width
  },
  modalLeftCloseButton: {
    position: "absolute",
    left: 20,
    top: 0,
    height: 40
  },
  modalRightCloseButton: {
    position: "absolute",
    right: 20,
    top: 0,
    height: 40
  }
});

export default styles;
