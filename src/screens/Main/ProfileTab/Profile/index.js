/* eslint-disable react/no-unescaped-entities */
import React, { Component } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import Icon from "react-native-vector-icons/AntDesign";
import { observer, inject } from "mobx-react";
import Button from "components/Button";
import { colors } from "utils/Constants";
import { widthPercentageToDP as wp } from "utils/Styling";
import styles from "./styles";

@inject("userStore", "profileStore", "dataStore")
@observer
export default class ProfileScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerTitle: "Mon profil",
    headerRight: (
      <TouchableOpacity
        style={{ padding: wp(10), marginRight: 10 }}
        onPress={() => navigation.navigate("Setting")}
      >
        <Icon name="setting" size={25} color={colors.text.darkGreen} />
      </TouchableOpacity>
    )
  });

  // sendNoti = async () => {
  //   const { userStore } = this.props;
  //   const { user } = userStore;
  //   const { deviceToken } = user;
  //   console.log(JSON.stringify(deviceToken));
  //   const result = await fetch(
  //     "https://us-central1-test2-cca37.cloudfunctions.net/notify",
  //     {
  //       method: "POST",
  //       headers: {
  //         Accept: "application/json",
  //         "Content-Type": "application/json"
  //       },
  //       body: JSON.stringify({
  //         tokens: deviceToken,
  //         data: {
  //           title: "hello",
  //           message: "hello"
  //         },
  //         notification: {
  //           title: "hello",
  //           message: "hello"
  //         }
  //       })
  //     }
  //   );
  //   console.log(JSON.stringify(result));
  // };

  render() {
    const { profileStore, dataStore, navigation } = this.props;
    return (
      <View style={styles.scrollViewContainer}>
        <View style={styles.detailView}>
          <Text style={styles.titleText}>Solde global disponible :</Text>
          {/* <TouchableOpacity
            style={{
              textAlign: "center",
              color: "#333333",
              marginBottom: 5
            }}
            onPress={async () => await this.sendNoti()}
          >
            <Text style={styles.instructions}>Click here to receive noti</Text>
          </TouchableOpacity> */}
          {profileStore.employeeBalance ? (
            <Text style={styles.price}>
              {`${profileStore.employeeBalance} XPF`}
            </Text>
          ) : (
            <Text style={styles.balanceText}>Currently unavailable.</Text>
          )}
          {profileStore.availableBalance && (
            <View style={[styles.triangle, styles.arrowUp]} />
          )}
          {profileStore.availableBalance && (
            <View style={styles.balance}>
              <Text style={styles.balanceText}>Dépensable aujourd'hui</Text>
              <Text style={styles.balancePrice}>
                {`${profileStore.availableBalance} XPF`}
              </Text>
            </View>
          )}
          <View style={styles.information}>
            <Text style={styles.informationTitle}>
              Conditions d’utilisation
            </Text>
          </View>
          <View style={styles.information}>
            {profileStore.availableTimes ? (
              <Text style={styles.informationItem}>
                {profileStore.availableTimes}
              </Text>
            ) : (
              <Text
                style={[
                  styles.informationItem,
                  { marginTop: 10, marginBottom: 10 }
                ]}
              >
                Pas de réstriction
              </Text>
            )}
            <Text style={styles.informationItem}>
              {profileStore.dailyAmount}
            </Text>
            {profileStore.expirationDate ? (
              <Text style={styles.informationItem}>
                {profileStore.expirationDate}
              </Text>
            ) : (
              <View />
            )}
          </View>
        </View>
        <View style={styles.buttonContainer}>
          <Button
            labelStyle={styles.btnText}
            style={{
              alignItems: "center",
              backgroundColor: dataStore.themes.employee
            }}
            onPress={() => navigation.navigate("MyQRCode")}
            title="Mon QR Code"
          />
        </View>
      </View>
    );
  }
}
