import { StyleSheet } from "react-native";
import { fonts, colors, fontSizes, backgroundColors } from "utils/Constants";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  screenWidth,
  screenHeight
} from "utils/Styling";

const styles = StyleSheet.create({
  scrollViewContainer: {
    flex: 1,
    alignItems: "center",
    padding: wp(10)
  },
  detailView: {
    flex: 3,
    justifyContent: "center",
    alignItems: "center",
    paddingBottom: hp(30),
    paddingTop: hp(20)
  },
  buttonContainer: {
    flex: 2,
    width: "100%",
    justifyContent: screenHeight < 600 ? "flex-start" : "center",
    alignItems: "center",
    borderTopWidth: 1,
    borderTopColor: colors.text.grayWhite,
    paddingTop: screenHeight < 600 ? hp(30) : 0,
    paddingBottom: screenHeight < 600 ? 0 : hp(20)
  },
  titleText: {
    color: colors.primary.green,
    fontFamily: fonts.primary.bold,
    fontSize: fontSizes.detail.largerTitle,
    paddingBottom: hp(20)
  },
  triangle: {
    width: 0,
    height: 0,
    backgroundColor: "transparent",
    borderStyle: "solid"
  },
  arrowUp: {
    borderTopWidth: 0,
    borderRightWidth: 12,
    borderBottomWidth: 12,
    borderLeftWidth: 12,
    borderTopColor: "transparent",
    borderRightColor: "transparent",
    borderBottomColor: backgroundColors.balance,
    borderLeftColor: "transparent"
  },
  price: {
    paddingBottom: hp(10),
    fontSize: fontSizes.balance,
    color: colors.text.blackGreen,
    fontFamily: fonts.primary.light,
    textAlign: "center"
  },
  balance: {
    backgroundColor: backgroundColors.balance,
    width: wp(219),
    height: hp(82),
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 10
  },
  balanceText: {
    fontFamily: fonts.primary.medium,
    color: colors.text.gray.instruction,
    fontSize: fontSizes.detail.small,
    paddingBottom: hp(5)
  },
  balancePrice: {
    fontSize: fontSizes.detail.bigger,
    fontFamily: fonts.primary.semibold,
    color: colors.text.blackGreen,
    textAlign: "center"
  },
  information: {
    alignItems: "center",
    justifyContent: "center",
    paddingTop: hp(20)
  },
  informationTitle: {
    fontFamily: fonts.primary.bold,
    fontSize: fontSizes.detail.small,
    color: colors.text.gray.instruction
  },
  informationItem: {
    fontFamily: fonts.primary.regular,
    fontSize: fontSizes.detail.small,
    color: colors.text.gray.instruction,
    textAlign: "center",
    width: wp(screenWidth * 0.7)
  },
  btnText: {
    fontSize: fontSizes.detail.huge
  }
});

export default styles;
