import { StyleSheet, Dimensions } from "react-native";
import { fonts, colors, fontSizes } from "utils/Constants";
import { Header } from "react-navigation";

const { height } = Dimensions.get("window");
const mainScreen = height - Header.HEIGHT - 100;

const styles = StyleSheet.create({
  container: {
    height: mainScreen,
    alignItems: "center",
    justifyContent: "center"
  },
  imageStyle: {
    width: 159,
    height: 159
  },
  contentContainer: {
    alignItems: "center",
    marginTop: "5%",
    justifyContent: "center"
  },
  instructionText: {
    textAlign: "center",
    color: colors.text.black,
    fontSize: fontSizes.detail.small,
    fontFamily: fonts.primary.regular
  },
  wraperTitle: {
    alignItems: "center",
    justifyContent: "center",
    paddingTop: 24,
    paddingBottom: 12
  },
  fullName: {
    color: colors.text.black,
    fontSize: fontSizes.detail.largerTitle,
    fontFamily: fonts.primary.bold,
    textAlign: "center"
  },
  type: {
    color: colors.text.black,
    fontSize: fontSizes.detail.big,
    fontFamily: fonts.primary.regular,
    textAlign: "center"
  },
  contactWrapper: {
    alignItems: "center",
    justifyContent: "center",
    paddingTop: 12,
    paddingBottom: 24
  },
  contact: {
    color: colors.text.black,
    fontSize: fontSizes.detail.big2,
    fontFamily: fonts.primary.semibold,
    textAlign: "center"
  },
  imageContainer: {
    alignItems: "center"
  }
});

export default styles;
