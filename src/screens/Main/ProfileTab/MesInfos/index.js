import React, { Component } from "react";
import { View, Text, Image, TouchableOpacity, Linking } from "react-native";
import { observer, inject } from "mobx-react";
import HeaderLeft from "components/HeaderLeft";
import { colors, userRole } from "utils/Constants";
import { SERVER_URL, BACKOFFICE_URL } from "utils/API";
import styles from "./styles";

const imageProfileEmployee = require("assets/images/ig_user_profile.png");
const imagePlaceholder = require("assets/images/img_placeholder.png");

@inject("userStore", "dataStore")
@observer
export default class MesInfosScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: "Mes infos",
      headerLeft: <HeaderLeft goBack={() => navigation.goBack()} />
    };
  };

  handleOnOpenWebsite = website => {
    if (!website) return;
    Linking.canOpenURL(website)
      .then(supported => {
        if (supported) {
          Linking.openURL(website);
        }
      })
      .catch(error => console.log(website, error));
  };

  render() {
    const { userStore } = this.props;
    const {
      firstName = "",
      surName = "",
      restaurantName = "",
      companyName = "",
      email = "",
      phoneNumber = "",
      phoneNumber1 = "",
      phoneNumber2 = "",
      role,
      avatar
    } = userStore.user;

    let imageSource = null;
    if (role === userRole.EMPLOYEE) {
      imageSource = imageProfileEmployee;
    } else if (avatar) {
      // role restaurant
      imageSource = { uri: SERVER_URL + avatar.path };
    } else {
      imageSource = imagePlaceholder;
    }

    return (
      <View style={styles.container}>
        <View style={styles.imageContainer}>
          <Image
            source={imageSource}
            style={styles.imageStyle}
            resizeMode="contain"
          />
          <View style={styles.wraperTitle}>
            {role === userRole.EMPLOYEE ? (
              <Text style={styles.fullName}>{`${firstName} ${surName}`}</Text>
            ) : (
              <Text style={styles.fullName}>{restaurantName}</Text>
            )}
            <Text style={styles.type}>{companyName}</Text>
          </View>
        </View>
        <View style={styles.contactWrapper}>
          <Text style={styles.contact}>{email}</Text>
          {role === userRole.EMPLOYEE ? (
            <Text style={styles.contact}>{phoneNumber}</Text>
          ) : (
            <Text style={styles.contact}>
              {`${phoneNumber1}\n${phoneNumber2}`}
            </Text>
          )}
        </View>
        <View style={styles.contentContainer}>
          <Text style={styles.instructionText}>
            Pour modifier vos informations,
          </Text>
          <Text style={styles.instructionText}>
            {role === userRole.EMPLOYEE
              ? "contactez votre employeur"
              : "connectez-vous à la plateforme de gestion :"}
          </Text>
          {role === userRole.RESTAURANT && (
            <TouchableOpacity
              onPress={() => this.handleOnOpenWebsite(BACKOFFICE_URL)}
            >
              <Text
                style={[styles.instructionText, { color: colors.text.green }]}
              >
                {`[Tama'a Management Platform]`}
              </Text>
            </TouchableOpacity>
          )}
        </View>
      </View>
    );
  }
}
