import React, { Component } from "react";
import {
  StyleSheet,
  Modal,
  View,
  Text,
  TouchableOpacity,
  BackHandler
} from "react-native";
import { observer, inject } from "mobx-react";
import { colors, fonts, fontSizes } from "utils/Constants";
import { screenHeight } from "utils/Styling";
import Icon from "react-native-vector-icons/Feather";
import Keyboard from "components/Keyboard";
import ToastMessage from "components/ToastMessage";

@inject("keyboardStore", "dataStore", "changePincodeStore", "navbarStore")
@observer
export default class VerifyPincode extends Component {
  static navigationOptions = {
    header: null
  };

  componentDidMount() {
    const { navigation, changePincodeStore } = this.props;
    changePincodeStore.initScreen(navigation);

    this.backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      this.handleHardwareBack
    );
  }

  handleHardwareBack = () => {
    const { navigation, navbarStore } = this.props;
    // navigation.pop();
    console.log(navigation);
    if (navigation.state.routeName === "Verify") {
      navigation.replace("APRHomeTab");
      navbarStore.setSelectedTab("APRHomeTab");
    }
  };

  handleOnDelete = () => {
    const { keyboardStore, changePincodeStore } = this.props;
    changePincodeStore.removePincode();
    keyboardStore.onDelete();
  };

  render() {
    const { keyboardStore, changePincodeStore, dataStore } = this.props;
    return (
      <Modal
        visible={changePincodeStore.modal}
        hardwareAccelerated
        animationType="slide"
        onRequestClose={() => {}}
      >
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <View style={{ alignItems: "center" }}>
            <Text
              style={{
                color: colors.text.black,
                fontSize: fontSizes.title,
                fontFamily: fonts.primary.semibold,
                textAlign: "center"
              }}
            >
              {`ENTREZ VOTRE CODE TAMA'A`}
            </Text>
          </View>
          <View style={styles.input}>
            {keyboardStore.length ? (
              <View style={styles.pincodeTextContainer}>
                <View style={styles.pincodeTextWrapper}>
                  <Text style={styles.pincodeText}>
                    {changePincodeStore.pincodeViewStr}
                  </Text>
                </View>
                <TouchableOpacity
                  style={styles.deleteIconWrapper}
                  onPress={this.handleOnDelete}
                >
                  <Icon name="delete" size={25} style={styles.deleteIcon} />
                </TouchableOpacity>
              </View>
            ) : (
              <View style={styles.underline} />
            )}
          </View>
          <TouchableOpacity
            style={{
              alignItems: "center",
              marginBottom: screenHeight > 600 ? 100 : 50,
              marginTop: screenHeight > 600 ? 30 : 10
            }}
            onPress={changePincodeStore.forgotPincode}
          >
            <Text style={styles.forgotPincodeStyle}>Forgot pincode?</Text>
          </TouchableOpacity>
          <Keyboard
            onPress={changePincodeStore.addPincodeForProfile}
            color={{ color: dataStore.currentTheme }}
          />
          <ToastMessage />
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  input: {
    height: "8%",
    width: "50%",
    maxHeight: 60,
    alignItems: "center",
    marginTop: 50,
    marginBottom: 20
  },
  pincodeTextContainer: {
    flexDirection: "row",
    alignItems: "center"
  },
  pincodeTextWrapper: {
    alignItems: "center",
    width: "80%",
    height: "100%"
  },
  pincodeText: {
    fontSize: 40,
    color: colors.text.black,
    fontFamily: fonts.primary.medium
  },
  underline: {
    height: 1,
    backgroundColor: colors.text.black,
    width: "100%",
    position: "absolute",
    bottom: 0,
    left: 0
  },
  deleteIconWrapper: {
    height: "100%",
    paddingTop: 5,
    paddingLeft: 10
  },
  deleteIcon: {
    color: colors.text.gray.icon
  },
  disabledButton: {
    backgroundColor: colors.disabled,
    marginTop: screenHeight > 600 ? 20 : 0
  },
  forgotPincodeStyle: {
    fontFamily: fonts.primary.regular,
    color: colors.text.gray.transaction,
    fontSize: fontSizes.detail.big2,
    textDecorationLine: "underline"
  }
});
