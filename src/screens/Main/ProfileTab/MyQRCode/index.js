import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import { observer, inject } from "mobx-react";
import HeaderLeft from "components/HeaderLeft";
import { userRole } from "utils/Constants";
import { SERVER_URL } from "utils/API";
import styles from "./styles";

@inject("userStore", "dataStore")
@observer
export default class MyQRCodeScreen extends Component {
  static navigationOptions = ({ navigation, screenProps }) => {
    return {
      headerTitle:
        screenProps.theme.role === "employee"
          ? "Mon QR code personnel"
          : "QR code",
      headerLeft: <HeaderLeft goBack={() => navigation.goBack()} />
    };
  };

  render() {
    const { userStore } = this.props;
    const { user } = userStore;
    const { customId } = user;
    if (!customId)
      return (
        <View style={styles.container}>
          <Text>No Data</Text>
        </View>
      );

    const qrcode = {
      uri: `${SERVER_URL}/images/qr/${customId}.png`
    };

    return (
      <View style={styles.container}>
        <View
          style={{
            width: 221,
            height: 221
          }}
        >
          <Image source={qrcode} style={{ flex: 1 }} resizeMode="stretch" />
        </View>
        <View style={styles.contentContainer}>
          <Text style={styles.instructionText}>
            {user.role === userRole.EMPLOYEE
              ? "À présenter dans les"
              : "A présenter aux utilisateurs"}
          </Text>
          <Text style={styles.instructionText}>
            {user.role === userRole.EMPLOYEE
              ? "établissements disposant de"
              : "disposant de l’application"}
          </Text>
          <Text style={styles.instructionText}>
            {user.role === userRole.EMPLOYEE
              ? "l’application pour payer avec vos"
              : "pour payer avec leurs"}
          </Text>
          <Text style={styles.instructionText}>PASS TAMA’A</Text>
        </View>
      </View>
    );
  }
}
