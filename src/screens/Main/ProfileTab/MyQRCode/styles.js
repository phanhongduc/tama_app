import { StyleSheet, Dimensions } from "react-native";
import { Header } from "react-navigation";
import { fonts, colors, fontSizes } from "utils/Constants";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "utils/Styling";

const { height, width } = Dimensions.get("window");
const mainScreenHeight = height - Header.HEIGHT;

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    justifyContent: "center",
    height: mainScreenHeight - 100,
    width
  },
  imageStyle: {
    width: wp(65),
    height: hp(65),
    justifyContent: "center",
    alignItems: "center",
    marginRight: wp(10)
  },
  contentContainer: {
    alignItems: "center",
    marginTop: "5%",
    justifyContent: "center"
  },
  instructionText: {
    textAlign: "center",
    color: colors.text.gray.instruction,
    fontSize: fontSizes.detail.small,
    fontFamily: fonts.primary.regular
  }
});

export default styles;
