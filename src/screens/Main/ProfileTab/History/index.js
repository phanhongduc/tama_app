import React, { Component } from "react";
import { Text, View, FlatList } from "react-native";
import { observer, inject } from "mobx-react";
import Swiper from "react-native-swiper";
import Icon from "react-native-vector-icons/MaterialIcons";
import HeaderLeft from "components/HeaderLeft";
import { colors, userRole } from "utils/Constants";
import { monthToString } from "stores/HistoryStore";
import Validate from "utils/Validate";
import styles from "./styles";

@inject("historyScreenStore", "userStore")
@observer
export default class HistoryScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: "Historique",
      headerLeft: <HeaderLeft goBack={() => navigation.goBack()} />
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      activeSlide: 0
    };
  }

  async componentDidMount() {
    const { historyScreenStore } = this.props;
    await historyScreenStore.getInformation();
  }

  nextBtn = () => {
    return <Icon name="chevron-right" size={32} color="#000" />;
  };

  preBtn = () => {
    return <Icon name="chevron-left" size={32} color="#000" />;
  };

  checkIndexIsEven = n => {
    return n % 2 === 0;
  };

  renderItem = ({ item, index }) => {
    const { userStore } = this.props;
    const { user } = userStore;
    const selectedDate = new Date(item.date);
    let date = selectedDate.getDate();
    const month = selectedDate.getMonth();
    let hour = selectedDate.getHours();
    hour = hour < 10 ? `0${hour}` : hour;
    let minutes = selectedDate.getMinutes();
    minutes = minutes < 10 ? `0${minutes}` : minutes;

    date = `${date} ${this.transferMonthToWord(month)}`;
    const content =
      user.role === userRole.EMPLOYEE ? item.name : `${hour}:${minutes}`;

    const amount =
      item.payment_type !== "CREDIT" && user.role === userRole.EMPLOYEE
        ? `-${item.amount}`
        : `+${item.amount}`;
    return (
      <View
        style={[
          styles.transaction,
          {
            backgroundColor: this.checkIndexIsEven(index)
              ? colors.background.gray.item
              : "white"
          }
        ]}
      >
        <Text style={styles.transactionDate}>{date}</Text>
        <Text style={styles.transactionNameAccount}>{content}</Text>
        <Text style={styles.transactionBalance}>{`${amount} XPF`}</Text>
      </View>
    );
  };

  handleSwipeIndexChange = activeSlide => {
    this.setState({ activeSlide });
  };

  transferMonthToWord = m => {
    const month = [];
    month[0] = "Jan";
    month[1] = "Fev";
    month[2] = "Mars";
    month[3] = "Avr";
    month[4] = "Mai";
    month[5] = "Juin";
    month[6] = "Juil";
    month[7] = "Août";
    month[8] = "Sep";
    month[9] = "Oct";
    month[10] = "Nov";
    month[11] = "Dec";

    const n = month[m];
    return n;
  };

  normalizeTitle(delta = 0) {
    const time = monthToString(delta).Time;
    const selectedDate = new Date(time);
    const month = this.transferMonthToWord(selectedDate.getMonth());
    const year = selectedDate.getFullYear();
    return `${month} ${year}`;
  }

  render() {
    const { historyScreenStore } = this.props;
    const { activeSlide } = this.state;

    const currentMonth = this.normalizeTitle(0);
    const lastMonth = this.normalizeTitle(-1);
    const last2Months = this.normalizeTitle(-2);

    const dataMonthTransactionsRenderCurrent = historyScreenStore.thisMonth
      ? historyScreenStore.thisMonth
      : null;
    const dataMonthTransactionsRenderLastMonth = historyScreenStore.lastMonth
      ? historyScreenStore.lastMonth
      : null;
    const dataMonthTransactionsRenderLast2Months = historyScreenStore.last2Month
      ? historyScreenStore.last2Month
      : null;
    let dataRender;
    if (activeSlide === 0) {
      dataRender = dataMonthTransactionsRenderCurrent;
    } else if (activeSlide === 1) {
      dataRender = dataMonthTransactionsRenderLastMonth;
    } else if (activeSlide === 2) {
      dataRender = dataMonthTransactionsRenderLast2Months;
    }

    return (
      <View
        style={{ flex: 1, alignItems: "center", backgroundColor: "#FBFCFC" }}
      >
        <View style={styles.wrapper}>
          <Swiper
            autoplay={false}
            // containerStyle={styles.swiper}
            // style={styles.child}
            showsButtons
            loop={false}
            showsPagination={false}
            nextButton={this.nextBtn()}
            prevButton={this.preBtn()}
            onIndexChanged={this.handleSwipeIndexChange}
            index={activeSlide}
          >
            <View style={styles.slide}>
              <Text style={styles.text}>{currentMonth}</Text>
            </View>
            <View style={styles.slide}>
              <Text style={styles.text}>{lastMonth}</Text>
            </View>
            <View style={styles.slide}>
              <Text style={styles.text}>{last2Months}</Text>
            </View>
          </Swiper>
        </View>
        <View style={styles.scrollView}>
          {Validate.isValidField(dataRender) ? (
            <FlatList
              data={dataRender}
              renderItem={this.renderItem}
              keyExtractor={(item, index) => index.toString()}
            />
          ) : (
            <View style={styles.scrollView}>
              <Text style={{ color: colors.text.gray.buttonRefulser }}>
                No Data
              </Text>
            </View>
          )}
        </View>
      </View>
    );
  }
}
