import { StyleSheet } from "react-native";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import { fonts, colors, fontSizes } from "utils/Constants";

const styles = StyleSheet.create({
  wrapper: {
    height: 64,
    width: "100%"
  },
  swiper: {
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
    flex: 1
  },
  child: {
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
    flex: 1
  },
  slide: {
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
    height: "100%"
  },
  text: {
    color: colors.text.black,
    fontSize: fontSizes.detail.big2,
    fontFamily: fonts.primary.regular
  },
  wrapButtonText: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    paddingBottom: 12,
    height: "100%"
  },
  clickBtnNext: {},
  wrapperBtnPrev: {},
  WrapperScrollView: {},
  scrollView: {
    justifyContent: "center",
    height: hp("65%"),
    paddingLeft: 16,
    paddingRight: 16
  },
  transaction: {
    flexDirection: "row",
    flexWrap: "nowrap",
    alignItems: "center",
    width: "100%",
    height: 52,
    borderRadius: 6,
    paddingLeft: 16,
    paddingRight: 16,
    // marginTop: 8,
    justifyContent: "space-between"
  },
  transactionDate: {
    width: "25%",
    // paddingLeft: 8,
    // paddingRight: 8,
    color: colors.text.gray.icon,
    fontFamily: fonts.primary.regular,
    fontSize: fontSizes.detail.small
  },
  transactionNameAccount: {
    width: "45%",
    color: colors.text.gray.icon,
    fontFamily: fonts.primary.medium,
    fontSize: fontSizes.detail.small
  },
  transactionBalance: {
    // paddingLeft: 8,
    // paddingRight: 8,
    width: "30%",
    color: colors.text.gray.icon,
    fontFamily: fonts.primary.regular,
    fontSize: fontSizes.detail.small,
    textAlign: "right"
  }
});
export default styles;
