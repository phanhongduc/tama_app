import React, { Component } from "react";
import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  Image,
  Linking
} from "react-native";
import { observer, inject } from "mobx-react";
// import ToastMessage from "components/ToastMessage";
import HeaderLeft from "components/HeaderLeft";
import getRNDraftJSBlocks from "react-native-draftjs-render";
import { errorMessage } from "utils/Constants";
import styles from "./styles";

const callIcon = require("assets/images/ic_call.png");

@inject("informationStore", "toastMessageStore")
@observer
export default class AboutUsScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: "A propos",
      headerLeft: <HeaderLeft goBack={() => navigation.goBack()} />
    };
  };

  componentDidMount() {
    const { informationStore } = this.props;
    informationStore.getAboutUs();
    informationStore.getPhoneNumber();
    informationStore.getEmail();
  }

  handleOnPressMail = mail => {
    if (!mail) return;
    Linking.canOpenURL(`mailto:${mail}`)
      .then(supported => {
        if (supported) {
          Linking.openURL(`mailto:${mail}`);
        } else {
          Linking.openURL(
            "https://accounts.google.com/ServiceLogin?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1"
          );
        }
      })
      .catch(error => console.log(error));
  };

  handleOnMakeCall = phoneNumber => {
    const { toastMessageStore } = this.props;
    const actionURL = `tel:${phoneNumber}`;
    Linking.canOpenURL(actionURL)
      .then(supported => {
        if (!supported) {
          toastMessageStore.toast("Phone number in not available");
        } else {
          return Linking.openURL(actionURL);
        }
        return false;
      })
      .catch(err => console.log(err));
  };

  render() {
    const {
      informationStore: { aboutUs, email, phoneNumber }
    } = this.props;

    const aboutUsObject = aboutUs ? JSON.parse(aboutUs) : null;
    const emailObject = email ? JSON.parse(email) : null;
    const phoneObject = phoneNumber ? JSON.parse(phoneNumber) : null;

    if (!aboutUsObject || !emailObject || !phoneObject)
      return (
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <Text style={styles.noContent}>{errorMessage.NO_CONTENT}</Text>
        </View>
      );

    let content = <View />;
    let emailContent = <View />;
    let phoneContent = <View />;
    content = aboutUsObject
      ? getRNDraftJSBlocks({
          contentState: aboutUsObject
        })
      : content;

    emailContent = emailObject
      ? getRNDraftJSBlocks({
          contentState: emailObject
        })
      : emailContent;
    phoneContent = phoneObject
      ? getRNDraftJSBlocks({
          contentState: phoneObject
        })
      : phoneContent;

    return (
      <View style={styles.infoScreenWrapper}>
        <ScrollView
          scrollEventThrottle={100}
          showsVerticalScrollIndicator={false}
        >
          {content}
          {content && (
            <View style={styles.contactInfoContainer}>
              {emailObject ? (
                <TouchableOpacity
                  onPress={() => {
                    const target = emailObject
                      ? emailObject.blocks[0].text
                      : "";
                    this.handleOnPressMail(target);
                  }}
                >
                  {emailContent}
                </TouchableOpacity>
              ) : (
                <View />
              )}
              {phoneObject ? (
                <TouchableOpacity
                  style={styles.callButtonWrapper}
                  onPress={() => {
                    const target = phoneObject
                      ? phoneObject.blocks[0].text
                      : "";
                    this.handleOnMakeCall(target);
                  }}
                >
                  <View style={styles.callIconWrapper}>
                    <Image source={callIcon} style={styles.callIcon} />
                  </View>
                  {phoneContent}
                </TouchableOpacity>
              ) : (
                <View />
              )}
            </View>
          )}
        </ScrollView>
        {/* <ToastMessage /> */}
      </View>
    );
  }
}
