import { StyleSheet } from "react-native";
import { fonts, colors, fontSizes } from "utils/Constants";

const styles = StyleSheet.create({
  infoScreenWrapper: {
    flex: 1,
    alignItems: "center",
    paddingBottom: 100,
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 20
  },
  callIconWrapper: {
    height: 40,
    width: 40,
    justifyContent: "center",
    alignItems: "center",
    marginRight: 10
  },
  callIcon: {},
  contactInfoContainer: {
    width: "100%",
    height: 70,
    marginVertical: 20,
    alignItems: "center",
    justifyContent: "space-between"
  },
  contactInfo: {
    fontFamily: fonts.primary.regular,
    fontSize: fontSizes.detail.big2,
    textAlign: "center"
  },
  callButtonWrapper: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between"
  },
  noContent: {
    fontFamily: fonts.primary.regular,
    fontSize: fontSizes.detail.small,
    color: colors.text.gray.instruction
  }
});

export const markdownStyle = StyleSheet.create({
  text: {
    fontFamily: fonts.primary.regular,
    fontSize: fontSizes.detail.small,
    color: colors.text.gray.instruction,
    paddingLeft: 16,
    paddingRight: 16
  }
});

export default styles;
