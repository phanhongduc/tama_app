/* eslint-disable react/jsx-indent */
import React, { Component } from "react";
import { View, Text, TouchableOpacity, BackHandler } from "react-native";
import { observer, inject } from "mobx-react";
import * as Animatable from "react-native-animatable";
import Icon from "react-native-vector-icons/Feather";
import changePincodeStore from "stores/ChangePincodeStore";
import HeaderLeft from "components/HeaderLeft";
import Keyboard from "components/Keyboard";
import ToastMessage from "components/ToastMessage";
import {
  widthPercentageToDP as wp,
  screenWidth,
  screenHeight
} from "utils/Styling";
import { fonts, fontSizes } from "utils/Constants";
import styles from "./styles";

@inject("keyboardStore", "changePincodeStore", "dataStore")
@observer
export default class ChangePinCodeScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: "Changez votre code Tama’a",
      headerLeft: (
        <HeaderLeft
          goBack={() => {
            if (
              changePincodeStore.numberFailed ===
              changePincodeStore.MAXIMUM_FAILURE
            ) {
              return;
            }
            switch (changePincodeStore.inputType) {
              case changePincodeStore.INPUT_NEW_PINCODE:
              case changePincodeStore.REINPUT_NEW_PINCODE:
                changePincodeStore.handleReInputCurrentPincode();
                break;
              case changePincodeStore.INPUT_CONFIRM_PINCODE:
                changePincodeStore.handleReInputNewPincode();
                break;
              default:
                navigation.pop();
                break;
            }
          }}
        />
      ),
      headerTitleStyle: {
        fontFamily: fonts.primary.semibold,
        fontSize: fontSizes.detail.big2,
        fontWeight: "200",
        marginLeft: screenHeight < 600 && screenWidth < 350 ? 0 : wp(-40),
        marginRight: screenHeight < 600 && screenWidth < 350 ? 0 : wp(-60)
      }
    };
  };

  componentDidMount() {
    const { navigation } = this.props;
    changePincodeStore.initScreen(navigation);
    navigation.setParams({ showTabBar: false });

    this.backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      this.handleHardwareBack
    );
  }

  componentWillUnmount() {
    this.backHandler.remove();
  }

  handleOnDelete = () => {
    const { keyboardStore } = this.props;
    changePincodeStore.removePincode();
    keyboardStore.onDelete();
  };

  handleHardwareBack = () => {
    if (
      changePincodeStore.numberFailed === changePincodeStore.MAXIMUM_FAILURE
    ) {
      return;
    }
    switch (changePincodeStore.inputType) {
      case changePincodeStore.INPUT_NEW_PINCODE:
      case changePincodeStore.REINPUT_NEW_PINCODE:
        changePincodeStore.handleReInputCurrentPincode();
        break;
      case changePincodeStore.INPUT_CONFIRM_PINCODE:
        changePincodeStore.handleReInputNewPincode();
        break;
      default:
        break;
    }
  };

  handleOnValidate = () => {};

  renderTitle = () => {
    switch (changePincodeStore.inputType) {
      case changePincodeStore.INPUT_CURRENT_PINCODE:
        return (
          <Animatable.View
            ref={changePincodeStore.setTitleRef}
            style={styles.titleWrapper}
            useNativeDriver
          >
            <Text style={styles.title}>ENTREZ VOTRE</Text>
            <Text style={styles.title}>CODE TAMA’A ACUTEL</Text>
          </Animatable.View>
        );
      case changePincodeStore.INPUT_NEW_PINCODE:
        return (
          <Animatable.View
            ref={changePincodeStore.setTitleRef}
            style={styles.titleWrapper}
            animation="bounceIn"
            duration={500}
            useNativeDriver
          >
            <Text style={styles.title}>ENTREZ VOTRE</Text>
            <Text style={styles.title}>NOUVEAU CODE TAMA’A</Text>
          </Animatable.View>
        );
      case changePincodeStore.INPUT_CONFIRM_PINCODE:
        return (
          <Animatable.View
            ref={changePincodeStore.setTitleRef}
            style={styles.titleWrapper}
            animation="bounceInRight"
            duration={900}
            useNativeDriver
          >
            <Text style={styles.title}>CONFIRMEZ VOTRE</Text>
            <Text style={styles.title}>NOUVEAU CODE TAMA’A</Text>
          </Animatable.View>
        );
      case changePincodeStore.REINPUT_CURRENT_PINCODE:
        return (
          <Animatable.View
            ref={changePincodeStore.setTitleRef}
            style={styles.titleWrapper}
            animation="bounceInLeft"
            duration={900}
            useNativeDriver
          >
            <Text style={styles.title}>ENTREZ VOTRE</Text>
            <Text style={styles.title}>CODE TAMA’A ACUTEL</Text>
          </Animatable.View>
        );
      case changePincodeStore.REINPUT_NEW_PINCODE:
        return (
          <Animatable.View
            ref={changePincodeStore.setTitleRef}
            style={styles.titleWrapper}
            animation="bounceIn"
            duration={900}
            useNativeDriver
          >
            <Text style={styles.title}>ENTREZ VOTRE</Text>
            <Text style={styles.title}>NOUVEAU CODE TAMA’A</Text>
          </Animatable.View>
        );
      default:
        return <View />;
    }
  };

  render() {
    const { keyboardStore, dataStore } = this.props;

    return (
      <View style={styles.container}>
        <View style={styles.titleContainer}>
          {this.renderTitle()}
          {changePincodeStore.displayError && (
            <Text style={[styles.title, styles.errorMessage]}>
              {changePincodeStore.errorMessage}
            </Text>
          )}
        </View>
        <View style={styles.input}>
          {keyboardStore.length ? (
            <View style={styles.pincodeTextContainer}>
              <View style={styles.pincodeTextWrapper}>
                <Text style={styles.pincodeText}>
                  {changePincodeStore.pincodeViewStr}
                </Text>
              </View>
              <TouchableOpacity
                style={styles.deleteIconWrapper}
                onPress={this.handleOnDelete}
              >
                <Icon name="delete" size={25} style={styles.deleteIcon} />
              </TouchableOpacity>
            </View>
          ) : (
            <View style={styles.underline} />
          )}
        </View>
        {changePincodeStore.forgotOption && (
          <TouchableOpacity
            style={{
              alignItems: "center",
              marginBottom: screenHeight > 700 ? 100 : 50,
              marginTop: screenHeight > 700 ? 30 : 10
            }}
            onPress={changePincodeStore.forgotPincode}
          >
            <Text style={styles.forgotPincodeStyle}>Forgot pincode?</Text>
          </TouchableOpacity>
        )}
        {!changePincodeStore.forgotOption && (
          <View
            style={{
              marginTop: screenHeight > 700 ? 50 : 30,
              marginBottom: screenHeight > 700 ? 100 : 50
            }}
          />
        )}
        <Keyboard
          onPress={changePincodeStore.addPincode}
          color={{ color: dataStore.currentTheme }}
        />
        <ToastMessage />
        {/* <Button
          title="Valider"
          disabled={changePincodeStore.disableButton}
          style={changePincodeStore.disableButton && styles.disabledButton}
        /> */}
      </View>
    );
  }
}
