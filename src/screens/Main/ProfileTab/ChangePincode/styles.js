import { StyleSheet, Dimensions } from "react-native";
import { fontSizes, fonts, colors } from "utils/Constants";

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    marginTop: 20,
    marginBottom: 20,
    backgroundColor: colors.secondary
  },
  input: {
    height: "8%",
    width: "50%",
    maxHeight: 60,
    alignItems: "center",
    marginTop: 50,
    marginBottom: 20
  },
  titleContainer: {
    marginTop: height > 700 ? 50 : 20,
    marginBottom: height > 700 ? 20 : 10,
    alignItems: "center"
  },
  titleWrapper: {
    alignItems: "center"
  },
  title: {
    color: colors.text.black,
    fontSize: fontSizes.title,
    fontFamily: fonts.primary.semibold,
    textAlign: "center"
  },
  errorMessage: {
    width: width * 0.6,
    color: colors.text.red
  },
  pincodeTextContainer: {
    flexDirection: "row",
    alignItems: "center"
  },
  pincodeTextWrapper: {
    alignItems: "center",
    width: "80%",
    height: "100%"
  },
  pincodeText: {
    fontSize: 40,
    color: colors.text.black,
    fontFamily: fonts.primary.medium
  },
  underline: {
    height: 1,
    backgroundColor: colors.text.black,
    width: "100%",
    position: "absolute",
    bottom: 0,
    left: 0
  },
  deleteIconWrapper: {
    height: "100%",
    paddingTop: 5,
    paddingLeft: 10
  },
  deleteIcon: {
    color: colors.text.gray.icon
  },
  disabledButton: {
    backgroundColor: colors.disabled
  },
  forgotPincodeStyle: {
    fontFamily: fonts.primary.regular,
    color: colors.text.gray.transaction,
    fontSize: fontSizes.detail.big2,
    textDecorationLine: "underline"
  }
});

export default styles;
