import React, { Component } from "react";
import { View, ScrollView, Text } from "react-native";
import { observer, inject } from "mobx-react";
import ToastMessage from "components/ToastMessage";
import HeaderLeft from "components/HeaderLeft";
import getRNDraftJSBlocks from "react-native-draftjs-render";
import { errorMessage } from "utils/Constants";
import styles from "./styles";

@inject("informationStore", "toastMessageStore")
@observer
export default class HelpScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: "Aide",
      headerLeft: <HeaderLeft goBack={() => navigation.goBack()} />
    };
  };

  componentDidMount() {
    const { informationStore } = this.props;
    informationStore.getHelp();
  }

  render() {
    const {
      informationStore: { help }
    } = this.props;

    let content = (
      <View>
        <Text>{errorMessage.NO_CONTENT}</Text>
      </View>
    );
    content = help
      ? getRNDraftJSBlocks({
          contentState: JSON.parse(help)
        })
      : content;
    return (
      <View style={styles.infoScreenWrapper}>
        <ScrollView
          scrollEventThrottle={100}
          showsVerticalScrollIndicator={false}
        >
          {content}
        </ScrollView>
        <ToastMessage />
      </View>
    );
  }
}
