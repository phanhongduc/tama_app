import { StyleSheet } from "react-native";
import { fonts, colors, fontSizes } from "utils/Constants";

const styles = StyleSheet.create({
  infoScreenWrapper: {
    flex: 1,
    alignItems: "center",
    paddingBottom: 100,
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 20
  },
  noContent: {
    fontFamily: fonts.primary.regular,
    fontSize: fontSizes.detail.small,
    color: colors.text.gray.instruction
  }
});

export const markdownStyle = StyleSheet.create({
  text: {
    fontFamily: fonts.primary.regular,
    fontSize: fontSizes.detail.small,
    color: colors.text.gray.instruction,
    paddingLeft: 16,
    paddingRight: 16
  }
});

export default styles;
