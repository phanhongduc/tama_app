import React, { Component } from "react";
import {
  View,
  TouchableOpacity,
  Text,
  Modal,
  ScrollView,
  Image
} from "react-native";
import IconMC from "react-native-vector-icons/MaterialCommunityIcons";
import { inject, observer } from "mobx-react";
import navbarStore from "stores/NavbarStore";
import SettingCardItem from "components/SettingCardItem";
import HeaderLeft from "components/HeaderLeft";
import ToastMessage from "components/ToastMessage";
import { colors, userRole, Images } from "utils/Constants";
import { widthPercentageToDP as wp } from "utils/Styling";
import notificationService from "services/NotificationService";
import styles from "./styles";

@inject("userStore", "dataStore")
@observer
export default class SettingScreen extends Component {
  static navigationOptions = ({ navigation, screenProps }) => {
    let title = "Paramètres";
    if (screenProps.theme.role === "restaurant") title = "Profil";

    return {
      headerTitle: title,
      headerLeft: screenProps.theme.role === "employee" && (
        <HeaderLeft goBack={() => navigation.goBack()} />
      ),
      headerLeftStyle: {
        marginLeft: 20,
        paddingLeft: 20
      }
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      logoutSelected: false
    };
  }

  setModalVisible = state => {
    this.setState({ logoutSelected: state });
  };

  confirmExit = async () => {
    const { userStore, navigation } = this.props;

    await userStore.logoutUser();
    this.setModalVisible(false);
    navigation.navigate("SignIn");
    navbarStore.setSelectedTab(
      userStore.user.role === userRole.EMPLOYEE ? "HomeTab" : "APRHomeTab"
    );

    await notificationService.resetToken();
  };

  renderHistoryIcon = () => (
    <Image
      source={Images.IC_SETTING_HISTORY}
      resizeMode="contain"
      style={{ width: wp(23) }}
    />
  );

  renderMyInfoIcon = () => (
    <Image
      source={Images.IC_SETTING_INFO}
      resizeMode="contain"
      style={{ width: wp(23) }}
    />
  );

  renderPincodeIcon = () => (
    <Image
      source={Images.IC_SETTING_CHANGEPIN}
      resizeMode="contain"
      style={{ width: wp(23) }}
    />
  );

  renderContactIcon = () => (
    <Image
      source={Images.IC_SETTING_CONTACT}
      resizeMode="contain"
      style={{ width: wp(23) }}
    />
  );

  renderHelpIcon = () => (
    <Image
      source={Images.IC_SETTING_HELP}
      resizeMode="contain"
      style={{ width: wp(23) }}
    />
  );

  renderAboutUsIcon = () => (
    <Image
      source={Images.IC_SETTING_ABOUT}
      resizeMode="contain"
      style={{ width: wp(23) }}
    />
  );

  render() {
    const { userStore, navigation } = this.props;
    const { logoutSelected } = this.state;
    return (
      <View
        style={{
          flex: 1,
          alignItems: "center",
          backgroundColor: colors.background.nearwhite
        }}
      >
        <ScrollView
          showsVerticalScrollIndicator={false}
          style={styles.scrollView}
        >
          <SettingCardItem
            title="Historique"
            renderIcon={this.renderHistoryIcon}
            page="History"
            navigation={navigation}
          />
          <SettingCardItem
            title="Mes infos"
            renderIcon={this.renderMyInfoIcon}
            page="MesInfos"
            navigation={navigation}
          />
          <SettingCardItem
            title="Changer votre code Tama'a"
            renderIcon={this.renderPincodeIcon}
            page="ChangePincode"
            navigation={navigation}
          />
          <SettingCardItem
            title="Nous contacter"
            renderIcon={this.renderContactIcon}
            page="ContactUs"
            navigation={navigation}
          />
          <SettingCardItem
            title="Aide"
            renderIcon={this.renderHelpIcon}
            page="Help"
            navigation={navigation}
          />
          <SettingCardItem
            title="A propos"
            renderIcon={this.renderAboutUsIcon}
            page="AboutUs"
            navigation={navigation}
          />
          <TouchableOpacity
            style={styles.cardItemView}
            onPress={() => {
              this.setModalVisible(true);
            }}
          >
            <View style={styles.cardItemTitleView}>
              <View style={styles.cardIconWrapper}>
                <Image
                  source={Images.IC_SETTING_LOGOUT}
                  resizeMode="contain"
                  style={{ width: wp(23) }}
                />
              </View>
              <Text style={styles.cardItemText}>Se déconnecter</Text>
            </View>
            <View style={styles.cardItemIconView}>
              <IconMC
                name="chevron-right"
                size={30}
                color={colors.text.gray.chevronRight}
              />
            </View>
          </TouchableOpacity>
          {userStore.user.role === userRole.RESTAURANT && (
            <TouchableOpacity
              style={styles.myqrButtonView}
              onPress={() => navigation.navigate("MyQRCode")}
            >
              <Text style={styles.myqrButtonText}>QR Code</Text>
            </TouchableOpacity>
          )}
        </ScrollView>
        <Modal
          visible={logoutSelected}
          transparent
          hardwareAccelerated
          animationType="fade"
        >
          <View style={styles.modalContainer}>
            <View style={styles.modalWrapper}>
              <View style={styles.modalContentContainer}>
                <Text style={styles.modalTitle}>
                  Voulez-vous vraiment vous déconnecter?
                </Text>
              </View>
              <View style={styles.modalButtonContainer}>
                <TouchableOpacity
                  style={styles.modalCancelButton}
                  onPress={() => this.setModalVisible(false)}
                >
                  <Text style={styles.modalButtonLabel}>Non</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.modalConfirmButton}
                  onPress={this.confirmExit}
                >
                  <Text style={styles.modalButtonLabel}>Oui</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
        <ToastMessage />
      </View>
    );
  }
}
