import { StyleSheet } from "react-native";
import { colors, fontSizes, fonts } from "utils/Constants";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  screenWidth,
  screenHeight
} from "utils/Styling";

const styles = StyleSheet.create({
  scrollView: {
    marginTop: screenHeight < 600 ? hp(30) : hp(50),
    marginBottom: hp(100)
  },
  container: {
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "center",
    backgroundColor: colors.background.nearwhite,
    paddingTop: hp(40)
  },
  cardItemView: {
    width: wp(screenWidth * 0.8),
    height: hp(50),
    backgroundColor: colors.background.white,
    borderRadius: 10,
    margin: wp(5),
    padding: wp(10),
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  cardItemTitleView: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    flex: 3
  },
  cardIconWrapper: {
    flex: 1
  },
  cardItemText: {
    flex: 3.5,
    fontFamily: fonts.primary.medium,
    color: colors.text.gray.icon,
    fontSize: fontSizes.detail.small
  },
  cardItemIconView: {
    flex: 1,
    alignItems: "flex-end"
  },
  modalContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: colors.background.overlay
  },
  modalWrapper: {
    width: wp(screenWidth * 0.7),
    maxWidth: hp(300),
    aspectRatio: 135 / 52,
    borderRadius: 12,
    backgroundColor: colors.background.white
  },
  modalContentContainer: {
    flex: 1,
    alignItems: "center",
    padding: hp(10)
  },
  modalTitle: {
    width: "100%",
    color: colors.text.black,
    fontSize: fontSizes.detail.big2,
    fontFamily: fonts.secondary.regular,
    textAlign: "center"
  },
  modalButtonContainer: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    borderTopWidth: 0.5,
    borderTopColor: colors.text.gray.border,
    width: "100%"
  },
  modalConfirmButton: {
    flex: 1,
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
    borderLeftWidth: 0.5,
    borderLeftColor: colors.text.gray.border
  },
  modalCancelButton: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    borderRightColor: colors.text.gray.border
  },
  modalButtonLabel: {
    color: colors.text.blue,
    fontSize: fontSizes.detail.bigger,
    fontFamily: fonts.secondary.medium
  },
  myqrButtonView: {
    width: wp(175),
    height: hp(52),
    borderRadius: 26,
    backgroundColor: colors.background.orange,
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    marginTop: hp(30),
    marginBottom: hp(30)
  },
  myqrButtonText: {
    fontFamily: fonts.primary.regular,
    fontSize: fontSizes.button.huge,
    color: colors.text.white
  }
});

export default styles;
