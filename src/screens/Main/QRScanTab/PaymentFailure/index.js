import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import Sound from "react-native-sound";
import { NavigationActions, StackActions } from "react-navigation";
import { inject, observer } from "mobx-react";
import Button from "components/Button";
import { userRole } from "utils/Constants";
import styles from "./styles";

const failureIconAPE = require("assets/images/ig_transaction_fail.png");
const failureIconAPR = require("assets/images/ig_transaction_fail_apr.png");

@inject("navbarStore", "qrScanStore", "userStore", "dataStore")
@observer
export default class PaymentFailureScreen extends Component {
  static navigationOptions = {
    headerTitle: "Échec de la transaction",
    headerLeft: null
  };

  componentDidMount() {
    const { navigation } = this.props;
    navigation.setParams({
      navigation
    });

    // Enable playback in silence mode
    Sound.setCategory("Playback");
    this.errorSound = new Sound(
      "sound_failure.mp3",
      Sound.MAIN_BUNDLE,
      error => {
        if (error) {
          console.log("Failed to load the sound", error);
          return;
        }
        this.errorSound.play(success => {
          if (success) {
            // console.log(success);
          } else {
            console.log("something went wrong");
          }
        });
      }
    );
  }

  componentWillUnmount() {
    // Release the audio player resource
    // This should be released when user close app
    this.errorSound.release();
  }

  handleBackToMainScreen = () => {
    const { navigation, navbarStore, userStore } = this.props;
    const resetStack = StackActions.popToTop();
    if (userStore.user.role === userRole.EMPLOYEE) {
      navbarStore.setSelectedTab("HomeTab");
      const resetAction = StackActions.reset({
        key: "HomeTab",
        index: 0,
        actions: [NavigationActions.navigate({ routeName: "MainHome" })]
      });
      navigation.dispatch(resetStack);
      navigation.dispatch(resetAction);
    } else if (userStore.user.role === userRole.RESTAURANT) {
      navbarStore.setSelectedTab("APRHomeTab");
      const resetAction = StackActions.reset({
        key: "APRHomeTab",
        index: 0,
        actions: [
          NavigationActions.navigate({ routeName: "RestaurantHomePage" })
        ]
      });
      navigation.dispatch(resetStack);
      navigation.dispatch(resetAction);
    }
  };

  handleOnPressValidate = () => {
    const { qrScanStore } = this.props;
    this.handleBackToMainScreen();
    qrScanStore.setIsProcessing(false);
  };

  render() {
    const { userStore, dataStore } = this.props;
    return (
      <View style={styles.container}>
        <Text style={styles.title}>{`La transaction \n n’a pas abouti`}</Text>
        {userStore.user.role === userRole.EMPLOYEE && (
          <Image source={failureIconAPE} style={styles.failureIcon} />
        )}
        {userStore.user.role === userRole.RESTAURANT && (
          <Image source={failureIconAPR} style={styles.failureIcon} />
        )}
        <Button
          title="OK"
          onPress={this.handleOnPressValidate}
          style={{ backgroundColor: dataStore.currentTheme }}
        />
        {/* <Image
          source={failureIcon}
          style={styles.failureIcon}
          resizeMode={"contain"}
        />
        <Button title="OK" onPress={this.handleOnPressValidate} /> */}
      </View>
    );
  }
}
