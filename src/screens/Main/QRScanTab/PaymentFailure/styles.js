import { StyleSheet } from "react-native";
import { fontSizes, fonts, colors } from "utils/Constants";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "utils/Styling";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "space-evenly",
    alignItems: "center",
    padding: wp(30)
  },
  title: {
    textAlign: "center",
    color: colors.text.black,
    fontSize: fontSizes.detail.largeTitle,
    fontFamily: fonts.primary.medium
  },
  failureIcon: {
    width: wp(172),
    height: hp(172),
    alignSelf:"center"
  }
});

export default styles;
