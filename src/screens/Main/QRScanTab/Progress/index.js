import React, { Component } from "react";
import { View, Text, ImageBackground } from "react-native";
import { inject, observer } from "mobx-react";
import { userRole, Gifs } from "utils/Constants";
import styles from "./styles";

@inject("progressStore", "navbarStore", "dataStore", "userStore")
@observer
export default class ProgressScreen extends Component {
  static navigationOptions = ({ screenProps }) => {
    const themeApp = screenProps.theme;
    return {
      headerTitle: "Autorisation en cours",
      headerLeft: null,
      headerStyle: {
        backgroundColor: themeApp.currentTheme
      }
    };
  };

  componentDidMount() {
    const { navigation, progressStore } = this.props;

    progressStore.initScreen(navigation);
    progressStore.processPayment();
  }

  renderAPELoader = () => {
    return (
      <ImageBackground
        source={Gifs.TRANSACTION_LOADING}
        style={styles.loadingImage}
        resizeMode="contain"
      >
        <View style={styles.blankView} />
        <View style={styles.titleView}>
          <Text style={styles.titleText}>
            {`Transaction en cours \n Veuillez patienter`}
          </Text>
        </View>
      </ImageBackground>
    );
  };

  renderAPRLoader = () => {
    return (
      <View style={[styles.container, { justifyContent: "center" }]}>
        <ImageBackground
          source={Gifs.TRANSACTION_LOADING_APR}
          style={styles.aprLoadingImage}
          resizeMode="contain"
        />
        <View style={styles.aprTextView}>
          <Text style={styles.titleText}>
            {`Transaction en cours \n Veuillez patienter`}
          </Text>
        </View>
      </View>
    );
  };

  render() {
    const { userStore } = this.props;
    return (
      <View style={styles.container}>
        {userStore.user.role === userRole.EMPLOYEE && this.renderAPELoader()}
        {userStore.user.role === userRole.RESTAURANT && this.renderAPRLoader()}
      </View>
    );
  }
}
