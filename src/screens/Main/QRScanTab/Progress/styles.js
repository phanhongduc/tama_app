import { StyleSheet } from "react-native";
import { fontSizes, fonts, colors } from "utils/Constants";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "utils/Styling";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fbfbfb",
    alignItems: "center"
  },
  loadingImage: {
    height: "100%",
    width: "100%",
    marginBottom: hp(40),
    alignItems: "center",
    justifyContent: "center"
  },
  blankView: { flex: 3 },
  titleView: {
    flex: 2,
    justifyContent: "flex-start",
    alignItems: "center"
  },
  titleText: {
    textAlign: "center",
    color: colors.text.gray.title,
    fontSize: fontSizes.detail.huge,
    fontFamily: fonts.primary.medium
  },
  aprLoadingImage: {
    width: wp(200),
    height: hp(200)
  },
  aprTextView: { marginTop: 20 }
});

export default styles;
