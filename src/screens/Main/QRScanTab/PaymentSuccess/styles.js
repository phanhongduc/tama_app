import { StyleSheet } from "react-native";
import { fontSizes, fonts, colors } from "utils/Constants";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  screenHeight
} from "utils/Styling";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  successIcon: {
    flex: 3,
    width: wp(150),
    height: hp(150),
    marginTop: hp(10),
    marginBottom: hp(20)
  },
  instructionView: {
    flex: 1,
    marginTop: screenHeight < 800 ? hp(40) : hp(70),
    marginBottom: hp(10)
  },
  instructionText: {
    textAlign: "center",
    color: colors.text.black,
    fontSize: fontSizes.detail.largeTitle,
    fontFamily: fonts.primary.medium
  },
  contentContainer: {
    flex: 2,
    alignItems: "center",
    marginBottom: hp(50)
  },
  successTitle: {
    textAlign: "center",
    color: colors.border,
    fontSize: fontSizes.detail.small,
    fontFamily: fonts.primary.regular,
    marginBottom: hp(10)
  },
  amountText: {
    textAlign: "center",
    color: colors.text.black,
    fontSize: fontSizes.detail.extremeLarge,
    fontFamily: fonts.primary.semibold,
    marginTop: hp(10),
    marginBottom: hp(10)
  },
  timeText: {
    textAlign: "center",
    color: colors.text.black,
    fontSize: fontSizes.detail.normal,
    fontFamily: fonts.primary.regular
  }
});

export default styles;
