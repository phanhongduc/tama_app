import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import Sound from "react-native-sound";
import { NavigationActions, StackActions } from "react-navigation";
import { inject, observer } from "mobx-react";
import moment from "moment";
import Button from "components/Button";
import { format } from "utils/Number";
import { userRole } from "utils/Constants";
import {
  // widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "utils/Styling";
import styles from "./styles";

const successIconAPE = require("assets/images/ig_transaction_ok.png");
const successIconAPR = require("assets/images/ig_transaction_ok_apr.png");

@inject("navbarStore", "qrScanStore", "dataStore", "userStore")
@observer
export default class PaymentSuccessScreen extends Component {
  static navigationOptions = ({ screenProps }) => {
    const themeApp = screenProps.theme;
    return {
      title: "Transaction acceptée",
      headerStyle: {
        backgroundColor: themeApp.currentTheme
      },
      headerLeft: null
    };
  };

  componentDidMount() {
    const { navigation } = this.props;
    navigation.setParams({
      navigation
    });

    // Enable playback in silence mode
    Sound.setCategory("Playback");
    this.successSound = new Sound(
      "sound_success.mp3",
      Sound.MAIN_BUNDLE,
      error => {
        if (error) {
          console.log("Failed to load the sound", error);
          return;
        }
        this.successSound.play(success => {
          if (success) {
            // console.log(success);
          } else {
            console.log("something went wrong");
          }
        });
      }
    );
  }

  componentWillUnmount() {
    this.successSound.release();
  }

  handleBackToMainScreen = () => {
    const { navigation, navbarStore, userStore } = this.props;
    const resetStack = StackActions.popToTop();
    if (userStore.user.role === userRole.EMPLOYEE) {
      navbarStore.setSelectedTab("ProfileTab");
      const resetAction = StackActions.reset({
        key: "ProfileTab",
        index: 0,
        actions: [NavigationActions.navigate({ routeName: "Profile" })]
      });
      navigation.dispatch(resetStack);
      navigation.dispatch(resetAction);
    } else if (userStore.user.role === userRole.RESTAURANT) {
      navbarStore.setSelectedTab("APRHomeTab");
      const resetAction = StackActions.reset({
        key: "APRHomeTab",
        index: 0,
        actions: [
          NavigationActions.navigate({ routeName: "RestaurantHomePage" })
        ]
      });
      navigation.dispatch(resetStack);
      navigation.dispatch(resetAction);
    }
    navigation.dispatch(resetStack);
  };

  handleOnPressValidate = () => {
    const { qrScanStore } = this.props;
    this.handleBackToMainScreen();
    qrScanStore.setIsProcessing(false);
  };

  render() {
    const { navigation, userStore, dataStore } = this.props;
    const amount = navigation.getParam("amount", null);
    const time = navigation.getParam("time", null);
    const restaurantName = navigation.getParam("restaurantName", null);

    return (
      <View style={styles.container}>
        <View style={styles.instructionView}>
          <Text style={styles.instructionText}>Présentez cet écran</Text>
          <Text style={styles.instructionText}>au restaurateur</Text>
        </View>
        {userStore.user.role === userRole.EMPLOYEE && (
          <Image
            source={successIconAPE}
            style={styles.successIcon}
            resizeMode="contain"
          />
        )}
        {userStore.user.role === userRole.RESTAURANT && (
          <Image
            source={successIconAPR}
            style={styles.successIcon}
            resizeMode="contain"
          />
        )}
        <View style={styles.contentContainer}>
          <Text
            style={(styles.successTitle, { color: dataStore.currentTheme })}
          >
            VOUS VENEZ DE VERSER
          </Text>
          {amount && (
            <Text style={styles.amountText}>{`${format(amount)} XPF`}</Text>
          )}
          {restaurantName && (
            <Text style={styles.timeText}>{`à ${restaurantName}`}</Text>
          )}
          <Text style={styles.timeText}>
            {moment.unix(time).format("DD/MM/YY [à] HH:mm")}
          </Text>
          <Button
            title="Valider"
            onPress={this.handleOnPressValidate}
            style={{
              backgroundColor: dataStore.currentTheme,
              marginTop: hp(10)
            }}
          />
        </View>
      </View>
    );
  }
}
