import React, { Component } from "react";
import { View, Text, TouchableOpacity, Modal, Image } from "react-native";
import { inject, observer } from "mobx-react";
import Carousel, { Pagination } from "react-native-snap-carousel";
import QRCodeScanner from "react-native-qrcode-scanner";
import qrScanStore from "stores/QRScanStore";
import ToastMessage from "components/ToastMessage";
import { screenWidth, screenHeight } from "utils/Styling";
import { userRole, Images } from "utils/Constants";
import userService from "services/UserService";
import styles from "./styles";

@inject("userStore", "dataStore")
@observer
export default class QRScanScreen extends Component {
  static navigationOptions = ({ screenProps }) => {
    const themeApp = screenProps.theme;
    return {
      headerTitle: "Scannez le QR code",
      headerStyle: {
        backgroundColor: themeApp.currentTheme
      }
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      shouldRender: false,
      modalVisible: false,
      reactivate: true
    };
  }

  componentDidMount() {
    const { navigation } = this.props;
    this.onOneTimeLoad();
    qrScanStore.initScreen(navigation);
    this.removeWillFocusListener = navigation.addListener("willFocus", () => {
      this.setState({ shouldRender: true });
    });
    this.removeWillBlurListener = navigation.addListener("willBlur", () => {
      this.setState({ shouldRender: false });
    });
  }

  setModalVisible(state) {
    this.setState({ modalVisible: state });
  }

  onOneTimeLoad = () => {
    const {
      userStore: { user }
    } = this.props;

    if (!user.readInstruction) {
      this.setModalVisible(true);
    }
  };

  get pagination() {
    const { dataStore, userStore } = this.props;
    return (
      <Pagination
        dotsLength={
          userStore.user.role === userRole.EMPLOYEE
            ? qrScanStore.contentsAPE.length
            : qrScanStore.contentsAPR.length
        }
        activeDotIndex={qrScanStore.activeSlide}
        containerStyle={styles.paginationStyle}
        dotStyle={[
          styles.activeDotStyle,
          { backgroundColor: dataStore.currentTheme }
        ]}
        inactiveDotStyle={styles.inactiveDotStyle}
        inactiveDotOpacity={0.7}
        inactiveDotScale={1}
      />
    );
  }

  handleReadInstruction = () => {
    const { userStore } = this.props;
    const { user } = userStore;
    userService.readInstruction(user.token);
    user.readInstruction = true;
    this.setModalVisible(false);
    this.setState({ shouldRender: true });
  };

  // eslint-disable-next-line class-methods-use-this
  renderItem({ item, index }) {
    return (
      <View key={`instruction_slide_${index}`} style={styles.instructionView}>
        <Text style={styles.instructionTitle}>{item.name}</Text>
        <View style={styles.instructionImageView}>
          <Image
            source={item.image}
            resizeMode="contain"
            style={styles.instructionImage}
          />
        </View>
        <View style={styles.instructionTextView}>
          <Text style={styles.instructionText}>{item.data}</Text>
        </View>
      </View>
    );
  }

  render() {
    const { dataStore, userStore } = this.props;
    const { shouldRender, reactivate, modalVisible } = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.qrContainer}>
          {shouldRender && (
            <QRCodeScanner
              onRead={qrScanStore.onScanSuccess}
              ref={node => {
                this.scanner = node;
              }}
              cameraStyle={{ height: screenHeight }}
              reactivate={reactivate}
              showMarker
              customMarker={() => (
                <Image
                  source={Images.IC_QR_CORNER}
                  resizeMode="contain"
                  style={{ marginBottom: 50 }}
                />
              )}
              checkAndroid6Permissions
            />
          )}
        </View>
        <ToastMessage />
        <Modal
          animationType="slide"
          transparent
          visible={modalVisible}
          onRequestClose={this.handleReadInstruction}
        >
          <View style={styles.modalContainer}>
            <View style={styles.instructionModalView}>
              <Carousel
                ref={c => {
                  this.carousel = c;
                }}
                data={
                  userStore.user.role === userRole.EMPLOYEE
                    ? qrScanStore.contentsAPE
                    : qrScanStore.contentsAPR
                }
                renderItem={this.renderItem}
                activeDotIndex={qrScanStore.activeSlide}
                onSnapToItem={qrScanStore.setActiveSlide}
                sliderWidth={screenWidth}
                itemWidth={screenWidth}
                removeClippedSubviews={false}
              />
            </View>
            <View style={styles.paginationView}>
              {this.pagination}
              {qrScanStore.activeSlide === 3 ? (
                <TouchableOpacity
                  style={[
                    styles.proceedButtonView,
                    { backgroundColor: dataStore.currentTheme }
                  ]}
                  onPress={this.handleReadInstruction}
                >
                  <Text style={styles.proceedButtonText}>
                    Lancez un paiement
                  </Text>
                </TouchableOpacity>
              ) : (
                <View style={styles.emptyButtonView} />
              )}
            </View>

            <TouchableOpacity
              style={styles.closeButtonView}
              onPress={this.handleReadInstruction}
            >
              <Image
                source={Images.IC_CLOSE}
                style={styles.closeButtonStyle}
                resizeMode="contain"
              />
            </TouchableOpacity>
          </View>
        </Modal>
      </View>
    );
  }
}
