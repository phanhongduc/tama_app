import { StyleSheet } from "react-native";
import { fonts, colors, fontSizes } from "utils/Constants";
import { Header } from "react-navigation";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  screenHeight
} from "utils/Styling";

const styles = StyleSheet.create({
  instructionView: { flex: 1, alignItems: "center", justifyContent: "center" },
  instructionTitle: {
    fontFamily: fonts.primary.bold,
    color: colors.text.black,
    fontSize: fontSizes.bigtitle,
    textAlign: "center",
    marginBottom: hp(20),
    marginTop: screenHeight < 600 ? hp(Header.HEIGHT) : 40
  },
  instructionImageView: { marginBottom: hp(20) },
  instructionImage:
    screenHeight > 700
      ? { width: wp(300), height: hp(300) }
      : { width: wp(200), height: hp(200) },
  instructionTextView: { width: wp("80%"), alignItems: "center" },
  instructionText: {
    fontSize: fontSizes.detail.huge,
    textAlign: "center",
    fontFamily: fonts.primary.semibold,
    color: colors.text.black
  },
  container: { flex: 1 },
  qrContainer: { alignItems: "center" },
  modalContainer: {
    flex: 1,
    alignItems: "center",
    backgroundColor: colors.background.white
  },
  activeDotStyle: {
    width: wp(12),
    height: hp(12),
    borderRadius: 6,
    marginHorizontal: wp(5),
    backgroundColor: colors.background.green
  },
  inactiveDotStyle: {
    backgroundColor: colors.background.gray.dot,
    width: wp(12),
    height: hp(12),
    borderRadius: 6,
    marginHorizontal: wp(5)
  },
  paginationStyle: { backgroundColor: colors.background.white },
  instructionModalView: {
    flex: 4,
    backgroundColor: colors.background.white,
    justifyContent: "center",
    alignItems: "center"
  },
  paginationView: {
    flex: 1,
    alignItems: "center",
    marginBottom: hp(30)
  },
  proceedButtonView: {
    height: hp(50),
    backgroundColor: colors.background.green,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 30,
    padding: wp(15)
  },
  proceedButtonText: {
    fontFamily: fonts.primary.medium,
    fontSize: fontSizes.detail.huge,
    color: colors.text.white
  },
  emptyButtonView: {
    width: wp("90%"),
    height: hp(50),
    marginTop: hp(10),
    marginBottom: hp(30)
  },
  closeButtonView: {
    position: "absolute",
    top: screenHeight > 600 ? 40 : 20,
    right: screenHeight > 600 ? 40 : 20
  },
  closeButtonStyle: { flex: 1 }
});

export default styles;
