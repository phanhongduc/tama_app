import React, { Component } from "react";
import { View, Text, TouchableOpacity, Modal } from "react-native";
import { observer, inject } from "mobx-react";
import Icon from "react-native-vector-icons/Feather";
import paymentPincodeStore from "stores/PaymentPincodeStore";
import Keyboard from "components/Keyboard";
import Button from "components/Button";
import ToastMessage from "components/ToastMessage";
import { convertTimeFormat } from "utils/Time";
import { colors } from "utils/Constants";
import styles from "./styles";

@inject(
  "keyboardStore",
  "paymentPincodeStore",
  "navbarStore",
  "dataStore",
  "profileStore"
)
@observer
export default class PaymentPincodeScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: navigation.state.params.restaurantName,
      headerLeft: (
        <TouchableOpacity
          style={styles.cancelButtonWrapper}
          onPress={paymentPincodeStore.toggleCancelModal}
        >
          <Text style={styles.cancelButtonTitle}>Annuler</Text>
        </TouchableOpacity>
      )
    };
  };

  componentDidMount() {
    const { navigation, profileStore } = this.props;
    paymentPincodeStore.initScreen(navigation);
    profileStore.updateData();
  }

  handleOnDelete = () => {
    const { keyboardStore } = this.props;
    keyboardStore.onDelete(() => {
      paymentPincodeStore.removeDigit();
    });
  };

  renderBalanceModal = () => {
    const {
      openBalanceModal,
      toggleBalanceModal,
      employeeBalance
    } = paymentPincodeStore;

    return (
      <Modal
        visible={openBalanceModal}
        animationType="fade"
        transparent
        hardwareAccelerated
      >
        <View style={styles.modalContainer}>
          <View style={styles.balanceModalContainer}>
            <View style={styles.balanceModalContentWrapper}>
              <Text style={styles.balanceModalTitle}>
                Solde disponible à dépenser aujourd’hui à
                {convertTimeFormat(new Date().getTime())}
              </Text>
              <Text style={styles.balanceModalAmount}>
                {`${employeeBalance} XPF`}
              </Text>
            </View>
            <View style={styles.modalButtonWrapper}>
              <TouchableOpacity
                style={styles.modalButton}
                onPress={toggleBalanceModal}
              >
                <Text style={styles.modalButtonTitle}>Fermer</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    );
  };

  renderCancelModal = () => {
    return (
      <Modal
        visible={paymentPincodeStore.openCancelModal}
        animationType="fade"
        transparent
        hardwareAccelerated
      >
        <View style={styles.modalContainer}>
          <View style={styles.closeModalContainer}>
            <View style={styles.closeModalContentWrapper}>
              <Text style={styles.closeModalTitle}>
                Confirmez-vous l’annulation ?
              </Text>
            </View>
            <View style={styles.modalButtonWrapper}>
              <TouchableOpacity
                style={[styles.modalButton, styles.rightVerticalRule]}
                onPress={paymentPincodeStore.toggleCancelModal}
              >
                <Text style={styles.modalButtonTitle}>Non</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.modalButton}
                onPress={paymentPincodeStore.handleCancelPayment}
              >
                <Text style={styles.modalButtonTitle}>Oui</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    );
  };

  render() {
    const { keyboardStore } = this.props;

    return (
      <View style={styles.container}>
        <View style={styles.amountContainer}>
          <Text style={styles.amountTitle}>MONTANT À PAYER</Text>
          <Text style={styles.amountText}>
            {`${paymentPincodeStore.paymentAmount} XPF`}
          </Text>
        </View>
        <Text style={styles.title}>{paymentPincodeStore.titlePincode}</Text>
        <View style={styles.input}>
          {keyboardStore.length ? (
            <View style={styles.pincodeTextContainer}>
              <Text style={styles.pincodeText}>
                {paymentPincodeStore.pincodeViewStr}
              </Text>
              <TouchableOpacity
                style={styles.deleteIconWrapper}
                onPress={this.handleOnDelete}
              >
                <Icon name="delete" size={25} style={styles.deleteIcon} />
              </TouchableOpacity>
            </View>
          ) : (
            <View style={styles.underline} />
          )}
        </View>
        <Keyboard onPress={paymentPincodeStore.addDigit} />
        <TouchableOpacity
          style={styles.viewBalanceContainer}
          onPress={paymentPincodeStore.toggleBalanceModal}
        >
          <Text style={styles.viewBalanceText}>Voir mon solde</Text>
        </TouchableOpacity>
        <Button
          title="Valider"
          disabled={paymentPincodeStore.disableButton}
          style={[
            styles.disabledButton,
            !paymentPincodeStore.disableButton && {
              backgroundColor: colors.primary.green
            }
          ]}
          onPress={paymentPincodeStore.handleOnValidate}
        />
        {this.renderBalanceModal()}
        {this.renderCancelModal()}
        <ToastMessage />
      </View>
    );
  }
}
