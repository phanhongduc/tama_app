import { StyleSheet } from "react-native";
import { fontSizes, fonts, colors } from "utils/Constants";
import { Header } from "react-navigation";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  screenWidth,
  screenHeight
} from "utils/Styling";
const mainScreenHeight = screenHeight - Header.HEIGHT;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "space-evenly",
    backgroundColor: colors.secondary,
    padding: hp(10)
  },
  amountContainer: {
    alignItems: "center",
    marginTop: hp(10)
  },
  amountTitle: {
    color: colors.text.gray.tab,
    fontSize: fontSizes.detail.big,
    fontFamily: fonts.primary.semibold
  },
  amountText: {
    color: colors.text.gray.tab,
    fontSize: fontSizes.detail.amount,
    fontFamily: fonts.primary.medium
  },
  input: {
    alignItems: "center",
    justifyContent: "center",
    width: wp(screenWidth * 0.6),
    height: hp(40)
  },
  titleWrapper: {
    alignItems: "center"
  },
  title: {
    color: colors.text.black,
    fontSize: fontSizes.title,
    fontFamily: fonts.primary.bold,
    textAlign: "center",
    width: wp("90%"),
    marginTop: hp(5)
  },
  pincodeTextContainer: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  pincodeText: {
    fontSize: hp(30),
    color: colors.text.black,
    fontFamily: fonts.primary.bold
  },
  underline: {
    height: hp(1),
    backgroundColor: colors.text.black,
    width: "100%",
    position: "absolute",
    bottom: 0
  },
  balance: {
    fontSize: fontSizes.header,
    fontFamily: fonts.primary.semibold,
    color: colors.text.black
  },
  deleteIconWrapper: {
    position: "absolute",
    right: 0
  },
  deleteIcon: {
    color: colors.text.gray.icon
  },
  disabledButton: {
    backgroundColor: colors.disabled
  },
  viewBalanceContainer: {
    alignItems: "center"
  },
  viewBalanceText: {
    color: colors.text.gray.title,
    fontSize: fontSizes.detail.small,
    fontFamily: fonts.primary.regular,
    textDecorationLine: "underline"
  },
  modalContainer: {
    flex: 1,
    alignItems: "center",
    paddingTop: Header.HEIGHT + mainScreenHeight * 0.2,
    backgroundColor: colors.background.overlay
  },
  balanceModalContainer: {
    width: wp(screenWidth * 0.72),
    maxWidth: wp(300),
    aspectRatio: 3 / 2,
    borderRadius: 12,
    backgroundColor: colors.background.white,
    margin: 10
  },
  balanceModalContentWrapper: {
    flex: 3,
    justifyContent: "space-evenly",
    alignItems: "center"
  },
  balanceModalTitle: {
    color: colors.text.black,
    fontSize: fontSizes.detail.bigger,
    fontFamily: fonts.secondary.medium,
    textAlign: "center"
  },
  balanceModalAmount: {
    color: colors.text.black,
    fontSize: fontSizes.detail.lightBig,
    fontFamily: fonts.secondary.medium
  },
  modalButtonWrapper: {
    flex: 1,
    flexDirection: "row",
    borderTopWidth: 0.5,
    borderTopColor: colors.text.gray.border
  },
  modalButton: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  rightVerticalRule: {
    borderRightWidth: 0.5,
    borderRightColor: colors.text.gray.border
  },
  modalButtonTitle: {
    color: colors.text.blue,
    fontSize: fontSizes.detail.bigger,
    fontFamily: fonts.secondary.medium
  },
  closeModalContainer: {
    width: wp(screenWidth * 0.8),
    maxWidth: wp(300),
    aspectRatio: 135 / 52,
    borderRadius: 12,
    backgroundColor: colors.background.white
  },
  closeModalContentWrapper: {
    height: "57%",
    justifyContent: "center",
    alignItems: "center",
    padding: wp(10)
  },
  closeModalTitle: {
    color: colors.text.black,
    fontSize: fontSizes.detail.bigger,
    fontFamily: fonts.secondary.medium,
    textAlign: "center"
  },
  cancelButtonWrapper: {
    marginLeft: wp(20),
    height: hp(20)
  },
  cancelButtonTitle: {
    color: colors.text.darkGreen,
    fontSize: fontSizes.detail.small,
    fontFamily: fonts.primary.bold
  }
});

export default styles;
