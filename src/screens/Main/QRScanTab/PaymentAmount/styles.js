import { StyleSheet, Dimensions } from "react-native";
import { fontSizes, fonts, colors } from "utils/Constants";
import { Header } from "react-navigation";

const { height } = Dimensions.get("window");
const mainScreenHeight = height - Header.HEIGHT;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "space-around",
    backgroundColor: colors.secondary
  },
  input: {
    height: "8%",
    width: "58%",
    maxHeight: 60,
    alignItems: "center"
  },
  titleWrapper: {
    alignItems: "center"
  },
  title: {
    color: colors.text.black,
    fontSize: fontSizes.title,
    fontFamily: fonts.primary.semibold,
    marginTop: mainScreenHeight * 0.05
  },
  amountTextContainer: {
    flexDirection: "row",
    alignItems: "center"
  },
  amountTextWrapper: {
    alignItems: "center",
    width: "95%",
    height: "100%",
    paddingRight: "2%"
  },
  amountText: {
    fontSize: 45,
    color: colors.text.black,
    fontFamily: fonts.primary.medium
  },
  underline: {
    height: 1,
    backgroundColor: colors.text.black,
    width: "100%",
    position: "absolute",
    bottom: 0,
    left: 0
  },
  balance: {
    fontSize: fontSizes.header,
    fontFamily: fonts.primary.semibold,
    color: colors.text.black
  },
  deleteIconWrapper: {},
  deleteIcon: {
    color: colors.text.gray.icon
  },
  disabledButton: {
    backgroundColor: colors.disabled
  },
  keyboardOrange: {
    color: colors.primary.orange
  },
  buttonOrange: {
    backgroundColor: colors.primary.orange
  }
});

export default styles;
