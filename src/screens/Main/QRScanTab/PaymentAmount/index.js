import React, { Component } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { observer, inject } from "mobx-react";
import Icon from "react-native-vector-icons/Feather";
import HeaderLeft from "components/HeaderLeft";
import Keyboard from "components/Keyboard";
import Button from "components/Button";
import { colors, userRole } from "utils/Constants";
import styles from "./styles";

@inject("keyboardStore", "paymentAmountStore", "userStore", "profileStore")
@observer
export default class PaymentAmountScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: navigation.state.params.restaurantName,
      headerStyle: {
        backgroundColor: colors.primary.orange,
        shadowOffset: { width: 0, height: 0 },
        elevation: 0
      },
      headerLeft: (
        <HeaderLeft
          navigation={navigation}
          goBack={() => navigation.goBack()}
        />
      )
    };
  };

  componentDidMount() {
    const { navigation, paymentAmountStore, profileStore } = this.props;
    paymentAmountStore.initScreen(navigation);
    profileStore.updateData();
  }

  handleOnDelete = () => {
    const { keyboardStore, paymentAmountStore } = this.props;
    keyboardStore.onDelete(() => {
      paymentAmountStore.removeDigit();
    });
  };

  render() {
    const { paymentAmountStore, keyboardStore, userStore } = this.props;
    const { user } = userStore;

    const isRestaurant = user.role === userRole.RESTAURANT;

    return (
      <View style={styles.container}>
        <Text style={styles.title}>MONTANT À PAYER</Text>
        <View style={styles.input}>
          {keyboardStore.length ? (
            <View style={styles.amountTextContainer}>
              <View style={styles.amountTextWrapper}>
                <Text style={styles.amountText}>
                  {paymentAmountStore.amountViewStr}
                </Text>
              </View>
              <TouchableOpacity
                style={styles.deleteIconWrapper}
                onPress={this.handleOnDelete}
              >
                <Icon name="delete" size={25} style={styles.deleteIcon} />
              </TouchableOpacity>
            </View>
          ) : (
            <View style={styles.underline} />
          )}
        </View>
        {isRestaurant ? (
          <Text style={styles.balance}>
            {`Solde disponible : ${paymentAmountStore.balance} XPF`}
          </Text>
        ) : (
          <View>
            <Text style={[styles.balance, { textAlign: "center" }]}>
              {`Solde dépensable aujourd'hui :`}
            </Text>
            <Text style={[styles.balance, { textAlign: "center" }]}>
              {`${paymentAmountStore.balance || "0"}  XPF`}
            </Text>
          </View>
        )}
        <Keyboard
          onPress={paymentAmountStore.addDigit}
          color={styles.keyboardOrange}
        />
        <Button
          title="Valider"
          disabled={paymentAmountStore.disableButton}
          style={
            paymentAmountStore.disableButton
              ? styles.disabledButton
              : styles.buttonOrange
          }
          onPress={async () => {
            await paymentAmountStore.handleOnValidate();
          }}
        />
      </View>
    );
  }
}
