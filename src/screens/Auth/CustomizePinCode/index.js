import React, { Component } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { observer, inject } from "mobx-react";
import * as Animatable from "react-native-animatable";
import Icon from "react-native-vector-icons/Feather";
import customizePinCodeStore from "stores/CustomizePinCodeStore";
import headerLeftStore from "stores/HeaderLeftStore";
import Keyboard from "components/Keyboard";
import HeaderLeft from "components/HeaderLeft";
import ToastMessage from "components/ToastMessage";
import {
  widthPercentageToDP as wp,
  screenHeight,
  screenWidth
} from "utils/Styling";
import { fonts, fontSizes } from "utils/Constants";
import styles from "./styles";

@inject("keyboardStore", "customizePinCodeStore", "dataStore")
@observer
export default class CustomizePinCode extends Component {
  static navigationOptions = () => {
    return {
      headerTitle: "Personnalisez votre code Tama’a",
      headerLeft: (
        <HeaderLeft
          goBack={() => {
            if (
              customizePinCodeStore.numberFailed ===
              customizePinCodeStore.MAXIMUM_FAILURE
            ) {
              return;
            }
            if (
              customizePinCodeStore.inputType ===
              customizePinCodeStore.INPUT_CONFIRM_PINCODE
            ) {
              customizePinCodeStore.handleReInputPincode();
              headerLeftStore.setVisible(false);
            }
          }}
        />
      ),
      headerTitleStyle: {
        fontFamily: fonts.primary.semibold,
        fontSize: fontSizes.detail.big2,
        fontWeight: "200",
        marginLeft: screenHeight < 600 && screenWidth < 350 ? 0 : wp(-40),
        marginRight: screenHeight < 600 && screenWidth < 350 ? 0 : wp(-60)
      }
    };
  };

  componentDidMount() {
    const { navigation } = this.props;
    customizePinCodeStore.initScreen(navigation);
  }

  handleOnDelete = () => {
    const { keyboardStore } = this.props;
    customizePinCodeStore.removePincode();
    keyboardStore.onDelete();
  };

  // handleOnValidate = () => {};

  renderTitle = () => {
    switch (customizePinCodeStore.inputType) {
      case customizePinCodeStore.INPUT_PINCODE:
        return (
          <Animatable.Text
            ref={customizePinCodeStore.setTitleRef}
            style={styles.title}
            useNativeDriver
          >
            ENTREZ VOTRE CODE TAMA’A
          </Animatable.Text>
        );
      case customizePinCodeStore.INPUT_CONFIRM_PINCODE:
        return (
          <Animatable.Text
            ref={customizePinCodeStore.setTitleRef}
            animation="bounceInRight"
            duration={900}
            style={styles.title}
            useNativeDriver
          >
            CONFIRMEZ VOTRE CODE TAMA’A
          </Animatable.Text>
        );
      case customizePinCodeStore.REINPUT_PINCODE:
        return (
          <Animatable.Text
            ref={customizePinCodeStore.setTitleRef}
            animation="bounceInLeft"
            duration={900}
            style={styles.title}
            useNativeDriver
          >
            ENTREZ VOTRE CODE TAMA’A
          </Animatable.Text>
        );
      default:
        return <View />;
    }
  };

  render() {
    const { keyboardStore, dataStore } = this.props;

    return (
      <View style={styles.container}>
        <View style={styles.titleContainer}>
          {this.renderTitle()}
          {customizePinCodeStore.displayError && (
            <Text style={[styles.title, styles.errorMessage]}>
              {customizePinCodeStore.errorMessage}
            </Text>
          )}
        </View>
        <View style={styles.input}>
          {keyboardStore.length ? (
            <View style={styles.pincodeTextContainer}>
              <View style={styles.pincodeTextWrapper}>
                <Text style={styles.pincodeText}>
                  {customizePinCodeStore.pincodeViewStr}
                </Text>
              </View>
              <TouchableOpacity
                style={styles.deleteIconWrapper}
                onPress={this.handleOnDelete}
              >
                <Icon name="delete" size={25} style={styles.deleteIcon} />
              </TouchableOpacity>
            </View>
          ) : (
            <View style={styles.underline} />
          )}
        </View>
        <Keyboard
          onPress={() => customizePinCodeStore.addPincode()}
          color={{ color: dataStore.currentTheme }}
        />
        <ToastMessage />
        {/* <Button
          title="Valider"
          disabled={customizePinCodeStore.disableButton}
          style={
            customizePinCodeStore.disableButton ? styles.disabledButton : null
          }
          onPress={customizePinCodeStore.handleOnValidate}
        /> */}
      </View>
    );
  }
}
