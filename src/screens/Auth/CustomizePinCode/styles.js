import { StyleSheet } from "react-native";
import { fontSizes, fonts, colors } from "utils/Constants";
import { screenWidth } from "utils/Styling";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "space-around",
    backgroundColor: colors.secondary
  },
  input: {
    height: "8%",
    width: screenWidth * 0.58,
    maxHeight: 60,
    alignItems: "center"
  },
  titleContainer: {
    height: 70,
    alignItems: "center"
  },
  titleWrapper: {
    alignItems: "center"
  },
  title: {
    color: colors.text.black,
    fontSize: fontSizes.title,
    fontFamily: fonts.primary.semibold,
    textAlign: "center"
  },
  errorMessage: {
    width: screenWidth * 0.6,
    color: colors.text.red
  },
  pincodeTextContainer: {
    flexDirection: "row",
    alignItems: "center"
  },
  pincodeTextWrapper: {
    alignItems: "center",
    width: "80%",
    height: "100%"
  },
  pincodeText: {
    fontSize: 40,
    color: colors.text.black,
    fontFamily: fonts.primary.medium
  },
  underline: {
    height: 1,
    backgroundColor: colors.text.black,
    width: "100%",
    position: "absolute",
    bottom: 0,
    left: 0
  },
  deleteIconWrapper: {
    height: "100%",
    paddingTop: 5,
    paddingLeft: 10
  },
  deleteIcon: {
    color: colors.text.gray.icon
  },
  disabledButton: {
    backgroundColor: colors.disabled
  }
});

export default styles;
