import { StyleSheet, Platform } from "react-native";
import { fonts, colors, fontSizes, isIOS } from "utils/Constants";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  screenWidth,
  screenHeight
} from "utils/Styling";

const styles = StyleSheet.create({
  containerView: { backgroundColor: colors.background.green },
  contentContainerView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  logoImage: {
    width: screenWidth < 330 ? wp(130) : wp(150),
    height: screenHeight < 600 ? wp(130) : hp(150)
  },
  usernameIconStyle: {
    width: wp(18),
    height: hp(20),
    paddingLeft: wp(20),
    paddingRight: wp(20)
  },
  passwordIconStyle: {
    width: wp(18),
    height: hp(20),
    paddingLeft: wp(20),
    paddingRight: wp(20)
  },
  inputContainer: {
    paddingBottom: hp(20),
    alignItems: "center"
  },
  inputEmailWrapper: {
    width: wp(screenWidth * 0.8),
    height: hp(60),
    flexDirection: "row",
    alignItems: "center",
    borderWidth: 3,
    borderColor: colors.secondary,
    borderRadius: 30,
    marginTop: hp(50),
    paddingLeft: wp(10),
    paddingRight: wp(10)
  },
  inputPasswordWrapper: {
    width: wp(screenWidth * 0.8),
    height: hp(60),
    flexDirection: "row",
    alignItems: "center",
    borderWidth: 3,
    borderColor: colors.secondary,
    borderRadius: 30,
    marginTop: hp(10),
    marginBottom: hp(10),
    paddingLeft: wp(10),
    paddingRight: wp(10)
  },
  textInput: {
    width: isIOS ? wp(screenWidth * 0.65) : screenWidth * 0.55,
    color: colors.text.white,
    fontFamily: fonts.primary.regular,
    fontSize: fontSizes.detail.huge,
    paddingTop: hp(15),
    paddingBottom: hp(15)
  },
  forgotPasswordWrapper: {
    color: colors.text.white,
    textDecorationLine: "underline",
    fontSize: fontSizes.detail.small,
    fontFamily: fonts.primary.regular
  },
  button: {
    backgroundColor: colors.secondary,
    marginTop: hp(50),
    marginBottom: 0
  },
  buttonText: {
    fontFamily: fonts.primary.medium,
    fontSize: fontSizes.button.huge,
    color: colors.text.green
  },
  icon: {
    color: colors.text.white,
    paddingLeft: hp(20),
    paddingRight: hp(20)
  },
  suggestContainer: {
    width: wp("80%"),
    paddingLeft: wp(40),
    paddingRight: wp(30),
    position: "absolute",
    top: hp(110),
    ...Platform.select({
      android: {
        zIndex: 1
      }
    })
  },
  suggestListContainer: {
    backgroundColor: "#FFF",
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    ...Platform.select({
      android: {
        zIndex: 1
      }
    })
  },
  suggestEmailWrapper: {
    paddingLeft: wp(10),
    height: hp(40),
    justifyContent: "center"
  },
  inputEmailContainer: {
    ...Platform.select({
      ios: {
        zIndex: 1
      }
    })
  },
  backIcon: {
    width: 20,
    height: 20,
    marginLeft: 10
  }
});

export default styles;
