import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Image,
  TextInput,
  FlatList
} from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { observer, inject } from "mobx-react";
import Button from "components/Button";
import ToastMessage from "components/ToastMessage";
import LoadingModal from "components/LoadingModal";
import {
  colors,
  fonts,
  fontSizes,
  isAndroid,
  isIOS,
  Images
} from "utils/Constants";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  screenWidth
} from "utils/Styling";
import styles from "./styles";

@inject("userStore", "toastMessageStore", "signInStore", "dataStore")
@observer
export default class SignInScreen extends Component {
  static navigationOptions = ({ screenProps }) => {
    const themeApp = screenProps.theme;
    return {
      header: null,
      headerBackTitle: null,
      headerStyle: {
        backgroundColor: themeApp.themes.employee
      }
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      showPassword: true
    };
  }

  componentDidMount() {
    const { navigation, signInStore } = this.props;
    signInStore.init(navigation);
  }

  renderListSuggest = () => {
    const { signInStore } = this.props;
    let result = null;
    if (signInStore.showSuggest) {
      result = (
        <View style={styles.suggestContainer}>
          <FlatList
            data={signInStore.recentlyEmail}
            contentContainerStyle={styles.suggestListContainer}
            keyExtractor={item => item}
            renderItem={this.renderSuggestItem}
            keyboardShouldPersistTaps="handled"
          />
        </View>
      );
    }

    return result;
  };

  renderSuggestItem = ({ item }) => {
    const { signInStore } = this.props;

    return (
      <TouchableOpacity
        style={styles.suggestEmailWrapper}
        onPress={() => {
          signInStore.inputUsername(item);
          this.passwordInputRef.focus();
        }}
      >
        <Text numberOfLines={1} ellipsizeMode="tail">
          {item}
        </Text>
      </TouchableOpacity>
    );
  };

  render() {
    const { navigation, signInStore } = this.props;
    // const textInputStyle = {
    //   width: wp(screenWidth * 0.5)
    // };
    const { showPassword } = this.state;

    // if (isIOS) {
    //   if (screenWidth > 420) textInputStyle.width = wp(screenWidth * 0.7);
    //   else textInputStyle.width = wp(screenWidth * 0.6);
    // } else if (screenWidth > 420) {
    //   textInputStyle.width = wp(screenWidth * 0.6);
    // }

    return (
      <KeyboardAwareScrollView
        showsVerticalScrollIndicator={false}
        style={styles.containerView}
        contentContainerStyle={styles.contentContainerView}
        enableOnAndroid
        keyboardShouldPersistTaps="handled"
      >
        <Image
          source={Images.IC_LOGO}
          style={styles.logoImage}
          resizeMode="contain"
        />
        <View style={styles.inputContainer}>
          <View style={styles.inputEmailContainer}>
            <View style={styles.inputEmailWrapper}>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Image
                  source={Images.IC_USERNAME}
                  resizeMode="contain"
                  style={styles.usernameIconStyle}
                />
                <TextInput
                  value={signInStore.username}
                  placeholder="Identifiant"
                  placeholderTextColor={colors.secondary}
                  onChangeText={signInStore.inputUsername}
                  style={{
                    // eslint-disable-next-line no-nested-ternary
                    width: isIOS
                      ? screenWidth > 420
                        ? wp(screenWidth * 0.7)
                        : wp(screenWidth * 0.6)
                      : screenWidth > 420
                      ? wp(screenWidth * 0.6)
                      : wp(screenWidth * 0.5),
                    color: colors.text.white,
                    fontFamily: fonts.primary.regular,
                    fontSize: fontSizes.detail.huge,
                    paddingTop: hp(15),
                    paddingBottom: hp(15)
                  }}
                  autoCorrect={false}
                  autoCapitalize="none"
                  underlineColorAndroid="transparent"
                  returnKeyType="next"
                  onSubmitEditing={() => this.passwordInputRef.focus()}
                  editable={signInStore.editableInput}
                  onFocus={() => signInStore.setShowSuggest(true)}
                  onBlur={() => signInStore.setShowSuggest(false)}
                  clearButtonMode="always"
                />
              </View>
              {isAndroid && signInStore.username !== "" && (
                <TouchableOpacity onPress={() => signInStore.inputUsername("")}>
                  <Image
                    source={Images.IC_INPUT_CLOSE}
                    resizeMode="contain"
                    style={styles.backIcon}
                  />
                </TouchableOpacity>
              )}
            </View>
            {this.renderListSuggest()}
          </View>
          <View style={styles.inputPasswordWrapper}>
            <Image
              source={Images.IC_PASS}
              resizeMode="contain"
              style={styles.passwordIconStyle}
            />
            <TextInput
              value={signInStore.password}
              secureTextEntry={showPassword}
              placeholder="Mot de Passe"
              placeholderTextColor={colors.secondary}
              onChangeText={signInStore.inputPassword}
              style={{
                width:
                  screenWidth > 420
                    ? wp(screenWidth * 0.65)
                    : wp(screenWidth * 0.5),
                color: colors.text.white,
                fontFamily: fonts.primary.regular,
                fontSize: fontSizes.detail.huge,
                paddingTop: hp(15),
                paddingBottom: hp(15)
              }}
              autoCorrect={false}
              autoCapitalize="none"
              underlineColorAndroid="transparent"
              returnKeyType="done"
              ref={ref => {
                this.passwordInputRef = ref;
              }}
              onSubmitEditing={signInStore.handleOnLogin}
              editable={signInStore.editableInput}
            />
            {showPassword === false ? (
              <TouchableWithoutFeedback
                onPress={() => this.setState({ showPassword: !showPassword })}
              >
                <Image
                  source={Images.IC_EYE}
                  resizeMode="contain"
                  style={styles.backIcon}
                />
              </TouchableWithoutFeedback>
            ) : (
              <TouchableWithoutFeedback
                onPress={() => this.setState({ showPassword: !showPassword })}
              >
                <Image
                  source={Images.IC_EYE_OFF}
                  resizeMode="contain"
                  style={styles.backIcon}
                />
              </TouchableWithoutFeedback>
            )}
          </View>
          <TouchableOpacity onPress={() => navigation.navigate("Forgot")}>
            <Text style={styles.forgotPasswordWrapper}>
              Mot de passe oublié ?
            </Text>
          </TouchableOpacity>
          <Button
            style={styles.button}
            labelStyle={styles.buttonText}
            title="Connexion"
            onPress={signInStore.handleOnLogin}
            disabled={signInStore.disableButton}
          />
        </View>
        <ToastMessage />
        <LoadingModal isLoading={signInStore.isLoading} />
      </KeyboardAwareScrollView>
    );
  }
}
