import React, { Component } from "react";
import Carousel, { Pagination } from "react-native-snap-carousel";
import { View, Text, Image, TouchableOpacity } from "react-native";
import { observer, inject } from "mobx-react";
import etape1Store from "stores/Etape1Store";
import { screenWidth } from "utils/Styling";
import { userRole } from "utils/Constants";
import styles from "./styles";

@inject("dataStore", "userStore")
@observer
export default class Etape1Screen extends Component {
  static navigationOptions = {
    header: null
  };

  componentDidMount() {
    const { navigation } = this.props;
    etape1Store.initScreen(navigation);
  }

  get pagination() {
    const { dataStore, userStore } = this.props;
    return (
      <Pagination
        dotsLength={
          userStore.user.role === userRole.EMPLOYEE
            ? etape1Store.informationAPE.length
            : etape1Store.informationAPR.length
        }
        activeDotIndex={etape1Store.activeSlide}
        containerStyle={styles.paginationContainerStyle}
        dotStyle={[
          styles.dotStyle,
          { backgroundColor: dataStore.currentTheme }
        ]}
        inactiveDotStyle={styles.inactiveDotStyle}
        inactiveDotOpacity={0.7}
        inactiveDotScale={1}
      />
    );
  }

  // eslint-disable-next-line class-methods-use-this
  renderItem({ item, index }) {
    return (
      <View key={`slide_${index}`} style={styles.itemContainer}>
        <Text style={styles.itemTitle}>{item.name}</Text>
        <View style={styles.imageWrapper}>
          <Image style={styles.img} source={item.image} resizeMode="contain" />
        </View>
        <View>
          <Text style={styles.itemInstruction}>{item.data}</Text>
        </View>
      </View>
    );
  }

  render() {
    const { dataStore, userStore } = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.contentContainer}>
          <Carousel
            ref={etape1Store.setCarouselRef}
            data={
              userStore.user.role === userRole.EMPLOYEE
                ? etape1Store.informationAPE
                : etape1Store.informationAPR
            }
            renderItem={this.renderItem}
            onSnapToItem={etape1Store.setActiveSlide}
            sliderWidth={screenWidth}
            itemWidth={screenWidth}
            removeClippedSubviews={false}
            inactiveSlideOpacity={0}
            contentContainerCustomStyle={styles.contentContainerCarousel}
          />
          {this.pagination}
        </View>
        {etape1Store.activeSlide === 3 ? (
          <TouchableOpacity
            style={[styles.button, { backgroundColor: dataStore.currentTheme }]}
            onPress={etape1Store.handleOnNavigate}
          >
            <Text style={styles.buttonText}>Personnaliser votre code</Text>
          </TouchableOpacity>
        ) : (
          <View style={styles.blankArea} />
        )}
      </View>
    );
  }
}
