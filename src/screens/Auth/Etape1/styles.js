import { StyleSheet } from "react-native";
import { fonts, colors, fontSizes } from "utils/Constants";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  screenWidth,
  screenHeight
} from "utils/Styling";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center"
  },
  button: {
    height: hp(50),
    backgroundColor: colors.background.green,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 30,
    padding: wp(10),
    paddingLeft: wp(20),
    paddingRight: wp(20)
  },
  buttonText: {
    fontFamily: fonts.primary.medium,
    fontSize: fontSizes.detail.huge,
    color: colors.text.white
  },
  blankArea: {
    width: wp("90%"),
    height: hp(50)
  },
  dotStyle: {
    width: wp(12),
    height: hp(12),
    borderRadius: 6,
    marginHorizontal: 5,
    backgroundColor: colors.background.green
  },
  inactiveDotStyle: {
    width: wp(12),
    height: hp(12),
    borderRadius: 6,
    marginHorizontal: 5,
    backgroundColor: colors.background.gray.dot
  },
  itemContainer: {
    justifyContent: "space-evenly",
    alignItems: "center",
    height: hp(screenHeight * 0.8)
  },
  itemTitle: {
    fontFamily: fonts.primary.bold,
    color: colors.text.black,
    fontSize: fontSizes.bigtitle,
    textAlign: "center",
    marginTop: screenHeight < 600 ? 0 : hp(60),
    marginBottom: 0
  },
  imageWrapper: {
    marginTop: hp(20),
    marginBottom: hp(20)
  },
  img: {
    width: wp(200),
    height: hp(200)
  },
  itemInstruction: {
    fontSize: fontSizes.detail.huge,
    textAlign: "center",
    fontFamily: fonts.primary.semibold,
    color: colors.text.black,
    width: wp(screenWidth * 0.8)
  },
  paginationContainerStyle: {
    backgroundColor: colors.background.white,
    marginBottom: 0
  },
  contentContainer: {
    width: wp(screenWidth),
    height:
      screenHeight > 600 ? hp(screenHeight * 0.85) : hp(screenHeight * 0.9),
    justifyContent: "space-evenly",
    alignItems: "center"
  },
  contentContainerCarousel: {
    alignItems: "center"
  }
});
export default styles;
