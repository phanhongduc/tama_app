import { StyleSheet } from "react-native";
import { fonts, colors, fontSizes } from "utils/Constants";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "utils/Styling";

const styles = StyleSheet.create({
  infoScreenWrapper: {
    flex: 1,
    alignItems: "center",
    padding: wp(20)
  },
  btn: {
    alignItems: "center",
    justifyContent: "center",
    width: wp(128),
    aspectRatio: 128 / 52,
    borderRadius: 26
  },
  btnDisabled: {
    backgroundColor: colors.termDisabled
  },
  btnTextDisabled: {
    color: "#E3D3D0",
    textDecorationLine: "none",
    fontSize: fontSizes.detail.big
  },
  btnText: {
    color: colors.text.gray.buttonRefulser,
    textDecorationLine: "underline",
    textDecorationStyle: "solid",
    textDecorationColor: colors.text.gray.buttonRefulser,
    fontSize: fontSizes.detail.big
  },
  btnTextGreen: {
    color: colors.text.white,
    textDecorationLine: "none",
    fontSize: fontSizes.detail.huge
  },
  btnView: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginBottom: hp(20),
    paddingTop: hp(20)
  },
  btnRefuse: {
    color: colors.text.gray.buttonRefulser,
    textDecorationLine: "underline",
    textDecorationStyle: "solid",
    textDecorationColor: colors.text.gray.buttonRefulser,
    fontSize: fontSizes.detail.big
  }
});

export const markdownStyle = StyleSheet.create({
  text: {
    fontFamily: fonts.primary.regular,
    fontSize: fontSizes.detail.small,
    color: colors.text.gray.instruction,
    paddingLeft: 16,
    paddingRight: 16
  }
});

export default styles;
