import React, { Component } from "react";
import { View, ScrollView, Text } from "react-native";
// import Markdown from "react-native-markdown-renderer";
import getRNDraftJSBlocks from "react-native-draftjs-render";
import { observer, inject } from "mobx-react";
import { Header } from "react-navigation";
import Button from "components/Button";
import { colors } from "utils/Constants";
import { screenHeight } from "utils/Styling";
import styles from "./styles";

@inject("dataStore", "termConditionStore")
@observer
export default class TermConditionScreen extends Component {
  static navigationOptions = ({ screenProps }) => {
    const themeApp = screenProps.theme;
    return {
      headerTitle: "Conditions générales",
      headerLeft: null,
      headerStyle: {
        backgroundColor: themeApp.currentTheme,
        shadowOffset: { width: 0, height: 0 },
        elevation: 0,
        borderBottomWidth: 0
      }
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      disableValidateBtn: true
    };
  }

  componentDidMount() {
    const { navigation, termConditionStore } = this.props;
    termConditionStore.init(navigation);
  }

  isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
    const paddingToBottom = 20;

    return (
      layoutMeasurement.height + contentOffset.y >=
      contentSize.height - paddingToBottom
    );
  };

  onContentSizeChange = (contentWidth, contentHeight) => {
    if (!contentWidth) return;
    const mainHeight = screenHeight - Header.HEIGHT - 100;
    if (contentHeight < mainHeight) {
      this.setState({
        disableValidateBtn: false
      });
    }
  };

  render() {
    const { dataStore, termConditionStore } = this.props;
    const { disableValidateBtn } = this.state;
    let content = <Text>Erreur</Text>;
    content = !termConditionStore.isLoading
      ? getRNDraftJSBlocks({
          contentState: JSON.parse(termConditionStore.content)
        })
      : content;

    return (
      <View style={styles.infoScreenWrapper}>
        <ScrollView
          onScroll={({ nativeEvent }) => {
            if (this.isCloseToBottom(nativeEvent)) {
              this.setState({ disableValidateBtn: false });
            } else {
              this.setState({ disableValidateBtn: true });
            }
          }}
          scrollEventThrottle={100}
          onContentSizeChange={this.onContentSizeChange}
          contentContainerStyle={{ paddingBottom: 50, paddingTop: 20 }}
        >
          {content}
        </ScrollView>
        <View
          style={{
            backgroundColor: colors.background.white
          }}
        >
          <View style={styles.btnView}>
            <Button
              disabled={disableValidateBtn}
              style={styles.btn}
              labelStyle={
                disableValidateBtn === true
                  ? [styles.btnRefuse, styles.btnTextDisabled]
                  : styles.btnRefuse
              }
              onPress={termConditionStore.handleRefuse}
              title="Refuser"
            />
            <Button
              disabled={disableValidateBtn}
              style={[
                styles.btn,
                disableValidateBtn
                  ? styles.btnDisabled
                  : { backgroundColor: dataStore.currentTheme }
              ]}
              labelStyle={{}}
              onPress={termConditionStore.handleAccept}
              title="Accepter"
            />
          </View>
        </View>
      </View>
    );
  }
}
