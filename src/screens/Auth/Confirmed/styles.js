import { StyleSheet } from "react-native";
import { fonts, colors, fontSizes } from "utils/Constants";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  screenWidth,
  screenHeight
} from "utils/Styling";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: colors.background.green
  },
  logoImage: {
    width: screenWidth < 330 ? wp(130) : wp(150),
    height: screenHeight < 600 ? wp(130) : hp(150)
  },
  button: {
    backgroundColor: colors.secondary,
    marginTop: hp(70)
  },
  buttonText: {
    fontFamily: fonts.primary.medium,
    fontSize: fontSizes.button.huge,
    color: colors.text.green
  },
  imageContainer: {
    paddingTop: hp(20),
    paddingBottom: hp(20)
  },
  emailTextWrapper: {
    marginTop: hp(40),
    marginBottom: hp(20)
  },
  textWrapper: {
    marginTop: hp(20),
    marginBottom: hp(20)
  },
  title: {
    fontFamily: fonts.primary.regular,
    color: colors.text.white,
    fontSize: fontSizes.detail.largeTitle,
    textAlign: "center"
  },
  instruction: {
    fontFamily: fonts.primary.regular,
    color: colors.text.white,
    fontSize: fontSizes.detail.big2,
    textAlign: "center"
  }
});

export default styles;
