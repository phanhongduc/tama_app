import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import { observer } from "mobx-react";
import Button from "components/Button";
import { Images } from "utils/Constants";
import styles from "./styles";

@observer
export default class ConfirmedScreen extends Component {
  static navigationOptions = {
    header: null
  };

  render() {
    const { navigation } = this.props;
    return (
      <View style={styles.container}>
        <Image
          source={Images.IC_LOGO}
          style={styles.logoImage}
          resizeMode="contain"
        />
        <View style={styles.emailTextWrapper}>
          <Text style={styles.title}>Email envoyé</Text>
        </View>
        <View style={styles.textWrapper}>
          <Text style={styles.instruction}>Vous avez reçu un email</Text>
          <Text style={styles.instruction}>
            avec un lien de réinitialisation
          </Text>
        </View>
        <Button
          style={styles.button}
          labelStyle={styles.buttonText}
          title="Connexion"
          onPress={() => navigation.popToTop()}
        />
      </View>
    );
  }
}
