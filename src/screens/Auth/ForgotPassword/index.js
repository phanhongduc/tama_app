import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  TextInput,
  FlatList,
  TouchableOpacity
} from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { observer, inject } from "mobx-react";
import Button from "components/Button";
import HeaderLeft from "components/HeaderLeft";
import ToastMessage from "components/ToastMessage";
import userService from "services/UserService";
import { colors, errorMessage, isAndroid, Images } from "utils/Constants";
import styles from "./styles";

@inject("toastMessageStore", "signInStore")
@observer
export default class ForgotScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerStyle: {
      borderBottomWidth: 0,
      shadowOpacity: 0,
      elevation: 0,
      backgroundColor: colors.primary.green
    },
    headerLeft: <HeaderLeft goBack={() => navigation.goBack()} />
  });

  constructor(props) {
    super(props);
    this.state = {
      email: "",
      confirmEmail: "",
      noEmail: true
    };
  }

  onConfirmEmail = () => {
    const { email, confirmEmail } = this.state;
    const { signInStore } = this.props;
    if (email === confirmEmail && email !== "") {
      this.setState({ noEmail: true });
      signInStore.saveRecentlyForgotEmail(email);
      this.handleResetPassword();
    } else {
      // this.setState({ noEmail: false });
      const { toastMessageStore } = this.props;
      toastMessageStore.toast(errorMessage.FILL_OUT_EMAIL);
    }
  };

  handleResetPassword = async () => {
    const { navigation, toastMessageStore } = this.props;
    const { email } = this.state;
    try {
      const response = await userService.forgotPassword(email);
      switch (response.statusCode) {
        case 200:
          navigation.navigate("Confirmed");
          break;
        case 602:
          toastMessageStore.toast(errorMessage.EMAIL_NOT_FOUND);
          this.emailRef.focus();
          break;
        default:
          toastMessageStore.toast(errorMessage.SOMETHING_WENT_WRONG);
          break;
      }
    } catch (error) {
      toastMessageStore.toast(errorMessage.SOMETHING_WENT_WRONG);
      console.log("error forgot password: ", error);
    }
  };

  renderListSuggest = () => {
    const { signInStore } = this.props;
    let result = null;
    if (signInStore.showSuggest) {
      result = (
        <View style={styles.suggestContainer}>
          <FlatList
            data={signInStore.recentlyForgotEmail}
            contentContainerStyle={styles.suggestListContainer}
            keyExtractor={item => item}
            renderItem={this.renderSuggestItem}
            keyboardShouldPersistTaps="handled"
          />
        </View>
      );
    }

    return result;
  };

  renderSuggestItem = ({ item }) => {
    // const { signInStore } = this.props;

    return (
      <TouchableOpacity
        style={styles.suggestEmailWrapper}
        onPress={() => {
          this.setState({ email: item });
          this.confirmEmailRef.focus();
        }}
      >
        <Text numberOfLines={1} ellipsizeMode="tail">
          {item}
        </Text>
      </TouchableOpacity>
    );
  };

  render() {
    const { signInStore } = this.props;
    const { email, confirmEmail, noEmail } = this.state;

    return (
      <KeyboardAwareScrollView
        showsVerticalScrollIndicator={false}
        style={{ backgroundColor: colors.background.green }}
        contentContainerStyle={styles.keyboardAwareWrapper}
        enableOnAndroid
        keyboardShouldPersistTaps="handled"
      >
        <Image
          source={Images.IC_LOGO}
          style={styles.logoStyle}
          resizeMode="contain"
        />
        <View style={styles.contentContainer}>
          <View style={styles.titleWrapper}>
            <Text style={styles.title}>Retrouvez votre compte</Text>
            <Text style={styles.subTitle}>Veuillez saisir votre adresse</Text>
            <Text style={styles.subTitle}>
              email pour chercher votre compte
            </Text>
          </View>
          <View style={styles.inputEmailContainer}>
            <View style={styles.inputWrapper}>
              <Image
                source={Images.IC_EMAIL}
                resizeMode="contain"
                style={styles.emailIcon}
              />
              <TextInput
                value={email}
                placeholder="Entrez votre email"
                placeholderTextColor={colors.secondary}
                onChangeText={text => this.setState({ email: text })}
                style={styles.textInput}
                autoCorrect={false}
                autoCapitalize="none"
                underlineColorAndroid="transparent"
                returnKeyType="next"
                onSubmitEditing={() => this.confirmEmailRef.focus()}
                onFocus={() => signInStore.setShowSuggest(true)}
                onBlur={() => signInStore.setShowSuggest(false)}
                ref={ref => {
                  this.emailRef = ref;
                }}
                clearButtonMode="always"
              />
              {isAndroid && email !== "" && (
                <TouchableOpacity onPress={() => this.setState({ email: "" })}>
                  <Image
                    source={Images.IC_INPUT_CLOSE}
                    resizeMode="contain"
                    style={styles.backIcon}
                  />
                </TouchableOpacity>
              )}
            </View>
            {this.renderListSuggest()}
          </View>
          <View style={styles.inputWrapper}>
            <Image
              source={Images.IC_EMAIL}
              resizeMode="contain"
              style={styles.emailIcon}
            />
            <TextInput
              value={confirmEmail}
              placeholder="Confirmez votre email"
              placeholderTextColor={colors.secondary}
              onChangeText={text => this.setState({ confirmEmail: text })}
              style={styles.textInput}
              autoCorrect={false}
              autoCapitalize="none"
              underlineColorAndroid="transparent"
              returnKeyType="done"
              ref={ref => {
                this.confirmEmailRef = ref;
              }}
              clearButtonMode="always"
            />
            {isAndroid && confirmEmail !== "" && (
              <TouchableOpacity
                onPress={() => this.setState({ confirmEmail: "" })}
              >
                <Image
                  source={Images.IC_INPUT_CLOSE}
                  resizeMode="contain"
                  style={styles.backIcon}
                />
              </TouchableOpacity>
            )}
          </View>
          {!noEmail && (
            <View style={styles.errorContainer}>
              <Text style={styles.errorTitle}>Aucun résultat de recherche</Text>
              <Text style={styles.errorSubTitle}>
                Veuillez saisir une autre adresse
              </Text>
            </View>
          )}
          {noEmail && <View style={styles.blankSpace} />}
          <Button
            style={styles.button}
            onPress={this.onConfirmEmail}
            labelStyle={styles.buttonText}
            title="Valider"
          />
          <ToastMessage />
        </View>
      </KeyboardAwareScrollView>
    );
  }
}
