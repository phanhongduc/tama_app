import { StyleSheet, Platform } from "react-native";
import { fonts, colors, fontSizes, isIOS } from "utils/Constants";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  screenWidth,
  screenHeight
} from "utils/Styling";

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    alignItems: "center",
    backgroundColor: colors.primary.green
  },
  contentContainer: {
    alignItems: "center"
  },
  titleWrapper: {
    paddingTop: hp(25),
    paddingBottom: hp(10),
    alignItems: "center"
  },
  title: {
    fontFamily: fonts.primary.regular,
    fontSize: fontSizes.detail.largeTitle,
    color: colors.text.white,
    paddingBottom: hp(5)
  },
  subTitle: {
    fontFamily: fonts.primary.regular,
    fontSize: fontSizes.detail.big2,
    color: colors.text.white,
    textAlign: "center"
  },

  inputWrapper: {
    width: wp("80%"),
    height: hp(55),
    flexDirection: "row",
    alignItems: "center",
    borderWidth: 3,
    borderColor: colors.secondary,
    borderRadius: 30,
    marginTop: hp(10)
    // marginBottom: hp(10)
  },

  textInput: {
    width: isIOS ? wp(screenWidth * 0.65) : screenWidth * 0.55,
    color: colors.text.white,
    fontFamily: fonts.primary.regular,
    fontSize: fontSizes.detail.huge,
    paddingTop: hp(15),
    paddingBottom: hp(15)
  },
  errorContainer: {
    height: hp(40),
    paddingBottom: hp(10)
  },
  errorTitle: {
    fontFamily: fonts.primary.boldItalic,
    fontSize: fontSizes.detail.small,
    color: colors.text.white,
    textAlign: "center"
  },
  errorSubTitle: {
    fontFamily: fonts.primary.regular,
    fontSize: fontSizes.detail.small,
    color: colors.text.white,
    textAlign: "center",
    textDecorationLine: "underline"
  },
  blankSpace: {
    height: hp(40)
  },
  buttonText: {
    fontFamily: fonts.primary.medium,
    fontSize: fontSizes.button.huge,
    color: colors.text.green
  },
  button: {
    backgroundColor: colors.secondary,
    marginTop: hp(10),
    marginBottom: hp(10)
  },
  icon: {
    color: colors.text.white,
    paddingLeft: hp(20),
    paddingRight: hp(20)
  },
  suggestContainer: {
    width: wp("80%"),
    paddingLeft: wp(40),
    paddingRight: wp(30),
    position: "absolute",
    top: hp(60),
    ...Platform.select({
      android: {
        zIndex: 1
      }
    })
  },
  suggestListContainer: {
    backgroundColor: "#FFF",
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    ...Platform.select({
      android: {
        zIndex: 1
      }
    })
  },
  suggestEmailWrapper: {
    paddingLeft: wp(10),
    height: hp(40),
    justifyContent: "center"
  },
  inputEmailContainer: {
    ...Platform.select({
      ios: {
        zIndex: 1
      }
    })
  },
  backIcon: {
    width: 20,
    height: 20,
    marginLeft: 10
  },
  emailIcon: {
    width: wp(21),
    height: hp(14),
    paddingLeft: wp(20),
    paddingRight: wp(20)
  },
  logoStyle: {
    width: screenWidth < 330 ? wp(130) : wp(150),
    height: screenHeight < 600 ? wp(130) : hp(150)
  },
  keyboardAwareWrapper: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
});

export default styles;
