import { observable, action, runInAction } from "mobx";
import { create, persist } from "mobx-persist";
import AsyncStorage from "@react-native-community/async-storage";
import toastMessageStore from "stores/ToastMessageStore";
import userStore from "stores/UserStore";
import networkStore from "stores/NetworkStore";
import searchbarStore from "stores/SearchbarStore";
import restaurantService from "services/RestaurantService";
import { getStatus } from "utils/Time";
import { errorMessage } from "utils/Constants";
import validate from "utils/Validate";

// @remotedev({ name: "restaurant" })
class RestaurantStore {
  @persist("list") @observable listRestaurants = [];

  @persist("list") @observable data = [];

  @observable isLoading = true;

  @observable reloadRemainTimeInterval = null;

  @action
  fetchListRestaurants = async callback => {
    try {
      const isConnected = await networkStore.hasInternetConnection();
      if (!isConnected) {
        runInAction(() => {
          this.isLoading = false;
        });
        if (callback) callback();
        return;
      }

      const { token } = userStore.user;
      const restaurants = await restaurantService.fetchRestaurants(token);
      runInAction(() => {
        if (restaurants.length) {
          this.listRestaurants = restaurants.filter(restaurant =>
            validate.isValidCoordinates(
              restaurant.latitude,
              restaurant.longitude
            )
          );
        }

        searchbarStore.initSourceData(this.listRestaurants, [
          "restaurantName",
          "address"
        ]);

        searchbarStore.initSourceDataList(this.listRestaurants, [
          "restaurantName"
        ]);

        this.processData();

        this.processRemainingTime();

        this.isLoading = false;

        if (callback) {
          callback();
        }
      });
    } catch (error) {
      toastMessageStore.toast(errorMessage.SOMETHING_WENT_WRONG);
      if (error) {
        error();
      }
      console.log("error fetch list restaurants", error);
    }
  };

  @action
  processData = () => {
    try {
      const sections = {};
      this.listRestaurants.forEach(restaurant => {
        if (restaurant.city) {
          const sectionName = restaurant.city.cityName;
          if (!sections[sectionName]) sections[sectionName] = [];
          sections[sectionName].push(restaurant);
        }
      });

      this.data = Object.keys(sections)
        .sort()
        .map(key => {
          return {
            section: key,
            items: sections[key].sort((res1, res2) => {
              const nameRes1 = res1.restaurantName.toUpperCase();
              const nameRes2 = res2.restaurantName.toUpperCase();
              if (nameRes1 < nameRes2) return -1;
              if (nameRes1 > nameRes2) return 1;
              return 0;
            })
          };
        });
    } catch (error) {
      console.log("Erreur ", error);
    }
  };

  @action
  processRemainingTime = () => {
    if (this.listRestaurants) {
      this.listRestaurants.forEach((item, index) => {
        const status = getStatus(item.openingHours);
        this.listRestaurants[index].isOpening = status.isOpening;
        this.listRestaurants[index].time = status.time;
      });
    }
  };

  @action
  setReloadInterval = () => {
    this.reloadRemainTimeInterval = setInterval(
      () => this.processRemainingTime(),
      60 * 1000
    );
  };

  @action
  removeReloadInterval = () => {
    clearInterval(this.reloadRemainTimeInterval);
  };

  @action
  clearData = () => {
    AsyncStorage.removeItem("restaurantStore");
  };
}

const hydrate = create({
  storage: AsyncStorage,
  jsonify: true
});
const restaurantStore = new RestaurantStore();
hydrate("restaurantStore", restaurantStore);
export default restaurantStore;
