import { observable, action, computed } from "mobx";

class KeyboardStore {
  @observable length = 0;

  @observable value = "";

  @observable isBlocking = false;

  @observable blockBeginZero = false; // default value

  @observable MAXLENGTH = 4; // default value

  @action
  onInput = (value, callback) => {
    if (this.isBlocking || (this.MAXLENGTH && this.length === this.MAXLENGTH))
      return;
    if (this.blockBeginZero && !this.length && !Number.parseInt(value, 10))
      return;

    this.length += 1;
    this.value += value;
    if (callback) callback();
  };

  @action
  onDelete = callback => {
    if (!this.length) return;
    this.value = this.value.substr(0, this.length - 1);
    this.length -= 1;
    if (callback) {
      callback();
    }
  };

  @action
  clear = () => {
    this.length = 0;
    this.value = "";
  };

  @action
  setMaxLength = maxLength => {
    this.MAXLENGTH = maxLength;
  };

  @computed
  get getValue() {
    return this.value;
  }

  @action
  blockKeyboard = () => {
    this.isBlocking = true;
  };

  @action
  unblockKeyboard = () => {
    this.isBlocking = false;
  };

  @action
  setBlockBeginZero = status => {
    if (typeof status !== "boolean") return;
    this.blockBeginZero = status;
  };
}

const keyboardStore = new KeyboardStore();
export default keyboardStore;
