import { observable, action, runInAction } from "mobx";
import keyboardStore from "stores/KeyboardStore";
import { format } from "utils/Number";
import { NavigationActions, StackActions } from "react-navigation";
import navbarStore from "stores/NavbarStore";
import userStore from "stores/UserStore";
import networkStore from "stores/NetworkStore";
import qrScanStore from "stores/QRScanStore";
import toastMessageStore from "stores/ToastMessageStore";
import userService from "services/UserService";
import { errorMessage, userRole } from "utils/Constants";

class PaymentPincodeStore {
  @observable titlePincode = "";

  @observable pincodeViewStr = "";

  @observable pincodeValue = null;

  @observable employeeBalance = "0";

  @observable paymentAmount = "0";

  @observable disableButton = true;

  @observable openBalanceModal = false;

  @observable openCancelModal = false;

  @observable navigation = null;

  @observable numberFailed = 0;

  @observable blockKeyboard = false;

  MAX_NUMBER_PINCODE = 4;

  MAXIMUM_FAILURE = 5;

  TITLE_PINCODE_NORMAL = "SAISISSEZ VOTRE CODE TAMA’A";

  TITLE_PINCODE_ERROR = errorMessage.SORRY_TRY_AGAIN;

  @action
  initScreen = navigation => {
    keyboardStore.clear();
    keyboardStore.setMaxLength(this.MAX_NUMBER_PINCODE);
    keyboardStore.setBlockBeginZero(false);
    this.pincodeViewStr = "";
    this.pincodeValue = null;
    this.disableButton = true;
    this.navigation = navigation;
    this.titlePincode = this.TITLE_PINCODE_NORMAL;
    this.numberFailed = 0;

    const amount = navigation.getParam("amount", 0);
    this.paymentAmount = format(amount);

    this.updateEmployeeBalance(); // Get available balance offline
    this.fetchEmployeeInfo(); // Update new balance
  };

  @action
  addDigit = () => {
    if (
      this.pincodeViewStr.length / 2 === this.MAX_NUMBER_PINCODE ||
      this.blockKeyboard
    )
      return;

    this.pincodeViewStr += "* ";
    if (this.pincodeViewStr.length / 2 === this.MAX_NUMBER_PINCODE) {
      this.disableButton = false;
    }
  };

  @action
  removeDigit = () => {
    if (!this.pincodeViewStr.length) return;

    this.pincodeViewStr = this.pincodeViewStr.substr(
      0,
      this.pincodeViewStr.length - 2
    );
    if (!this.disabledButton) {
      this.disableButton = true;
    }
  };

  @action
  toggleBalanceModal = () => {
    this.fetchEmployeeInfo();
    this.openBalanceModal = !this.openBalanceModal;
  };

  @action
  toggleCancelModal = () => {
    this.openCancelModal = !this.openCancelModal;
  };

  fetchEmployeeInfo = async () => {
    try {
      const isConnected = await networkStore.hasInternetConnection();
      if (!isConnected) return;

      await userStore.getInfo();
      this.updateEmployeeBalance();
    } catch (error) {
      toastMessageStore.toast(errorMessage.SOMETHING_WENT_WRONG);
      console.log("error while fetching new balance", error);
    }
  };

  @action
  updateEmployeeBalance = async () => {
    const { employeeBalance } = userStore.user;
    if (employeeBalance) {
      this.employeeBalance = format(employeeBalance);
    }
  };

  @action
  handleOnValidate = async () => {
    try {
      const isConnected = await networkStore.hasInternetConnection();
      if (!isConnected) {
        this.handleBackToMainScreen();
        qrScanStore.setIsProcessing(false);
        return;
      }

      this.pincodeValue = keyboardStore.getValue;
      const { token, role } = userStore.user;
      let response = null;

      if (role === userRole.EMPLOYEE) {
        response = await userService.verifyPincode(token, this.pincodeValue);
      } else {
        response = await userService.verifyPincode(
          token,
          this.pincodeValue,
          qrScanStore.getScanId
        );
      }

      switch (response.statusCode) {
        case 200: {
          const amount = this.navigation.getParam("amount", null);
          const restaurantName = this.navigation.getParam(
            "restaurantName",
            null
          );
          this.navigation.navigate("Progress", { amount, restaurantName });
          break;
        }
        case 601:
          this.numberFailed += 1;
          if (this.numberFailed === this.MAXIMUM_FAILURE) {
            this.numberFailed = 0;
            toastMessageStore.toast(errorMessage.VALIDATION_FAIL);
            qrScanStore.setIsProcessing(false);
            this.handleBackToMainScreen();
          } else {
            toastMessageStore.toast(errorMessage.PLEASE_TRY_AGAIN);
            runInAction(() => {
              keyboardStore.clear();
              keyboardStore.blockKeyboard();
              this.pincodeViewStr = "";
              this.disableButton = true;
              this.blockKeyboard = true;
              this.titlePincode = this.TITLE_PINCODE_ERROR;
            });
            setTimeout(() => {
              this.blockKeyboard = false;
              keyboardStore.unblockKeyboard();
              this.titlePincode = this.TITLE_PINCODE_NORMAL;
            }, 3000);
          }
          break;
        default:
          toastMessageStore.toast(errorMessage.PLEASE_TRY_AGAIN);
          break;
      }
    } catch (error) {
      toastMessageStore.toast(errorMessage.SOMETHING_WENT_WRONG);
      console.log("error payment pincode", error);
    }
  };

  handleCancelPayment = () => {
    this.toggleCancelModal();
    this.handleBackToMainScreen();
    qrScanStore.setIsProcessing(false);
  };

  handleBackToMainScreen = () => {
    const resetStack = StackActions.popToTop();
    if (userStore.user.role === userRole.EMPLOYEE) {
      navbarStore.setSelectedTab("HomeTab");
      const resetAction = StackActions.reset({
        key: "HomeTab",
        index: 0,
        actions: [NavigationActions.navigate({ routeName: "MainHome" })]
      });
      this.navigation.dispatch(resetStack);
      this.navigation.dispatch(resetAction);
    } else if (userStore.user.role === userRole.RESTAURANT) {
      navbarStore.setSelectedTab("APRHomeTab");
      const resetAction = StackActions.reset({
        key: "APRHomeTab",
        index: 0,
        actions: [
          NavigationActions.navigate({ routeName: "RestaurantHomePage" })
        ]
      });
      this.navigation.dispatch(resetStack);
      this.navigation.dispatch(resetAction);
    }
  };
}

const paymentPincodeStore = new PaymentPincodeStore();
export default paymentPincodeStore;
