import { observable, action, computed } from "mobx";
import { getStatus, validateDaysAllowed } from "utils/Time";
import { errorMessage, userRole } from "utils/Constants";
import { StackActions, NavigationActions } from "react-navigation";
import { format } from "utils/Number";
import keyboardStore from "./KeyboardStore";
import userStore from "./UserStore";
import toastMessageStore from "./ToastMessageStore";
import networkStore from "./NetworkStore";
import qrScanStore from "./QRScanStore";
import navbarStore from "./NavbarStore";

class PaymentAmountStore {
  @observable amountViewStr = "";

  @observable balance = "0"; // maximum display +2000

  @observable availableBalance = null;

  @observable disableButton = true;

  @observable navigation = null;

  MAX_LENGTH_DIGIT = 6;

  @action
  initScreen = navigation => {
    keyboardStore.clear();
    keyboardStore.setMaxLength(6);
    keyboardStore.setBlockBeginZero(true);
    this.navigation = navigation;
    this.amountViewStr = "";
    this.disableButton = true;

    this.updateEmployeeBalance(); // Get available balance offline
    this.fetchEmployeeInfo(); // Update new balance
  };

  @action
  addDigit = () => {
    if (this.disableButton) {
      this.disableButton = false;
    }
    this.amountViewStr = format(keyboardStore.getValue);
  };

  @action
  removeDigit = () => {
    if (!keyboardStore.getValue.length) {
      this.disableButton = true;
    }
    this.amountViewStr = format(keyboardStore.getValue);
  };

  fetchEmployeeInfo = async () => {
    try {
      const isConnected = await networkStore.hasInternetConnection();
      if (!isConnected) return;

      await userStore.getInfo();
      this.updateEmployeeBalance();
    } catch (error) {
      toastMessageStore.toast(errorMessage.SOMETHING_WENT_WRONG);
      console.log("error while fetching new balance", error);
    }
  };

  @action
  updateEmployeeBalance = async () => {
    const { role, employeeBalance, availableBalance } = userStore.user;
    this.availableBalance = this.navigation.getParam("availableBalance", 0);
    this.balance = this.navigation.getParam("currentBalance", 0);
    if (role === userRole.EMPLOYEE) {
      this.availableBalance = availableBalance;
      this.balance = employeeBalance;
    }
    // console.log(JSON.stringify(this.availableBalance));
    // console.log(JSON.stringify(this.balance));

    // availableBalance == null
    if (typeof this.availableBalance === "object" && !this.availableBalance) {
      if (typeof this.balance === "object" && !this.balance) {
        this.balance = "0";
        return;
      }
      if (this.balance > 2000) this.balance = "+2000";
      return;
    }
    this.balance =
      this.availableBalance >= this.balance
        ? this.balance
        : this.availableBalance;
    if (this.balance > 2000) this.balance = "+2000";
  };

  handleOnValidate = async () => {
    try {
      const isConnected = await networkStore.hasInternetConnection();
      if (!isConnected) {
        this.handleBackToMainScreen();
        return;
      }

      if (this.validateAmount()) {
        const restaurantName = this.navigation.getParam("restaurantName", null);
        const amount = Number.parseInt(keyboardStore.getValue, 10);

        this.navigation.navigate("PaymentPincode", {
          amount,
          restaurantName
        });
      } else {
        // throw "Error check validateAmount";
      }
    } catch (error) {
      toastMessageStore.toast(errorMessage.SOMETHING_WENT_WRONG);
      console.log("error payment amount: ", error);
    }
  };

  validateAmount = () => {
    try {
      const amount = Number.parseInt(keyboardStore.getValue, 10);
      const { role } = userStore.user;

      if (role === userRole.EMPLOYEE) {
        const {
          employeeBalance,
          availableBalance,
          employeeConditions
        } = userStore.user;

        switch (true) {
          // days allowed
          case this.checkDaysAllowed(employeeConditions):
            this.showErrorMessage(errorMessage.OVER_DAY_SLOT);
            return false;
          // time slot allowed
          case !this.checkTimeAllowed(employeeConditions):
            this.showErrorMessage(errorMessage.OVER_TIME_SLOT);
            return false;
          // daily amount
          case amount > Number.parseInt(availableBalance, 10):
            this.showErrorMessage(errorMessage.YOU_REACH_MAXIMUM_DAYLY_AMOUNT);
            return false;
          // employeeBalance == null
          case typeof employeeBalance === "object" && !employeeBalance:
            this.showErrorMessage(errorMessage.YOUR_BALANCE_NOT_ENOUGH);
            return false;
          // balance available
          case amount > Number.parseInt(employeeBalance, 10):
            this.showErrorMessage(errorMessage.YOUR_BALANCE_NOT_ENOUGH);
            return false;
          case this.isBalanceExpired:
            this.showErrorMessage(errorMessage.YOUR_BALANCE_EXPIRED);
            return false;
          default:
            return true;
        }
      }
      if (role === userRole.RESTAURANT) {
        const employeeBalance = this.navigation.getParam("currentBalance", 0);
        const availableBalance = this.navigation.getParam(
          "availableBalance",
          0
        );
        const employeeConditions = this.navigation.getParam(
          "employeeCondition",
          null
        );

        switch (true) {
          // days allowed
          case this.checkDaysAllowed(employeeConditions):
            this.showErrorMessage(errorMessage.OVER_DAY_SLOT);
            return false;
          // time slot allowed
          case !this.checkTimeAllowed(employeeConditions):
            this.showErrorMessage(errorMessage.OVER_TIME_SLOT);
            return false;
          // daily amount
          case amount > Number.parseInt(availableBalance, 10):
            this.showErrorMessage(errorMessage.YOU_REACH_MAXIMUM_DAYLY_AMOUNT);
            return false;
          // employeeBalance == null
          case typeof employeeBalance === "object" && !employeeBalance:
            this.showErrorMessage(errorMessage.YOUR_BALANCE_NOT_ENOUGH);
            return false;
          // balance available
          case amount > Number.parseInt(employeeBalance, 10):
            this.showErrorMessage(errorMessage.YOUR_BALANCE_NOT_ENOUGH);
            return false;
          case this.isBalanceExpired:
            this.showErrorMessage(errorMessage.YOUR_BALANCE_EXPIRED);
            return false;
          default:
            return true;
        }
      }
      return false;
    } catch (error) {
      return Promise.reject(error);
    }
  };

  handleBackToMainScreen = () => {
    if (userStore.user.role === userRole.EMPLOYEE) {
      this.navigation.popToTop();
      navbarStore.setSelectedTab("Hometab");
      const resetAction = StackActions.reset({
        key: "HomeTab",
        index: 0,
        actions: [NavigationActions.navigate({ routeName: "MainHome" })]
      });
      this.navigation.dispatch(resetAction);
      qrScanStore.setIsProcessing(false);
    } else {
      this.navigation.popToTop();
      navbarStore.setSelectedTab("APRHometab");
      const resetAction = StackActions.reset({
        key: "APRHomeTab",
        index: 0,
        actions: [
          NavigationActions.navigate({ routeName: "RestaurantHomePage" })
        ]
      });
      this.navigation.dispatch(resetAction);
      qrScanStore.setIsProcessing(false);
    }
  };

  @action
  showErrorMessage = error => {
    toastMessageStore.toast(error);
    keyboardStore.clear();
    this.amountViewStr = "";
    this.disableButton = true;
  };

  @computed
  get isBalanceExpired() {
    const { role } = userStore.user;
    const currDate = new Date();
    if (role === userRole.RESTAURANT) {
      const employeeExpiredDate = this.navigation.getParam(
        "employeeExpirationDate"
      );

      return employeeExpiredDate < currDate;
    }
    const { expirationDate } = userStore.user;
    const expiredDate = new Date(expirationDate);
    return expiredDate < currDate;
  }

  // eslint-disable-next-line class-methods-use-this
  checkTimeAllowed(condition) {
    const status = getStatus(condition);
    return status.isOpening;
  }

  // eslint-disable-next-line class-methods-use-this
  checkDaysAllowed(condition) {
    return validateDaysAllowed(condition);
  }
}

const paymentAmountStore = new PaymentAmountStore();
export default paymentAmountStore;
