import { observable, action } from "mobx";

class RestaurantCardItemStore {
  @observable openChoosePhoneModal = false; // Not use yet

  @action
  toggleChoosePhoneModal = () => {
    this.openChoosePhoneModal = !this.openChoosePhoneModal;
  };
}

const restaurantCardItemStore = new RestaurantCardItemStore();
export default restaurantCardItemStore;
