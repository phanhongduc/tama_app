import { observable, action, runInAction } from "mobx";
import { create, persist } from "mobx-persist";
import AsyncStorage from "@react-native-community/async-storage";
// import { errorMessage } from "utils/Constants";
import informationService from "services/InformationService";
// import toastMessageStore from "./ToastMessageStore";
import networkStore from "./NetworkStore";
import userStore from "./UserStore";

class InformationStore {
  @persist @observable aboutUs = null;

  @persist @observable contactUs = null;

  @persist @observable help = null;

  @persist @observable phoneNumber = null;

  @persist @observable email = null;

  @action
  getAboutUs = async () => {
    try {
      const isConnected = await networkStore.hasInternetConnection();
      if (!isConnected) return;

      const { token } = userStore.user;
      const response = await informationService.getAboutUs(token);

      if (response.content) {
        runInAction(() => {
          this.aboutUs = response.content;
        });
      }
    } catch (error) {
      // toastMessageStore.toast(errorMessage.SOMETHING_WENT_WRONG);
      console.log("Error get about us: ", error);
    }
  };

  @action
  getContactUs = async () => {
    try {
      const isConnected = await networkStore.hasInternetConnection();
      if (!isConnected) return;

      const { token } = userStore.user;
      const response = await informationService.getContactUs(token);

      if (response.content) {
        runInAction(() => {
          this.contactUs = response.content;
        });
      }
    } catch (error) {
      // toastMessageStore.toast(errorMessage.SOMETHING_WENT_WRONG);
      console.log("Error get about us: ", error);
    }
  };

  @action
  getHelp = async () => {
    try {
      const isConnected = await networkStore.hasInternetConnection();
      if (!isConnected) return;

      const { token } = userStore.user;
      const response = await informationService.getHelp(token);

      if (response.content) {
        runInAction(() => {
          this.help = response.content;
        });
      }
    } catch (error) {
      // toastMessageStore.toast(errorMessage.SOMETHING_WENT_WRONG);
      console.log("Error get about us: ", error);
    }
  };

  @action
  getPhoneNumber = async () => {
    try {
      const isConnected = await networkStore.hasInternetConnection();
      if (!isConnected) return;

      const { token } = userStore.user;
      const response = await informationService.getPhone(token);

      if (response.content) {
        runInAction(() => {
          this.phoneNumber = response.content;
        });
      }
    } catch (error) {
      // toastMessageStore.toast(errorMessage.SOMETHING_WENT_WRONG);
      console.log("Error get about us: ", error);
    }
  };

  @action
  getEmail = async () => {
    try {
      const isConnected = await networkStore.hasInternetConnection();
      if (!isConnected) return;

      const { token } = userStore.user;
      const response = await informationService.getEmail(token);

      if (response.content) {
        runInAction(() => {
          this.email = response.content;
        });
      }
    } catch (error) {
      // toastMessageStore.toast(errorMessage.SOMETHING_WENT_WRONG);
      console.log("Error get about us: ", error);
    }
  };

  @action
  clearData = () => {
    AsyncStorage.removeItem("informationStore");
  };
}

const hydrate = create({
  storage: AsyncStorage,
  jsonify: true
});

const informationStore = new InformationStore();
hydrate("informationStore", informationStore);

export default informationStore;
