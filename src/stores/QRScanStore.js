import { observable, computed, action, runInAction } from "mobx";
import toastMessageStore from "stores/ToastMessageStore";
import networkStore from "stores/NetworkStore";
import userService from "services/UserService";
import { errorMessage, userRole, Images } from "utils/Constants";
import userStore from "./UserStore";

class QRScanStore {
  @observable contentsAPE = [
    {
      name: "Etape 1",
      data: "Scannez le QR code \nde l'établissement",
      image: Images.IG_INSTRUCTION_1
    },
    {
      name: "Etape 2",
      data: "Le restaurateur saisit le \n montant de votre commande",
      image: Images.IG_INSTRUCTION_2
    },
    {
      name: "Etape 3",
      data: "Entrez votre \ncode Tama’a pour\n valider la transaction",
      image: Images.IG_INSTRUCTION_3
    },
    {
      name: "Transaction\nacceptée",
      data: "Présentez \nvotre justificatif\n au restaurateur",
      image: Images.IG_INSTRUCTION_4
    }
  ];

  @observable contentsAPR = [
    {
      name: "Etape 1",
      data: "Scannez le QR code \ndu client",
      image: Images.IG_INSTRUCTION_1_APR
    },
    {
      name: "Etape 2",
      data: "Saisissez le montant\nde la commande",
      image: Images.IG_INSTRUCTION_2_APR
    },
    {
      name: "Etape 3",
      data: "Le client entre son \n code Tama'a pour valider la transaction",
      image: Images.IG_INSTRUCTION_3_APR
    },
    {
      name: "Transaction\nacceptée",
      data: "Le justificatif de\n paiement s'affiche\n à l'écran",
      image: Images.IG_INSTRUCTION_4_APR
    }
  ];

  @observable activeSlide = 0;

  @observable carouselRef = null;

  @observable navigation = null;

  @observable isProcessing = false;

  @observable scanId = null;

  @action
  initScreen = navigation => {
    this.navigation = navigation;
    this.activeSlide = 0;
    this.scanId = null;
  };

  @action
  setActiveSlide = index => {
    this.activeSlide = index;
  };

  @action
  setCarouselRef = ref => {
    this.carouselRef = ref;
  };

  handleHardwareBack = () => {
    if (this.activeSlide) {
      this.activeSlide -= 1;
      this.carouselRef.snapToPrev();
    }
  };

  @action
  onScanSuccess = async e => {
    // isProcessing always == true
    // console.log("Success", JSON.stringify(this.isProcessing));

    if (this.isProcessing) return;
    this.setIsProcessing(true);

    try {
      const isConnected = await networkStore.hasInternetConnection();
      if (!isConnected) {
        this.setIsProcessing(false);
        return;
      }
      const { user } = userStore;
      const customId = e.data;
      const response = await userService.scanQRCode(user.token, customId); // Handle request for both two roles
      if (response.statusCode === 200) {
        let { restaurantName } = response;
        if (user.role === userRole.RESTAURANT) {
          restaurantName = user.restaurantName;
        }

        this.navigation.navigate("PaymentAmount", {
          restaurantName, // Used for ROLE_EMPLOYEE
          currentBalance: response.currentBalance, // Used for ROLE_RESTAURANT
          scanner: this.scanner,
          availableBalance: response.dailyAvailableBalance,
          employeeCondition: response.employeeConditions,
          employeeExpirationDate: response.expirationDate
        });
        runInAction(() => {
          this.scanId = customId;
        });
      } else {
        this.setIsProcessing(false);
        console.log("data", JSON.stringify(response.message));

        toastMessageStore.toast(response.message);
      }
    } catch (error) {
      this.setIsProcessing(false);
      toastMessageStore.toast(errorMessage.SOMETHING_WENT_WRONG);
      console.log("Error scan qrcode: ", error);
    }
  };

  @action
  setIsProcessing = status => {
    this.isProcessing = status;
  };

  @computed
  get getScanId() {
    return this.scanId;
  }

  @action
  resetScan = () => {
    this.scanId = null;
  };
}

const qrScanStore = new QRScanStore();
export default qrScanStore;
