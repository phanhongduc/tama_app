import { observable, action } from "mobx";
import ImagePicker from "react-native-image-crop-picker";
import userService from "services/UserService";
import userStore from "stores/UserStore";
import ToastMessageStore from "stores/ToastMessageStore";
import { isAndroid, errorMessage } from "utils/Constants";

const createFormData = (photos = []) => {
  const data = new FormData();
  let i = 0;
  photos.forEach(photo => {
    const part = photo.path.split("/");
    const name = `${part[part.length - 1].replace(" ", "")}`;
    const uri = !isAndroid
      ? photo.path
      : photo.path.replace("file://", "file:");
    data.append(`photo[${i}]`, {
      name,
      type: `image/jpg`,
      uri
    });
    i += 1;
  });

  return data;
};

class RestaurantHomepageStore {
  @observable photosForUpload = null;

  @action
  pickFromGallery = async id => {
    const { user } = userStore;
    const { token } = user;
    const options = {
      multiple: true,
      mediaType: `photo`
    };
    console.log("options", JSON.stringify(options));
    const photosFromLibrary = await ImagePicker.openPicker(options);
    // console.log("photosFromLibrary", JSON.stringify(photosFromLibrary));
    if (photosFromLibrary.length > 10) {
      ToastMessageStore.toast("Alert", errorMessage.EXCEED_10_PHOTOS);
      return { status: "failed" };
    }
    const photosForUpload = createFormData(photosFromLibrary);
    console.log("pick", JSON.stringify(photosForUpload));

    const result = await userService.uploadPhotos(id, photosForUpload, token);

    if (Object.prototype.toString.call(result) === "[object Array]") {
      let i = 0;
      let hasFailed = false;
      while (!hasFailed && i < result.length) {
        if (result[i].status === "fail") {
          hasFailed = true;
        } else {
          i += 1;
        }
      }

      if (hasFailed) {
        ToastMessageStore.toast("Alert", errorMessage.SOME_FAILED_PHOTOS);
      }
      return result;
    }
    ToastMessageStore.toast("Alert", errorMessage.UPLOAD_FAILED);
    return { status: "failed" };
  };

  @action
  pickFromCamera = async id => {
    const { user } = userStore;
    const { token } = user;

    const options = {
      multiple: true,
      mediaType: `photo`
    };
    const photosFromCamera = await ImagePicker.openCamera(options);
    const photosForUpload = createFormData([photosFromCamera]);

    return userService.uploadPhotos(id, photosForUpload, token);
  };
}

const restaurantHomepageStore = new RestaurantHomepageStore();
export default restaurantHomepageStore;
