import { observable, action, runInAction } from "mobx";
import { NavigationActions } from "react-navigation";
import userService from "services/UserService";
import { userRole, errorMessage } from "utils/Constants";
import userStore from "./UserStore";
import toastMessageStore from "./ToastMessageStore";
import networkStore from "./NetworkStore";

class TermConditionStore {
  @observable content = "";

  @observable isLoading = true;

  @observable navigation = null;

  @action
  init = navigation => {
    this.navigation = navigation;
    this.getContent();
  };

  @action
  getContent = async () => {
    try {
      const isConnected = await networkStore.hasInternetConnection();
      if (!isConnected) return;

      let response = null;
      const { role, token } = userStore.user;
      if (role === userRole.EMPLOYEE) {
        response = await userService.getEmployeeTermCondition(token);
      } else if (role === userRole.RESTAURANT) {
        response = await userService.getRestaurantTermCondition(token);
      }

      runInAction(() => {
        this.content = response.content;
        this.isLoading = false;
      });
    } catch (error) {
      toastMessageStore.toast(errorMessage.SOMETHING_WENT_WRONG);
      console.log("error term condition", error);
    }
  };

  handleRefuse = async () => {
    await userStore.logoutUser();
    this.navigation.goBack();
  };

  handleAccept = async () => {
    try {
      const response = await userService.acceptTermCondition(
        userStore.user.token
      );

      switch (response.statusCode) {
        case 200:
          this.navigation.reset(
            [NavigationActions.navigate({ routeName: "Etape1" })],
            0
          );
          break;
        default:
          toastMessageStore.toast(errorMessage.SOMETHING_WENT_WRONG);
          console.log("error term condition", response);
          break;
      }
    } catch (error) {
      toastMessageStore.toast(errorMessage.SOMETHING_WENT_WRONG);
      console.log("error term condition", error);
    }
  };
}

const termConditionStore = new TermConditionStore();
export default termConditionStore;
