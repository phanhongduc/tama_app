import { observable, action } from "mobx";
import { userRole } from "utils/Constants";
import profileStore from "./ProfileStore";
import userStore from "./UserStore";

class NavbarStore {
  @observable selected =
    userStore.user.role === userRole.EMPLOYEE ? "HomeTab" : "APRHomeTab";

  @action
  setSelectedTab = tab => {
    this.selected = tab;
    if (tab === "ProfileTab") {
      profileStore.getData();
    }
  };
}

const navbarStore = new NavbarStore();
export default navbarStore;
