import { observable, action } from "mobx";

class ToastMessageStore {
  @observable showModal = false;

  @observable title = "";

  @observable subTitle = "";

  @action
  toast = (title, subTitle) => {
    this.showModal = true;
    this.title = title;
    this.subTitle = subTitle;
  };

  @action
  closeModal = () => {
    this.showModal = false;
    this.title = "";
    this.subTitle = "";
  };
}

const toastMessageStore = new ToastMessageStore();
export default toastMessageStore;
