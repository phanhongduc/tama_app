import { observable, action } from "mobx";

class DataStore {
  @observable themes = {
    employee: "#00C68E",
    restaurant: "#FFB57A"
  };

  @observable role = "employee";

  @observable currentTheme = this.themes.employee;

  @action
  setTheme = role => {
    this.currentTheme = this.themes[role];
  };

  @action setRole = role => {
    this.role = role;
  };
}

const dataStore = new DataStore();
export default dataStore;
