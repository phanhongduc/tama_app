import { observable, action } from "mobx";
import { NavigationActions, StackActions } from "react-navigation";
import lang from "lodash/lang";
import userStore from "stores/UserStore";
import dataStore from "stores/DataStore";
import networkStore from "stores/NetworkStore";
import headerTitleStore from "stores/HeaderTitleStore";
import navbarStore from "stores/NavbarStore";
import notificationService from "services/NotificationService";
import userService from "services/UserService";

import { userRole } from "utils/Constants";

class SplashStore {
  @observable navigation = null;

  @observable notificationPayload = null;

  @action
  init = navigation => {
    this.navigation = navigation;
  };

  handleValidateAccount = async () => {
    try {
      await userStore.init();
      if (lang.isEmpty(userStore.user) || lang.isEmpty(userStore.user.token)) {
        this.navigation.navigate("Auth");
        return;
      }

      const isConnected = await networkStore.hasInternetConnection();
      if (isConnected) {
        this.handleValidateOnline();
      } else {
        this.checkNavigationFlow();
      }
    } catch (error) {
      console.log("Error validate account", error);
      this.navigation.navigate("Auth");
    }
  };

  handleValidateOnline = async () => {
    try {
      const deviceInfoData = await userStore.getDeviceInfo();
      const response = await userService.login(
        userStore.user.token,
        null,
        null,
        deviceInfoData
      );

      if (response.statusCode !== 200) {
        this.navigation.navigate("Auth");
      } else {
        userStore.saveLoginData(response);
        userStore.getInfo();
        dataStore.setRole(
          userStore.user.role === userRole.EMPLOYEE ? "employee" : "restaurant"
        );
        dataStore.setTheme(
          userStore.user.role === userRole.EMPLOYEE ? "employee" : "restaurant"
        );
        this.checkNavigationFlow();
      }
    } catch (error) {
      this.navigation.navigate("Auth");
      console.log("Error validate online", error);
    }
  };

  checkNavigationFlow = async () => {
    try {
      headerTitleStore.setUsername(userStore.user.firstName);
      const { role } = userStore.user;
      if (role === userRole.EMPLOYEE) {
        switch (true) {
          case userStore.user.hasPincode:
            if (this.notificationPayload) {
              notificationService.handleOnOpenNotification(
                this.notificationPayload
              );
              this.notificationPayload = null;
            } else {
              this.navigation.navigate("HomeTab");
              navbarStore.setSelectedTab("HomeTab");
            }
            break;
          case userStore.user.conditionAccepted === false:
            await userStore.logoutUser();
            this.navigation.navigate("Auth");
            break;
          case !userStore.user.readAboutApp:
            this.navigation.navigate("Etape1");
            break;
          case userStore.user.hasPincode === false: {
            const resetAction = StackActions.reset({
              index: 0,
              actions: [
                NavigationActions.navigate({ routeName: "CustomizePinCode" })
              ]
            });
            this.navigation.dispatch(resetAction);
            break;
          }
          default:
            break;
        }
      } else if (role === userRole.RESTAURANT) {
        switch (true) {
          case userStore.user.hasPincode:
            this.navigation.navigate("APRHomeTab");
            navbarStore.setSelectedTab("APRHomeTab");
            break;
          case userStore.user.conditionAccepted === false:
            await userStore.logoutUser();
            this.navigation.navigate("Auth");
            break;
          case !userStore.user.readAboutApp:
            this.navigation.navigate("Etape1");
            break;
          case userStore.user.hasPincode === false: {
            const resetAction = StackActions.reset({
              index: 0,
              actions: [
                NavigationActions.navigate({ routeName: "CustomizePinCode" })
              ]
            });
            this.navigation.dispatch(resetAction);
            break;
          }
          default:
            break;
        }
      } else {
        throw new Error("Error check navigation flow");
      }
    } catch (error) {
      this.navigation.navigate("Auth");
      console.log("Error check navigation flow: ", error);
    }
  };

  @action
  setNotificationPayload = notification => {
    this.notificationPayload = notification;
  };
}

const splashStore = new SplashStore();
export default splashStore;
