import { observable, action, runInAction } from "mobx";
import DeviceInfo from "react-native-device-info";
import AsyncStorage from "@react-native-community/async-storage";
import userService from "services/UserService";
import toastMessageStore from "stores/ToastMessageStore";
import headerTitleStore from "stores/HeaderTitleStore";
import informationStore from "stores/InformationStore";
import restaurantStore from "stores/RestaurantStore";
import searchbarStore from "stores/SearchbarStore";
import { userRole, errorMessage } from "utils/Constants";
import { getStatus } from "utils/Time";
import * as KeyChain from "react-native-keychain";

const storeKeyChain = async data => {
  try {
    const result = await KeyChain.setGenericPassword(
      "something",
      JSON.stringify(data)
    );
    return result;
  } catch (error) {
    console.log("Error At StoreKeyChain", JSON.stringify(error));
    return false;
  }
};
// const storeKeyChain = async data => {
//   try {
//     const result = AsyncStorage.setItem("something", JSON.stringify(data));
//     return !!result;
//   } catch (error) {
//     console.log("Error At StoreKeyChain", JSON.stringify(error));
//     return false;
//   }
// };

const defaultData = {
  id: "",
  firstName: "",
  surName: "",
  company: "",
  email: "",
  phoneNumber: "",
  employeeBalance: null,
  availableBalance: "",
  token: null,
  hasPincode: false,
  conditionAccepted: false,
  readAboutApp: false,
  readInstruction: false,
  role: "",
  employeeConditions: null,
  expirationDate: null,
  dailyAmount: null,
  customId: null,
  deviceToken: "",
  isLoggedout: false
};

const getUserInfo = async () => {
  let info = await KeyChain.getGenericPassword();
  if (info) {
    info = JSON.parse(info.password);
    return info;
  }
  return {
    ...defaultData
  };
};
// const getUserInfo = async () => {
//   let info = await AsyncStorage.getItem("something");
//   if (info) {
//     info = JSON.parse(info.password);
//     return info;
//   }
//   return {
//     ...defaultData
//   };
// };

class UserStore {
  @observable user = {
    ...defaultData
  };

  @action
  init = async () => {
    const data = await getUserInfo();
    this.user = {
      ...data,
      deviceToken: this.user.deviceToken
    };
  };

  saveUserInfo = async () => {
    const result = await storeKeyChain(this.user);
    return result;
  };

  clearUserInfo = async () => {
    await storeKeyChain(defaultData);
  };

  @action
  saveLoginData = async data => {
    const role =
      data.roles.find(r => r === userRole.EMPLOYEE) || userRole.RESTAURANT;

    this.user = {
      ...this.user,
      token: data.accessToken,
      hasPincode: data.hasPINCode,
      conditionAccepted: data.conditionAccepted,
      readAboutApp: data.readAboutApp,
      readInstruction: data.readInstruction,
      role,
      id: data.IDInfo,
      customId: data.customID
    };
    await this.saveUserInfo();
  };

  @action
  logoutUser = async () => {
    try {
      userService.logout(this.user.token);

      await this.clearUserInfo();
      informationStore.clearData();
      restaurantStore.clearData();
      searchbarStore.clearData();
      this.clearData();
      await this.dropSession();
      // await AsyncStorage.clear();
    } catch (error) {
      console.log("Error logout: ", error);
    }
  };

  @action
  getInfo = async () => {
    try {
      const { role } = this.user;
      if (role === userRole.EMPLOYEE) {
        await this.getEmployeeInfo();
      } else if (role === userRole.RESTAURANT) {
        await this.getRestaurantInfo();
      } else {
        return Promise.reject(new Error("Error getInfo: Cannot check role"));
      }
      this.saveUserInfo();
      return Promise.resolve(true);
    } catch (error) {
      toastMessageStore.toast(errorMessage.SOMETHING_WENT_WRONG);
      console.log("error: ", error);
      return Promise.reject(error);
    }
  };

  @action
  getEmployeeInfo = async () => {
    try {
      const { id, token } = this.user;
      const user = await userService.getEmployeeInfo(token, id);

      headerTitleStore.setUsername(user.firstName);

      runInAction(() => {
        if (user) {
          this.user = {
            ...this.user,
            email: user.email,
            firstName: user.firstName,
            surName: user.lastName,
            phoneNumber: user.phoneNumber,
            companyName: user.companyName,
            employeeConditions: user.employeeConditions,
            employeeBalance: user.balance,
            availableBalance: user.dailyAvailableBalance,
            expirationDate: user.expirationDate,
            dailyAmount: user.dailyAmount,
            userID: user.user.id
          };
        }
      });
    } catch (error) {
      toastMessageStore.toast(errorMessage.SOMETHING_WENT_WRONG);
      console.log("error: ", error);
    }
  };

  @action
  getRestaurantInfo = async () => {
    try {
      const { id, token } = this.user;
      const user = await userService.getRestaurantInfo(token, id);
      const status = getStatus(user.openingHours);

      runInAction(() => {
        if (user) {
          this.user = {
            ...this.user,
            restaurantName: user.restaurantName,
            corporateName: user.corporateName,
            email: user.email,
            facebook: user.facebook,
            instagram: user.instagram,
            website: user.website,
            description: user.description,
            latitude: user.latitude,
            longitude: user.longitude,
            phoneNumber1: user.phoneNumber1,
            phoneNumber2: user.phoneNumber2,
            address: user.address,
            cardAccepted: user.cardAccepted,
            images: user.photo,
            avatar: user.avatar,
            openingHours: user.openingHours,
            userID: user.user.id,
            ...status
          };
        }
      });
    } catch (error) {
      toastMessageStore.toast(errorMessage.SOMETHING_WENT_WRONG);
      console.log("error: ", error);
    }
  };

  dropSession = async () => {
    try {
      const { token } = this.user;
      return await userService.dropSession(token);
    } catch (error) {
      // toastMessageStore.toast(errorMessage.SOMETHING_WENT_WRONG);
      console.log("error: ", error);
    }
  };

  getDeviceInfo = async () => {
    const deviceInfo = {
      deviceId: DeviceInfo.getUniqueID(),
      deviceName: DeviceInfo.getDeviceName(),
      deviceVersion: DeviceInfo.getSystemVersion(),
      deviceIpAddress: await DeviceInfo.getIPAddress(),
      deviceToken: this.user.deviceToken
    };

    return JSON.stringify(deviceInfo);
  };

  @action
  clearData = async () => {
    if (this.user.isLoggedout) {
      await AsyncStorage.removeItem("userStore");
      this.user = {
        ...defaultData
      };
      console.log("clear", JSON.stringify(this.user));
    }
  };

  // @action
  // clearData = () => {
  //   if (this.user.isLoggedout) {
  //     AsyncStorage.removeItem("userStore");
  //     this.user = observable({
  //       ...defaultData
  //     });
  //     console.log("clear", JSON.stringify(this.user));
  //   }
  // };
}

const userStore = new UserStore();

export default userStore;
