import { observable, computed, action } from "mobx";
import { userRole } from "utils/Constants";
import userService from "services/UserService";
import userStore from "./UserStore";

const today = new Date();
const monthToString = delta => {
  const selectedDate = new Date(
    today.getFullYear(),
    today.getMonth() + delta,
    today.getDay()
  );

  const month = selectedDate.getMonth() + 1;
  const selectedMonth = month < 10 ? `0${month}` : month;
  const year = selectedDate.getFullYear();

  return {
    YearMonth: `${year}-${selectedMonth}`,
    MonthYear: `${selectedMonth}-${year}`,
    Time: selectedDate.getTime()
  };
};

const Private = {
  toMonthDateHistory: (delta = 0, history) => {
    const selectedMonth = monthToString(delta).YearMonth;
    let result = [];

    if (history && typeof history === "object") {
      const transactions = history[selectedMonth];
      if (!transactions) return [];
      transactions.forEach(transaction => {
        result.push(transaction);
      });

      result = result.sort((trans1, trans2) => {
        const firstDate = new Date(trans1.date);
        const secDate = new Date(trans2.date);
        return secDate.getTime() - firstDate.getTime();
      });
    }
    return result;
  }
};

// const fakeData = {
//   [userRole.EMPLOYEE]: {
//     "2019-07": [
//       {
//         date: "2019-07-30T05:30:13+02:00",
//         name: "employee1",
//         amount: 10
//       },
//       {
//         date: "2019-07-30T05:30:13+02:00",
//         name: "employee2",
//         amount: 10
//       }
//     ],
//     "2019-06": [],
//     "2019-05": [
//       {
//         date: "2019-05-30T05:30:13+02:00",
//         name: "employee1",
//         amount: 10
//       }
//     ]
//   },
//   [userRole.RESTAURANT]: {
//     "2019-07": [
//       {
//         date: "2019-07-30T05:30:13+02:00",
//         payment_type: "credit",
//         amount: 10
//       },
//       {
//         date: "2019-07-30T05:30:13+02:00",
//         payment_type: "credit",
//         amount: 10
//       }
//     ],
//     "2019-06": [],
//     "2019-05": [
//       {
//         date: "2019-05-30T05:30:13+02:00",
//         payment_type: "credit",
//         amount: 10
//       }
//     ]
//   }
// };

class HistoryScreenStore {
  @observable content;

  @observable information;

  @observable isLoading = false;

  @action
  getInformation = async () => {
    const { role, userID, token } = userStore.user;

    let response = null;
    try {
      if (role === userRole.EMPLOYEE) {
        response = await userService.getHistory(userID, "employee", token);
        // this.information = { ...fakeData[userRole.EMPLOYEE] };
      } else {
        response = await userService.getHistory(userID, "restaurant", token);
        // this.information = { ...fakeData[userRole.RESTAURANT] };
      }
      if (response) {
        this.information = { ...response };
      }
    } catch (error) {
      console.log(`Error${JSON.stringify(error)}`);
    }
  };

  @computed
  get lastMonth() {
    return Private.toMonthDateHistory(-1, this.information);
  }

  @computed
  get thisMonth() {
    return Private.toMonthDateHistory(0, this.information);
  }

  @computed
  get last2Month() {
    return Private.toMonthDateHistory(-2, this.information);
  }
}
const historyScreenStore = new HistoryScreenStore();
export default historyScreenStore;
export { monthToString };
