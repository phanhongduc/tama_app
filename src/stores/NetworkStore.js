import { observable } from "mobx";
import NetInfo from "@react-native-community/netinfo";
import { errorMessage } from "utils/Constants";
import toastMessageStore from "./ToastMessageStore";

class NetworkStore {
  @observable isConnected = true;

  @observable unsubscribe = null;

  // constructor() {
  //   this.unsubscribe = NetInfo.addEventListener(
  //     state => this.checkConnection(state)
  //   );
  // }

  // @action
  // setNetworkStatus = ({ isConnected }) => {
  //   this.isConnected = isConnected;
  // };

  // @action
  // checkConnection = ({ isConnected }) => {
  //   console.log("isConnected", isConnected);
  //   console.log('this.isConnected', this.isConnected)
  //   if (this.isConnected !== isConnected) {
  //     this.unsubscribe();
  //     this.unsubscribe = NetInfo.addEventListener(state =>
  //       this.checkConnection(state)
  //       );
  //     }
  //     this.setIsConnected(isConnected);

  // };

  // @action
  // setIsConnected = isConnected => {
  //   this.isConnected = isConnected;
  //   console.log("internet: ", isConnected);
  //   toastMessageStore.toast("internet " + isConnected);
  // };

  hasInternetConnection = async () => {
    try {
      const state = await NetInfo.fetch();
      if (!state.isConnected) {
        toastMessageStore.toast(errorMessage.NO_INTERNET_CONNECTION);
        return false;
      }

      return true;
    } catch (error) {
      console.log("Error check internet connection: ", error);
      return false;
    }
  };
}

const networkStore = new NetworkStore();
export default networkStore;
