import { observable, action, runInAction } from "mobx";
import { NavigationActions } from "react-navigation";
import AsyncStorage from "@react-native-community/async-storage";
import dataStore from "stores/DataStore";
import userService from "services/UserService";
import notificationService from "services/NotificationService";
import Validate from "utils/Validate";
import array from "lodash/array";
import { create, persist } from "mobx-persist";
import { userRole, errorMessage } from "utils/Constants";
import splashStore from "stores/SplashStore";
import toastMessageStore from "stores/ToastMessageStore";
import userStore from "stores/UserStore";
import networkStore from "stores/NetworkStore";
import navbarStore from "stores/NavbarStore";

class SignInStore {
  @observable username = "";

  @observable password = "";

  @observable navigation = null;

  @observable isConnected = true;

  @observable blockTiming = null;

  @observable numberFailed = 0;

  @observable disableButton = false;

  @observable editableInput = true;

  @observable showSuggest = false;

  @observable isLoading = false;

  @persist("list") @observable recentlyEmail = [];

  @persist("list") @observable recentlyForgotEmail = [];

  MAXIMUM_FAILURE = 5;

  BLOCK_TIMEOUT = 30000; // ms

  @action
  init = navigation => {
    this.username = "";
    this.password = "";
    this.navigation = navigation;
    this.disableButton = false;
    this.editableInput = true;
  };

  @action
  inputUsername = username => {
    this.username = username;
  };

  @action
  inputPassword = password => {
    this.password = password;
  };

  handleOnLogin = async () => {
    try {
      this.isLoading = true;
      const isValidInput = this.validateInput();
      console.log("handleOnLogin isValidInput", isValidInput);
      if (!isValidInput) return;
      console.log("handleOnLogin isConnected", isConnected);

      const isConnected = await networkStore.hasInternetConnection();
      if (!isConnected) return;

      const deviceInfoData = await userStore.getDeviceInfo();
      console.log("handleOnLogin deviceInfoData", deviceInfoData);

      let response = await userService.login(
        null,
        this.username,
        this.password,
        deviceInfoData
      );

      response = {
        deviceToken: JSON.parse(deviceInfoData).deviceToken,
        ...response
      };
      console.log("handleOnLogin response", response);

      switch (response.statusCode) {
        case 200:
          if (this.validateAccessRole(response)) {
            this.saveRecentlyEmail(this.username);
            this.handleLoginSuccess(response);
          } else {
            toastMessageStore.toast(errorMessage.ACCESS_DENIED);
            this.isLoading = false;
          }
          break;
        case 405:
          toastMessageStore.toast(errorMessage.ACCOUNT_DEACTIVATE);
          break;
        case 601:
        case 602:
        case 603:
          this.handleLoginFailed();
          break;
        default:
          toastMessageStore.toast(errorMessage.SOMETHING_WENT_WRONG);
          break;
      }
      this.isLoading = false;
    } catch (error) {
      toastMessageStore.toast(errorMessage.SOMETHING_WENT_WRONG);
      console.log("error login", error);
    }
  };

  @action
  handleLoginFailed = () => {
    this.numberFailed += 1;
    if (this.numberFailed === this.MAXIMUM_FAILURE) {
      this.disableButton = true;
      this.editableInput = false;
      this.isLoading = false;
      toastMessageStore.toast(
        errorMessage.YOU_HAVE_BEEN_BLOCKED_TRY_AGAIN_30SECOND
      );
      setTimeout(() => {
        runInAction(() => {
          this.disableButton = false;
          this.editableInput = true;
          this.numberFailed = 0;
        });
      }, this.BLOCK_TIMEOUT);
    } else {
      this.isLoading = false;
      toastMessageStore.toast(errorMessage.USERNAME_PASSWORD_INCORRECT);
    }
  };

  @action
  handleLoginSuccess = async data => {
    this.isLoading = false;
    userStore.saveLoginData(data);
    this.numberFailed = 0;
    const { user } = userStore;

    await userStore.getInfo();

    if (user.role === userRole.EMPLOYEE) {
      dataStore.setRole("employee");
      dataStore.setTheme("employee");
      switch (true) {
        case user.hasPincode:
          if (splashStore.notificationPayload) {
            notificationService.handleOnOpenNotification(
              splashStore.notificationPayload
            );
            splashStore.notificationPayload = null;
          } else {
            this.navigation.navigate("HomeTab");
            navbarStore.setSelectedTab("HomeTab");
          }
          break;
        case !user.conditionAccepted:
          this.navigation.navigate("Terms");
          break;
        case !user.readAboutApp:
          this.navigation.navigate("Etape1");
          break;
        case user.hasPincode === false:
          this.navigation.reset(
            [NavigationActions.navigate({ routeName: "CustomizePinCode" })],
            0
          );
          break;
        default:
          break;
      }
    } else if (user.role === userRole.RESTAURANT) {
      dataStore.setRole("restaurant");
      dataStore.setTheme("restaurant");
      switch (true) {
        case user.hasPincode:
          this.navigation.navigate("APRHomeTab");
          navbarStore.setSelectedTab("APRHomeTab");
          break;
        case !user.conditionAccepted:
          this.navigation.navigate("Terms");
          break;
        case !user.readAboutApp:
          this.navigation.navigate("Etape1");
          break;
        case user.hasPincode === false:
          this.navigation.reset(
            [NavigationActions.navigate({ routeName: "CustomizePinCode" })],
            0
          );
          break;
        default:
          break;
      }
    }
  };

  @action
  validateInput = () => {
    let isValid = true;

    switch (true) {
      case Validate.isEmpty(this.username) || Validate.isEmpty(this.password):
        toastMessageStore.toast(errorMessage.USERNAME_PASSWORD_NOT_FILL_OUT);
        isValid = false;
        break;
      // eslint-disable-next-line no-restricted-globals
      case !isNaN(this.username):
        isValid = true;
        break;
      case !Validate.isEmail(this.username):
        toastMessageStore.toast(errorMessage.EMAIL_INVALID);
        isValid = false;
        break;
      default:
        break;
    }

    return isValid;
  };

  validateAccessRole = response => {
    const { roles } = response;
    if (
      roles.includes(userRole.EMPLOYEE) ||
      roles.includes(userRole.RESTAURANT)
    )
      return true;
    return false;
  };

  @action
  saveRecentlyEmail = email => {
    if (array.findIndex(this.recentlyEmail, _email => _email === email) !== -1)
      return;
    this.recentlyEmail = [email, ...this.recentlyEmail.slice(0, 2)];
  };

  @action
  saveRecentlyForgotEmail = email => {
    if (
      array.findIndex(this.recentlyForgotEmail, _email => _email === email) !==
      -1
    )
      return;
    this.recentlyForgotEmail = [email, ...this.recentlyForgotEmail.slice(0, 2)];
  };

  @action
  setShowSuggest = status => {
    if (typeof status !== "boolean") return;
    this.showSuggest = status;
  };
}

const hydrate = create({
  storage: AsyncStorage,
  jsonify: true
});

const signInStore = new SignInStore();
hydrate("signInStore", signInStore);

export default signInStore;
