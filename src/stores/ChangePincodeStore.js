import { observable, action, computed, runInAction } from "mobx";
import userService from "services/UserService";
import { errorMessage, infoMessage } from "utils/Constants";
import keyboardStore from "./KeyboardStore";
import UserStore from "./UserStore";
import toastMessageStore from "./ToastMessageStore";
import networkStore from "./NetworkStore";

class ChangePincodeStore {
  @observable pincodeViewStr = "";

  @observable currPincodeValue = "";

  @observable newPincodeValue = "";

  @observable confirmPincodeValue = "";

  @observable isInputCompleted = false;

  @observable isDelaying = false;

  @observable disableButton = true;

  @observable inputType = null;

  @observable titleRef = null;

  @observable navigation = null;

  @observable displayError = false;

  @observable errorMessage = "";

  @observable numberFailed = 0;

  @observable forgotOption = false;

  @observable modal = true;

  MAX_NUMBER_PINCODE = 4;

  INPUT_CURRENT_PINCODE = 1;

  INPUT_NEW_PINCODE = 2;

  INPUT_CONFIRM_PINCODE = 3;

  REINPUT_CURRENT_PINCODE = 4;

  REINPUT_NEW_PINCODE = 5;

  DELAY_TRANSITION = 100;

  MAXIMUM_FAILURE = 5;

  @action
  initScreen = navigation => {
    this.inputType = this.INPUT_CURRENT_PINCODE;
    this.forgotOption = true;
    keyboardStore.setMaxLength(this.MAX_NUMBER_PINCODE);
    keyboardStore.setBlockBeginZero(false);
    this.refreshScreen();
    this.navigation = navigation;
    this.numberFailed = 0;
    this.modal = true;
  };

  @action
  refreshScreen = () => {
    keyboardStore.clear();
    this.pincodeViewStr = "";
    this.isDelaying = false;
    this.disableButton = true;
    this.isInputCompleted = false;
    this.displayError = false;
    this.errorMessage = "";
  };

  @action
  addPincode = () => {
    if (this.isInputCompleted || this.isDelaying) return;

    this.pincodeViewStr += "* ";
    if (keyboardStore.length !== this.MAX_NUMBER_PINCODE) return;

    switch (this.inputType) {
      case this.INPUT_CURRENT_PINCODE:
      case this.REINPUT_CURRENT_PINCODE:
        this.handleCompleteInputCurrentPincode();
        break;
      case this.INPUT_NEW_PINCODE:
      case this.REINPUT_NEW_PINCODE:
        this.handleCompleteInputNewPincode();
        break;
      case this.INPUT_CONFIRM_PINCODE:
        this.handleCompleteInputConfirmPincode();
        break;
      default:
        break;
    }
  };

  addPincodeForProfile = () => {
    if (this.isInputCompleted || this.isDelaying) return;

    this.pincodeViewStr += "* ";
    if (keyboardStore.length !== this.MAX_NUMBER_PINCODE) return;

    this.verifyProfile();
  };

  @action
  verifyProfile = async () => {
    try {
      const isConnected = await networkStore.hasInternetConnection();
      if (!isConnected) {
        this.navigation.pop();
        return;
      }

      this.currPincodeValue = keyboardStore.getValue;
      const { token } = UserStore.user;
      const response = await userService.verifyPincode(
        token,
        this.currPincodeValue
      );
      console.log(response);
      switch (response.statusCode) {
        case 200:
          this.navigation.replace("Setting");
          this.modal = false;
          break;
        case 601:
          this.showErrorMessage(errorMessage.PINCODE_NOT_MATCH, null);
          break;
        default:
          this.navigation.pop();
          toastMessageStore.toast(errorMessage.SOMETHING_WENT_WRONG);
          break;
      }
    } catch (error) {
      this.navigation.pop();
      toastMessageStore.toast(errorMessage.SOMETHING_WENT_WRONG);
    }
  };

  @action
  handleCompleteInputCurrentPincode = async () => {
    try {
      const isConnected = await networkStore.hasInternetConnection();
      if (!isConnected) {
        this.navigation.pop();
        return;
      }

      this.currPincodeValue = keyboardStore.getValue;
      const { token } = UserStore.user;
      const response = await userService.verifyPincode(
        token,
        this.currPincodeValue
      );

      switch (response.statusCode) {
        case 200:
          this.verifyCurrentPincodeSuccess();
          break;
        case 601:
          this.showErrorMessage(errorMessage.PINCODE_NOT_MATCH, null);
          break;
        default:
          this.navigation.pop();
          toastMessageStore.toast(errorMessage.SOMETHING_WENT_WRONG);
          break;
      }
    } catch (error) {
      this.navigation.pop();
      toastMessageStore.toast(errorMessage.SOMETHING_WENT_WRONG);
      console.log(error);
    }
  };

  @action
  verifyCurrentPincodeSuccess = () => {
    this.titleRef.fadeOut(this.DELAY_TRANSITION);
    this.isDelaying = true;
    setTimeout(() => {
      runInAction(() => {
        this.refreshScreen();
        this.inputType = this.INPUT_NEW_PINCODE;
      });
    }, this.DELAY_TRANSITION);
  };

  @action
  handleCompleteInputNewPincode = () => {
    this.newPincodeValue = keyboardStore.getValue;
    if (this.isPincodeValid) {
      this.titleRef.fadeOut(this.DELAY_TRANSITION);
      this.isDelaying = true;
      setTimeout(() => {
        runInAction(() => {
          this.refreshScreen();
          this.inputType = this.INPUT_CONFIRM_PINCODE;
        });
      }, this.DELAY_TRANSITION);
    } else {
      this.showErrorMessage(errorMessage.PINCODE_CANNOT_SAME_THE_OLD_ONE, null);
    }
  };

  @action
  handleCompleteInputConfirmPincode = () => {
    this.confirmPincodeValue = keyboardStore.getValue;
    this.isInputCompleted = true;
    this.disableButton = false;
    this.handleOnValidate();
  };

  @action
  handleReInputCurrentPincode = () => {
    this.isDelaying = true;
    this.titleRef.fadeOut(this.DELAY_TRANSITION);
    setTimeout(() => {
      this.inputType = this.REINPUT_CURRENT_PINCODE;
    }, this.DELAY_TRANSITION);
    this.refreshScreen();
  };

  @action
  handleReInputNewPincode = () => {
    this.isDelaying = true;
    this.titleRef.fadeOut(this.DELAY_TRANSITION);
    setTimeout(() => {
      this.inputType = this.REINPUT_NEW_PINCODE;
    }, this.DELAY_TRANSITION);
    this.refreshScreen();
    this.numberFailed = 0;
  };

  @action
  removePincode = () => {
    if (!this.pincodeViewStr.length) return;
    this.pincodeViewStr = this.pincodeViewStr.substr(
      0,
      this.pincodeViewStr.length - 2
    );
    if (this.isInputCompleted) {
      this.isInputCompleted = false;
    }
    if (!this.disabledButton) {
      this.disableButton = true;
    }
  };

  @action
  handleOnValidate = async () => {
    if (this.newPincodeValue !== this.confirmPincodeValue) {
      this.handleInputConfirmPincodeFailure();
    } else {
      try {
        const { token } = UserStore.user;
        const response = await userService.changePincode(
          token,
          this.newPincodeValue
        );

        if (response.statusCode === 200) {
          this.navigation.pop();
          this.refreshScreen();
          toastMessageStore.toast(infoMessage.CHANGE_PINCODE_SUCCESS);
        } else {
          throw new Error("Request change pincode failed");
        }
      } catch (error) {
        toastMessageStore.toast(errorMessage.SOMETHING_WENT_WRONG);
        this.handleReInputCurrentPincode();
        console.log("Error change pincode: ", error);
      }
    }
  };

  @action
  handleInputConfirmPincodeFailure = () => {
    this.numberFailed += 1;
    if (this.numberFailed === this.MAXIMUM_FAILURE) {
      this.showErrorMessage(
        errorMessage.YOU_HAVE_BEEN_BLOCKED_TRY_AGAIN_15SECOND,
        15000,
        () => {
          this.inputType = this.REINPUT_NEW_PINCODE;
          this.numberFailed = 0;
        }
      );
    } else {
      this.showErrorMessage(errorMessage.SORRY_TRY_AGAIN, null);
    }
  };

  @action
  setTitleRef = ref => {
    if (!ref) return;
    this.titleRef = ref;
  };

  // @action
  // showErrorMessage = (message, timeout, callback) => {
  //   this.refreshScreen();
  //   this.errorMessage = message;
  //   this.displayError = true;

  //   if (timeout) {
  //     keyboardStore.blockKeyboard();
  //     this.isDelaying = true;

  //     setTimeout(() => {
  //       this.isDelaying = false;
  //       keyboardStore.unblockKeyboard();
  //       this.displayError = false;
  //       if (callback) {
  //         callback();
  //       }
  //     }, timeout);
  //   }
  // };
  @action
  showErrorMessage = (message, timeout, callback) => {
    this.refreshScreen();
    toastMessageStore.toast(message);

    if (timeout) {
      keyboardStore.blockKeyboard();
      this.isDelaying = true;

      setTimeout(() => {
        this.isDelaying = false;
        keyboardStore.unblockKeyboard();
        if (callback) {
          callback();
        }
      }, timeout);
    }
  };

  @action
  handleBlockFailureInput = () => {
    this.errorMessage = errorMessage.YOU_HAVE_BEEN_BLOCKED_TRY_AGAIN_15SECOND;
    this.displayError = true;
    setTimeout(() => {
      this.isDelaying = false;
      keyboardStore.unblockKeyboard();
      this.inputType = this.REINPUT_PINCODE;
      this.displayError = false;
      this.numberFailed = 0;
    }, 15000);
  };

  @computed
  get isPincodeValid() {
    return this.currPincodeValue !== this.newPincodeValue;
  }

  @action
  forgotPincode = async () => {
    try {
      const { email, token } = UserStore.user;
      const response = await userService.forgotPincode(email, token);
      console.log(response);
      if (response.statusCode === 200) {
        this.navigation.pop();
        toastMessageStore.toast(
          "An email is sent to reset your pincode. Please check your email"
        );
      }
    } catch (error) {
      toastMessageStore.toast(errorMessage.SOMETHING_WENT_WRONG);
    }
  };
}

const changePincodeStore = new ChangePincodeStore();
export default changePincodeStore;
