import { observable, action } from "mobx";
import { Animated, Keyboard } from "react-native";
import { _ } from "lodash";
import { getStatus } from "utils/Time";
import restaurantStore from "./RestaurantStore";

class HomepageStore {
  @observable isInitializing = true;

  @observable restaurant = null;

  @observable listAnnotations = [];

  @observable activeAnnotationIndex = -1;

  @observable previousActiveAnnotationIndex = -1;

  @observable animationValue = null;

  @observable opacityValue = null;

  @observable mapRef = null;

  @observable showPreview = true;

  @observable screenshot = null;

  @observable isCapturing = true;

  @action
  setMapRef = ref => {
    this.mapRef = ref;
  };

  @action
  reload = () => {
    if (restaurantStore.listRestaurants) {
      this.listAnnotations = restaurantStore.listRestaurants.forEach(
        (item, index) => {
          const status = getStatus(item.openingHours);
          restaurantStore.listRestaurants[index].isOpening = status.isOpening;
          restaurantStore.listRestaurants[index].time = status.time;
        }
      );
    }
  };

  @action
  initListAnnotations = () => {
    this.animationValue = new Animated.Value(0.8);
    this.opacityValue = new Animated.Value(1);

    if (!restaurantStore.listRestaurants) return;
    this.listAnnotations = restaurantStore.listRestaurants.map(item => {
      const annotation = {
        id: item.id,
        coordinate: [item.longitude, item.latitude],
        title: item.restaurantName,
        animationStyle: {
          transform: [
            {
              scale: this.animationValue
            }
          ]
        },
        opacityStyle: {
          opacity: this.opacityValue
        },
        isOpening: item.isOpening,
        time: item.time
      };

      return annotation;
    });
    this.isInitializing = false;
  };

  @action
  onAnnotationSelected = selectedIndex => {
    // if (selectedIndex === this.activeAnnotationIndex) return;
    this.onAnnotationDeselected();

    this.opacityValue = new Animated.Value(0.4);
    this.activeAnnotationIndex = selectedIndex;

    const restaurant = restaurantStore.listRestaurants[selectedIndex];
    this.restaurant = {
      id: restaurant.id,
      avatar: restaurant.avatar,
      name: restaurant.restaurantName,
      images: restaurant.photo,
      facebook: restaurant.facebook,
      instagram: restaurant.instagram,
      website: restaurant.website,
      email: restaurant.email,
      phoneNumber: [restaurant.phoneNumber1, restaurant.phoneNumber2],
      openingHours: restaurant.openingHours,
      description: restaurant.description,
      isOpening: restaurant.isOpening,
      time: restaurant.time,
      cardAccepted: restaurant.cardAccepted
    };
    this.showPreview = true;

    if (this.previousActiveAnnotationIndex !== -1) {
      this.mapRef.moveTo(this.listAnnotations[selectedIndex].coordinate, 500);
    }
    Keyboard.dismiss();
  };

  @action
  onAnnotationDeselected = () => {
    this.opacityValue = new Animated.Value(1);
    this.previousActiveAnnotationIndex = this.activeAnnotationIndex;
    this.activeAnnotationIndex = -1;
    this.restaurant = null;
  };

  moveToMarker = (lng, lat) => {
    // this.mapRef.setCamera({
    //   centerCoordinate: [lng, lat],
    //   zoom: 15,
    //   duration: 500
    // });
    this.mapRef.moveTo([lng, lat], 500);
    // this.mapRef.zoomTo(13, 500);
  };

  @action
  moveToMarkerProps = (lng, lat, id) => {
    this.moveToMarker(lng, lat);
    const index = _.findIndex(this.listAnnotations, item => item.id === id);
    this.activeAnnotationIndex = index;
  };

  @action
  takeScreenshot = () => {
    this.mapRef
      .takeSnap()
      .then(success => {
        this.screenshot = success;
        this.isCapturing = false;
      })
      .catch(error => console.log(error));
  };
}

const homepageStore = new HomepageStore();
export default homepageStore;
