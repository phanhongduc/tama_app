import { observable, action } from "mobx";
import networkStore from "./NetworkStore";

class RestaurantDetailStore {
  @observable openGalleryModal = false;

  @observable selectedImgIdx = null;

  @observable isConnected = true;

  @action
  toggleGalleryModal = () => {
    this.openGalleryModal = !this.openGalleryModal;
  };

  @action
  onPressImage = index => {
    this.selectedImgIdx = index;
    this.toggleGalleryModal();
  };

  @action
  checkInternetConnection = async () => {
    this.isConnected = await networkStore.hasInternetConnection();
  };
}

const restaurantDetailStore = new RestaurantDetailStore();
export default restaurantDetailStore;
