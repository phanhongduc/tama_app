import { observable, action } from "mobx";
import { NavigationActions, StackActions } from "react-navigation";
import userService from "services/UserService";
import { errorMessage, Images } from "utils/Constants";
import toastMessageStore from "stores/ToastMessageStore";
import userStore from "./UserStore";

class Etape1Store {
  @observable informationAPE = [
    {
      name: `Bienvenue sur\nl’appli Pass Tama’a`,
      data: `Découvrez l'application\ndes Titres-Restaurants\ndématérialisés`,
      image: Images.IG_ABOUTAPP_1
    },
    {
      name: "Suivez\nvotre budget",
      data: `Accédez à vos paiements\net votre solde au quotidien`,
      image: Images.IG_ABOUTAPP_2
    },
    {
      name: "Trouvez\noù manger",
      data: `Localisez facilement les\nétablissements partenaires\nproches de vous`,
      image: Images.IG_ABOUTAPP_3
    },
    {
      name: "Payez en\ntoute sécurité",
      data: `Effectuez des règlements\nsécurisés avec votre\ncode Tama’a`,
      image: Images.IG_ABOUTAPP_4
    }
  ];

  @observable informationAPR = [
    {
      name: `Bienvenue sur \nI'appli Pass Tama'a`,
      data: `Votre système de paiement\nen Titres-Restaurants\ndématérialisés`,
      image: Images.IG_APR_ABOUTAPP_1
    },
    {
      name: `Gagnez en visibilité`,
      data: `Diffusez vos offres et photos \nalléchantes de vos menus`,
      image: Images.IG_APR_ABOUTAPP_2
    },
    {
      name: `Bénéficiez d'un \nsystème de paiement`,
      data: `Prélevez au franc près \nles adhérents du réseau \nPass Tama'a`,
      image: Images.IG_APR_ABOUTAPP_3
    },
    {
      name: `Suivez vos \nrevenus`,
      data: `Consultez I'historique \ndes transactions dans \nvotre espace sécurisé`,
      image: Images.IG_APR_ABOUTAPP_4
    }
  ];

  @observable activeSlide = 0;

  @observable carouselRef = null;

  @observable navigation = null;

  @action
  initScreen = navigation => {
    this.navigation = navigation;
    this.activeSlide = 0;
  };

  @action
  setActiveSlide = index => {
    this.activeSlide = index;
  };

  @action
  setCarouselRef = ref => {
    this.carouselRef = ref;
  };

  handleHardwareBack = () => {
    if (this.activeSlide) {
      this.activeSlide -= 1;
      this.carouselRef.snapToPrev();
    }
  };

  handleOnNavigate = async () => {
    try {
      const response = await userService.readAboutApp(userStore.user.token);
      const param = {
        requestReadAppPending: false
      };

      if (response.statusCode !== 200) {
        param.requestReadAppPending = true;
      } else {
        userStore.user.readAboutApp = true;
      }

      const resetAction = StackActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate(
            { routeName: "CustomizePinCode" },
            { param }
          )
        ]
      });
      this.navigation.dispatch(resetAction);
    } catch (error) {
      toastMessageStore.toast(errorMessage.SOMETHING_WENT_WRONG);
      console.log(error);
    }
  };
}
const etape1Store = new Etape1Store();
export default etape1Store;
