import { observable, action, runInAction } from "mobx";
import userService from "services/UserService";
import { errorMessage, userRole } from "utils/Constants";
import keyboardStore from "./KeyboardStore";
import userStore from "./UserStore";
import toastMessageStore from "./ToastMessageStore";
import headerLeftStore from "./HeaderLeftStore";
import navbarStore from "./NavbarStore";

class CustomizePinCodeStore {
  @observable pincodeViewStr = "";

  @observable inputPincodeValue = "";

  @observable confirmPincodeValue = "";

  @observable isInputCompleted = false;

  @observable isDelaying = false; // Delay for animation transition

  @observable disableButton = true;

  @observable inputType = null;

  @observable titleRef = null;

  @observable navigation = null;

  @observable displayError = false;

  @observable errorMessage = "";

  @observable numberFailed = 0;

  MAX_NUMBER_PINCODE = 4;

  INPUT_PINCODE = 1;

  INPUT_CONFIRM_PINCODE = 2;

  REINPUT_PINCODE = 3;

  DELAY_TRANSITION = 100;

  MAXIMUM_FAILURE = 5;

  @action
  initScreen = navigation => {
    this.inputType = this.INPUT_PINCODE;
    this.navigation = navigation;
    keyboardStore.setMaxLength(this.MAX_NUMBER_PINCODE);
    keyboardStore.setBlockBeginZero(false);
    this.numberFailed = 0;
    headerLeftStore.setVisible(false);
    this.refreshScreen();
    const param = navigation.getParam("param", {});
    if (param.requestReadAppPending) {
      userService.readAboutApp(userStore.user.token);
    }
  };

  @action
  refreshScreen = () => {
    keyboardStore.clear();
    this.pincodeViewStr = "";
    this.isDelaying = false;
    this.disableButton = true;
    this.isInputCompleted = false;
    this.displayError = false;
    this.errorMessage = "";
  };

  @action
  addPincode = () => {
    if (this.isInputCompleted || this.isDelaying) return;

    this.pincodeViewStr += "* ";
    if (keyboardStore.length !== this.MAX_NUMBER_PINCODE) return;

    headerLeftStore.setVisible(true);

    switch (this.inputType) {
      case this.INPUT_PINCODE:
      case this.REINPUT_PINCODE:
        this.isDelaying = true;
        setTimeout(() => {
          this.handleCompleteInputPincode();
        }, 100);
        break;
      case this.INPUT_CONFIRM_PINCODE:
        this.handleCompleteInputConfirmPincode();
        break;
      default:
        break;
    }
  };

  @action
  handleCompleteInputPincode = () => {
    this.inputPincodeValue = keyboardStore.getValue;
    this.titleRef.fadeOut(this.DELAY_TRANSITION);
    setTimeout(() => {
      runInAction(() => {
        this.inputType = this.INPUT_CONFIRM_PINCODE;
      });
    }, this.DELAY_TRANSITION);
    this.refreshScreen();
  };

  @action
  handleCompleteInputConfirmPincode = () => {
    this.confirmPincodeValue = keyboardStore.getValue;
    this.isInputCompleted = true;
    this.disableButton = false;
    this.handleOnValidate();
  };

  @action
  handleReInputPincode = () => {
    this.isDelaying = true;
    this.titleRef.fadeOut(this.DELAY_TRANSITION);
    setTimeout(() => {
      this.inputType = this.REINPUT_PINCODE;
    }, this.DELAY_TRANSITION);
    this.refreshScreen();
    this.numberFailed = 0;
  };

  @action
  removePincode = () => {
    if (!this.pincodeViewStr.length) return;
    this.pincodeViewStr = this.pincodeViewStr.substr(
      0,
      this.pincodeViewStr.length - 2
    );
    if (this.isInputCompleted) {
      this.isInputCompleted = false;
    }
    if (!this.disabledButton) {
      this.disableButton = true;
    }
  };

  @action
  handleOnValidate = () => {
    if (this.inputPincodeValue === this.confirmPincodeValue) {
      //  not set user role
      setTimeout(() => {
        this.createPincode();
      }, 100);
    } else {
      this.numberFailed += 1;
      if (this.numberFailed === this.MAXIMUM_FAILURE) {
        this.showErrorMessage(
          errorMessage.YOU_HAVE_BEEN_BLOCKED_TRY_AGAIN_15SECOND,
          15000,
          () => {
            this.inputType = this.REINPUT_PINCODE;
            this.numberFailed = 0;
          }
        );
      } else {
        this.showErrorMessage(errorMessage.SORRY_TRY_AGAIN, null);
      }
    }
  };

  // @action
  // showErrorMessage = (message, timeout, callback) => {
  //   this.refreshScreen();
  //   this.errorMessage = message;
  //   this.displayError = true;

  //   if (timeout) {
  //     keyboardStore.blockKeyboard();
  //     this.isDelaying = true;

  //     setTimeout(() => {
  //       this.isDelaying = false;
  //       keyboardStore.unblockKeyboard();
  //       this.displayError = false;
  //       if (callback) {
  //         callback();
  //       }
  //     }, timeout);
  //   }
  // };

  @action
  showErrorMessage = (message, timeout, callback) => {
    this.refreshScreen();
    toastMessageStore.toast(message);

    if (timeout) {
      keyboardStore.blockKeyboard();
      this.isDelaying = true;

      setTimeout(() => {
        this.isDelaying = false;
        keyboardStore.unblockKeyboard();
        if (callback) {
          callback();
        }
      }, timeout);
    }
  };

  @action
  setTitleRef = ref => {
    if (!ref) return;
    this.titleRef = ref;
  };

  createPincode = async () => {
    try {
      const response = await userService.createPincode(
        userStore.user.token,
        this.confirmPincodeValue
      );
      switch (response.statusCode) {
        case 200:
          userStore.user.hasPincode = true;
          if (userStore.user.role === userRole.EMPLOYEE) {
            this.navigation.navigate("HomeTab");
            navbarStore.setSelectedTab("HomeTab");
          } else if (userStore.user.role === userRole.RESTAURANT) {
            this.navigation.navigate("APRHomeTab");
            navbarStore.setSelectedTab("APRHomeTab");
          }
          this.refreshScreen();
          break;
        default:
          toastMessageStore.toast(errorMessage.SOMETHING_WENT_WRONG);
          break;
      }
    } catch (error) {
      toastMessageStore.toast(errorMessage.SOMETHING_WENT_WRONG);
      console.log("error create pincode", error);
    }
  };
}

const customizePinCodeStore = new CustomizePinCodeStore();
export default customizePinCodeStore;
