import { observable, action } from "mobx";

class HeaderLeftStore {
  @observable visible = true;

  @action
  setVisible = status => {
    this.visible = status;
  }
}

const headerLeftStore = new HeaderLeftStore();
export default headerLeftStore;
