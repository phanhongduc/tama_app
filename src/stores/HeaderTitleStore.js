import { observable, action } from "mobx";

class HeaderTitleStore {
  @observable title = null;

  @observable greeting = "Iaorana";

  @observable username = "";

  @action
  setTitle = title => {
    if (title) {
      this.title = title;
    } else {
      this.title = `${this.greeting} ${this.username}`;
    }
  };

  @action
  setUsername = username => {
    this.username = username;
    this.setTitle(null);
  };
}

const headerTitleStore = new HeaderTitleStore();
export default headerTitleStore;
