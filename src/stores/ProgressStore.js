import { observable, action } from "mobx";
import userStore from "stores/UserStore";
import networkStore from "stores/NetworkStore";
import qrScanStore from "stores/QRScanStore";
import userService from "services/UserService";
import { userRole } from "utils/Constants";

class ProgressStore {
  @observable navigation = null;

  ANIMATION_TIMEOUT = 2000; // ms

  @action
  initScreen = navigation => {
    this.navigation = navigation;
  };

  processPayment = async () => {
    try {
      const isConnected = await networkStore.hasInternetConnection();
      if (!isConnected) {
        this.navigation.navigate("PaymentFailure");
        qrScanStore.setIsProcessing(false);
        return;
      }

      const { token } = userStore.user;
      const amount = this.navigation.getParam("amount", null);
      const customID = qrScanStore.getScanId;

      const promise = [
        new Promise(resolve => {
          setTimeout(resolve, this.ANIMATION_TIMEOUT);
        }),
        userService.makePayment(token, customID, amount)
      ];

      const [, paymentResponse] = await Promise.all(promise);
      console.log("paymentResponse", JSON.stringify(paymentResponse));
      switch (paymentResponse.statusCode) {
        case 200: {
          qrScanStore.resetScan();

          const { user } = userStore;
          let restaurantName = null;

          if (user.role === userRole.EMPLOYEE) {
            restaurantName = this.navigation.getParam("restaurantName", null);
          } else {
            restaurantName = user.restaurantName;
          }

          this.navigation.navigate("PaymentSuccess", {
            amount: paymentResponse.amount,
            time: paymentResponse.time,
            restaurantName
          });
          break;
        }
        default:
          this.navigation.navigate("PaymentFailure");
          break;
      }
    } catch (error) {
      this.navigation.navigate("PaymentFailure");
      qrScanStore.setIsProcessing(false);
      console.log("error at payment", error);
    }
  };
}

const progressStore = new ProgressStore();
export default progressStore;
