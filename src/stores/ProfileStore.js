import { observable, action, runInAction } from "mobx";
import lang from "lodash/lang";
import { format } from "utils/Number";
import userStore from "stores/UserStore";
import toastMessageStore from "stores/ToastMessageStore";
import { convertTimeObjectToStringArray } from "utils/Time";
import { errorMessage } from "utils/Constants";
import moment from "moment";

class ProfileStore {
  @observable employeeBalance = null;

  @observable availableBalance = null;

  @observable availableTimes = null;

  @observable dailyAmount = null;

  @observable expirationDate = null;

  @action
  getData = async () => {
    try {
      userStore
        .getInfo()
        .then(() => this.updateData())
        .catch(error => Promise.reject(error));

      this.updateData();
    } catch (error) {
      toastMessageStore.toast(errorMessage.SOMETHING_WENT_WRONG);
      console.log("error: ", error);
    }
  };

  @action
  updateData = () => {
    try {
      const {
        employeeConditions,
        employeeBalance,
        availableBalance,
        expirationDate,
        dailyAmount
      } = userStore.user;

      runInAction(() => {
        if (typeof availableBalance === "number" && availableBalance >= 0) {
          this.availableBalance = format(availableBalance);
        }
        if (typeof employeeBalance === "number" && employeeBalance >= 0) {
          this.employeeBalance = format(employeeBalance);
        }
        if (!lang.isEmpty(employeeConditions)) {
          this.availableTimes = convertTimeObjectToStringArray(
            employeeConditions
          ).join("\n");
        }
        if (typeof dailyAmount === "number" && dailyAmount >= 0) {
          this.dailyAmount = `Plafond journalier: ${format(dailyAmount)} XPF`;
        } else if (typeof dailyAmount === "object" && !dailyAmount) {
          this.dailyAmount = `Plafond journalier: Pas de réstriction`;
        }
        if (!lang.isEmpty(expirationDate)) {
          this.expirationDate = `Date d'expiration: ${moment(
            expirationDate
          ).format("DD/MM/YYYY")}`;
        }
      });
    } catch (error) {
      toastMessageStore.toast(errorMessage.SOMETHING_WENT_WRONG);
      console.log("error: ", error);
    }
  };
}

const profileStore = new ProfileStore();
export default profileStore;
