import { observable, action } from "mobx";
import Fuse from "fuse.js";
import lang from "lodash/lang";
import array from "lodash/array";
import { create, persist } from "mobx-persist";
import AsyncStorage from "@react-native-community/async-storage";

class SearchbarStore {
  @observable hideResult = true;

  @observable searchTextList = "";

  @observable searchResultList = [];

  @observable searchTextMap = "";

  @observable searchResultMap = [];

  @observable searchResultIsland = [];

  @persist("list") @observable searchHistory = [];

  @persist("list") @observable searchHistoryList = [];

  @observable fuse = null;

  @observable fuseList = null;

  @observable fuseMap = null;

  @observable islands = [
    {
      islandName: "Faa'a",
      island: true,
      longitude: -149.6162426,
      latitude: -17.562068
    },
    {
      islandName: "Papeete",
      island: true,
      longitude: -149.5907887,
      latitude: -17.5594211
    },
    {
      islandName: "Arue",
      island: true,
      longitude: -149.5529683,
      latitude: -17.5484448
    },
    {
      islandName: "Punaauia",
      island: true,
      longitude: -149.6357038,
      latitude: -17.6243814
    },
    {
      islandName: "Tahiti",
      island: true,
      longitude: -149.5128895,
      latitude: -17.6872201
    },
    {
      islandName: "Moorea",
      island: true,
      longitude: -149.8342467,
      latitude: -17.5334145
    },
    {
      islandName: "Bora Bora",
      island: true,
      longitude: -151.7442086,
      latitude: -16.5008802
    },
    {
      islandName: "Raiatea",
      island: true,
      longitude: -151.4926674,
      latitude: -16.8194068
    }
  ];

  @action
  initSourceData = (data, keys) => {
    this.fuse = new Fuse(data, {
      shouldSort: true,
      tokenize: true,
      threshold: 0.4,
      location: 0,
      distance: 10,
      maxPatternLength: 32,
      minMatchCharLength: 1,
      keys
    });
  };

  @action
  initSourceDataList = (data, keys) => {
    this.fuseList = new Fuse(data, {
      shouldSort: true,
      tokenize: true,
      threshold: 0.5,
      location: 0,
      distance: 10,
      maxPatternLength: 32,
      minMatchCharLength: 1,
      keys
    });
  };

  @action
  searchList = searchText => {
    const tempSearchText = this.escapeCharacter(searchText);
    this.searchTextList = tempSearchText;

    if (tempSearchText === "") {
      this.searchResultList = [];
      return;
    }

    this.searchResultList = [
      {
        section: tempSearchText,
        items: this.fuseList.search(tempSearchText)
      }
    ];
  };

  @action
  searchMap = searchText => {
    const tempSearchText = this.escapeCharacter(searchText);
    this.searchTextMap = tempSearchText;

    if (lang.isEmpty(tempSearchText)) {
      this.searchResultMap = [];
      this.searchResultIsland = [];
      return;
    }

    this.searchResultMap = this.fuse.search(tempSearchText);

    // search islands
    const fuseIsland = new Fuse(this.islands, {
      shouldSort: true,
      tokenize: true,
      threshold: 0.5,
      location: 0,
      distance: 10,
      maxPatternLength: 10,
      minMatchCharLength: 1,
      keys: ["islandName"]
    });

    this.searchResultIsland = fuseIsland.search(searchText);
    this.searchResultMap = [
      ...this.searchResultMap,
      ...this.searchResultIsland
    ];
    console.log(this.searchResultMap, this.searchResultIsland);
  };

  @action
  clearInputMap = () => {
    this.searchTextMap = "";
  };

  @action
  clearInputSearch = () => {
    this.searchTextList = "";
  };

  @action
  hideSearchResult = () => {
    this.hideResult = true;
  };

  @action
  showSearchResult = () => {
    this.hideResult = false;
  };

  @action
  setSearchText = searchText => {
    this.searchTextMap = searchText;
  };

  escapeCharacter = string => {
    const result = string.replace(/[\]{}()*+?.\\^$|#]/g, "");

    return result;
  };

  @action
  saveHistory = item => {
    if (
      array.findIndex(
        this.searchHistory,
        restaurant => restaurant.id === item.id
      ) !== -1
    )
      return;
    this.searchHistory = [item, ...this.searchHistory.slice(0, 2)];
  };

  @action
  saveHistoryList = item => {
    if (
      array.findIndex(
        this.searchHistoryList,
        restaurant => restaurant.id === item.id
      ) !== -1
    )
      return;
    this.searchHistoryList = [item, ...this.searchHistoryList.slice(0, 2)];
  };

  @action
  clearData = () => {
    AsyncStorage.removeItem("searchbarStore");
  };
}

const hydrate = create({
  storage: AsyncStorage,
  jsonify: true
});

const searchbarStore = new SearchbarStore();
hydrate("searchbarStore", searchbarStore);

export default searchbarStore;
