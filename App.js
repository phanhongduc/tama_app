/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

// import from libraries
import React, { Component } from "react";
import {
  createAppContainer,
  createStackNavigator,
  createSwitchNavigator,
  NavigationActions
} from "react-navigation";
import { Provider } from "mobx-react";
import notificationService from "services/NotificationService";
// ------Splash Stack------
import SplashScreen from "screens/Splash";
// ------Auth Stack-------
import SignInScreen from "screens/Auth/SignIn";
import ForgotScreen from "screens/Auth/ForgotPassword";
import TermConditionScreen from "screens/Auth/TermsCondition";
import Etape1Screen from "screens/Auth/Etape1";
import CustomizePinCode from "screens/Auth/CustomizePinCode";
import ConfirmedScreen from "screens/Auth/Confirmed";
// ------APE Main Stack------
import APEMainStack from "utils/APENavigator";
// ------APR Main Stack------
import APRMainStack from "utils/APRNavigator";
// import constants
import { fonts, colors, fontSizes } from "utils/Constants";
// import stores
import keyboardStore from "stores/KeyboardStore";
import customizePinCodeStore from "stores/CustomizePinCodeStore";
import changePincodeStore from "stores/ChangePincodeStore";
import paymentAmountStore from "stores/PaymentAmountStore";
import paymentPincodeStore from "stores/PaymentPincodeStore";
import etape1Store from "stores/Etape1Store";
import profileStore from "stores/ProfileStore";
import termConditionStore from "stores/TermsConditionStore";
import mesInfosStore from "stores/MesInfosStore";
import restaurantDetailStore from "stores/RestaurantDetailStore";
import headerTitleStore from "stores/HeaderTitleStore";
import restaurantCardItemStore from "stores/RestaurantCardItemStore";
import navbarStore from "stores/NavbarStore";
import historyScreenStore from "stores/HistoryStore";
import restaurantStore from "stores/RestaurantStore";
import userStore from "stores/UserStore";
import homepageStore from "stores/HomepageStore";
import qrScanStore from "stores/QRScanStore";
import searchbarStore from "stores/SearchbarStore";
import dataStore from "stores/DataStore";
import toastMessageStore from "stores/ToastMessageStore";
import signInStore from "stores/SignInStore";
import networkStore from "stores/NetworkStore";
import informationStore from "stores/InformationStore";
import headerLeftStore from "stores/HeaderLeftStore";
import progressStore from "stores/ProgressStore";
import restaurantHomepageStore from "stores/RestaurantHomepageStore";
import splashStore from "stores/SplashStore";

const stores = {
  keyboardStore,
  customizePinCodeStore,
  changePincodeStore,
  paymentAmountStore,
  paymentPincodeStore,
  etape1Store,
  profileStore,
  termConditionStore,
  mesInfosStore,
  restaurantDetailStore,
  headerTitleStore,
  restaurantCardItemStore,
  navbarStore,
  historyScreenStore,
  restaurantStore,
  userStore,
  homepageStore,
  searchbarStore,
  dataStore,
  toastMessageStore,
  signInStore,
  networkStore,
  informationStore,
  qrScanStore,
  headerLeftStore,
  progressStore,
  restaurantHomepageStore,
  splashStore
};

const AuthStack = createStackNavigator(
  {
    SignIn: {
      screen: SignInScreen
    },
    Forgot: {
      screen: ForgotScreen
    },
    Terms: {
      screen: TermConditionScreen,
      navigationOptions: {
        gesturesEnabled: false
      }
    },
    Etape1: {
      screen: Etape1Screen,
      navigationOptions: {
        gesturesEnabled: false
      }
    },
    CustomizePinCode: {
      screen: CustomizePinCode,
      navigationOptions: {
        gesturesEnabled: false
      }
    },
    Confirmed: {
      screen: ConfirmedScreen,
      navigationOptions: {
        gesturesEnabled: false
      }
    }
  },
  {
    initialRouteName: "SignIn",
    defaultNavigationOptions: ({ screenProps }) => {
      const themeApp = screenProps.theme;
      return {
        headerBackTitle: null,
        headerStyle: {
          backgroundColor: themeApp.currentTheme,
          shadowOffset: { width: 0, height: 0 },
          elevation: 0,
          borderBottomWidth: 0
        },
        headerTintColor: colors.text.white,
        headerTitleStyle: {
          fontFamily: fonts.primary.semibold,
          fontWeight: "200",
          fontSize: fontSizes.header,
          color: colors.text.white
        }
      };
    },
    headerLayoutPreset: "center"
  }
);

// AuthStack.navigationOptions = ({ navigation }) => {
//   const { routeName } = navigation.state.routes[navigation.state.index];
//   const gesturesEnabled =
//     routeName !== "Etape1" && routeName !== "CustomizePinCode";

//   return {
//     gesturesEnabled
//   };
// };

const defaultGetStateForActionAuthStack = AuthStack.router.getStateForAction;
AuthStack.router.getStateForAction = (action, state) => {
  if (!state || action.type !== NavigationActions.BACK) {
    return defaultGetStateForActionAuthStack(action, state);
  }

  const { routeName } = state.routes[state.index];
  switch (routeName) {
    case "Confirmed":
      return defaultGetStateForActionAuthStack(action, state);
    case "Terms":
      return defaultGetStateForActionAuthStack(action, state);
    case "Etape1":
      etape1Store.handleHardwareBack();
      return null;
    case "CustomizePinCode":
      if (
        customizePinCodeStore.numberFailed ===
        customizePinCodeStore.MAXIMUM_FAILURE
      ) {
        return null;
      }
      if (
        customizePinCodeStore.inputType ===
        customizePinCodeStore.INPUT_CONFIRM_PINCODE
      ) {
        customizePinCodeStore.handleReInputPincode();
        return null;
      }
      return null;
    // return defaultGetStateForActionAuthStack(action, state);
    default:
      return defaultGetStateForActionAuthStack(action, state);
  }
};

const AppStack = createSwitchNavigator(
  {
    Splash: {
      screen: SplashScreen
    },
    Auth: {
      screen: AuthStack
    },
    Main: {
      screen: APEMainStack
    },
    MainAPR: {
      screen: APRMainStack
    }
  },
  {
    // initialRouteName: "Auth"
    // initialRouteName: "Main"
  }
);
const AppContainer = createAppContainer(AppStack);

export default class App extends Component {
  async componentDidMount() {
    await notificationService.init();
  }

  async componentWillUnmount() {
    notificationService.release();
    await userStore.dropSession();
  }

  render() {
    return (
      // eslint-disable-next-line react/jsx-props-no-spreading
      <Provider {...stores}>
        <AppContainer
          screenProps={{ theme: dataStore, role: userStore.role }}
        />
      </Provider>
    );
  }
}
